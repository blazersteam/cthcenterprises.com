<?php

if(!function_exists('eltd_core_version_class')) {
	/**
	 * Adds plugins version class to body
	 *
	 * @param $classes
	 *
	 * @return array
	 */
	function eltd_core_version_class($classes) {
		$classes[] = 'eltd-core-'.ELATED_CORE_VERSION;

		return $classes;
	}

	add_filter('body_class', 'eltd_core_version_class');
}

if(!function_exists('eltd_core_theme_installed')) {
	/**
	 * Checks whether theme is installed or not
	 * @return bool
	 */
	function eltd_core_theme_installed() {
		return defined('ELATED_ROOT');
	}
}

if(!function_exists('eltd_core_get_carousel_slider_array')) {
	/**
	 * Function that returns associative array of carousels,
	 * where key is term slug and value is term name
	 * @return array
	 */
	function eltd_core_get_carousel_slider_array() {
		$carousels_array = array();
        $args = array(
            'taxonomy' => 'carousels_category'
        );
        $terms = get_terms($args);

		if(is_array($terms) && count($terms)) {
			$carousels_array[''] = '';
			foreach($terms as $term) {
				$carousels_array[$term->slug] = $term->name;
			}
		}

		return $carousels_array;
	}
}

if(!function_exists('eltd_core_get_carousel_slider_array_vc')) {
	/**
	 * Function that returns array of carousels formatted for Visual Composer
	 *
	 * @return array array of carousels where key is term title and value is term slug
	 *
	 * @see eltd_core_get_carousel_slider_array
	 */
	function eltd_core_get_carousel_slider_array_vc() {
		return array_flip(eltd_core_get_carousel_slider_array());
	}
}

if(!function_exists('eltd_core_get_shortcode_module_template_part')) {
	/**
	 * Loads module template part.
	 *
	 * @param string $shortcode name of the shortcode folder
	 * @param string $template name of the template to load
	 * @param string $slug
	 * @param array $params array of parameters to pass to template
	 *
	 * @see malmo_elated_get_template_part()
	 */
	function eltd_core_get_shortcode_module_template_part($template, $module, $slug = '', $params = array()) {

		//HTML Content from template
		$html          = '';
		$template_path = ELATED_CORE_CPT_PATH.'/'.$module.'/shortcodes';

		$temp = $template_path.'/'.$template;
		if(is_array($params) && count($params)) {
			extract($params);
		}

		$template = '';

		if($temp !== '') {
			if($slug !== '') {
				$template = "{$temp}-{$slug}.php";
			}
			$template = $temp.'.php';
		}
		if($template) {
			ob_start();
			include($template);
			$html = ob_get_clean();
		}

		return $html;
	}
}

if(!function_exists('eltd_core_ajax_url')) {
	/**
	 * load themes ajax functionality
	 *
	 */
	function eltd_core_ajax_url() {
		echo '<script type="application/javascript">var eltdCoreAjaxUrl = "'.admin_url('admin-ajax.php').'"</script>';
	}

	add_action('wp_enqueue_scripts', 'eltd_core_ajax_url');

}

if(!function_exists('eltd_core_inline_style')) {
	/**
	 * Function that echoes generated style attribute
	 *
	 * @param $value string | array attribute value
	 *
	 */
	function eltd_core_inline_style($value) {
		echo eltd_core_get_inline_style($value);
	}
}

if(!function_exists('eltd_core_get_inline_style')) {
	/**
	 * Function that generates style attribute and returns generated string
	 *
	 * @param $value string | array value of style attribute
	 *
	 * @return string generated style attribute
	 *
	 */
	function eltd_core_get_inline_style($value) {
		return eltd_core_get_inline_attr($value, 'style', ';');
	}
}

if(!function_exists('eltd_core_class_attribute')) {
	/**
	 * Function that echoes class attribute
	 *
	 * @param $value string value of class attribute
	 *
	 * @see eltd_core_get_class_attribute()
	 */
	function eltd_core_class_attribute($value) {
		echo eltd_core_get_class_attribute($value);
	}
}

if(!function_exists('eltd_core_get_class_attribute')) {
	/**
	 * Function that returns generated class attribute
	 *
	 * @param $value string value of class attribute
	 *
	 * @return string generated class attribute
	 *
	 * @see eltd_core_get_inline_attr()
	 */
	function eltd_core_get_class_attribute($value) {
		return eltd_core_get_inline_attr($value, 'class', ' ');
	}
}

if(!function_exists('eltd_core_get_inline_attr')) {
	/**
	 * Function that generates html attribute
	 *
	 * @param $value string | array value of html attribute
	 * @param $attr string name of html attribute to generate
	 * @param $glue string glue with which to implode $attr. Used only when $attr is array
	 *
	 * @return string generated html attribute
	 */
	function eltd_core_get_inline_attr($value, $attr, $glue = '') {
		if(!empty($value)) {

			if(is_array($value) && count($value)) {
				$properties = implode($glue, $value);
			} elseif($value !== '') {
				$properties = $value;
			}

			return $attr.'="'.esc_attr($properties).'"';
		}

		return '';
	}
}

if(!function_exists('eltd_core_inline_attr')) {
	/**
	 * Function that generates html attribute
	 *
	 * @param $value string | array value of html attribute
	 * @param $attr string name of html attribute to generate
	 * @param $glue string glue with which to implode $attr. Used only when $attr is array
	 *
	 * @return string generated html attribute
	 */
	function eltd_core_inline_attr($value, $attr, $glue = '') {
		echo eltd_core_get_inline_attr($value, $attr, $glue);
	}
}

if(!function_exists('eltd_core_get_inline_attrs')) {
	/**
	 * Generate multiple inline attributes
	 *
	 * @param $attrs
	 *
	 * @return string
	 */
	function eltd_core_get_inline_attrs($attrs) {
		$output = '';

		if(is_array($attrs) && count($attrs)) {
			foreach($attrs as $attr => $value) {
				$output .= ' '.eltd_core_get_inline_attr($value, $attr);
			}
		}

		ltrim($output);

		return $output;
	}
}

if(!function_exists('eltd_core_get_attachment_id_from_url')) {
	/**
	 * Function that retrieves attachment id for passed attachment url
	 *
	 * @param $attachment_url
	 *
	 * @return null|string
	 */
	function eltd_core_get_attachment_id_from_url($attachment_url) {
		global $wpdb;
		$attachment_id = '';

		//is attachment url set?
		if($attachment_url !== '') {
			//prepare query

			$query = $wpdb->prepare("SELECT ID FROM {$wpdb->posts} WHERE guid=%s", $attachment_url);

			//get attachment id
			$attachment_id = $wpdb->get_var($query);
		}

		//return id
		return $attachment_id;
	}
}

if(!function_exists('eltd_core_init_shortcode_loader')) {
    function eltd_core_init_shortcode_loader() {

        include_once 'shortcode-loader.php';
    }

    add_action('malmo_elated_shortcode_loader', 'eltd_core_init_shortcode_loader');
}

if(!function_exists('eltd_core_add_user_custom_fields')) {
    /**
     * Function creates custom social fields for users
     *
     * return $user_contact
     */
    function eltd_core_add_user_custom_fields($user_contact) {

        /**
         * Function that add custom user fields
         **/
        $user_contact['position']   = esc_html__('Position', 'eltd_core');
        $user_contact['instagram']  = esc_html__('Instagram', 'eltd_core');
        $user_contact['twitter']    = esc_html__('Twitter', 'eltd_core');
        $user_contact['pinterest']  = esc_html__('Pinterest', 'eltd_core');
        $user_contact['tumblr']     = esc_html__('Tumbrl', 'eltd_core');
        $user_contact['facebook']   = esc_html__('Facebook', 'eltd_core');
        $user_contact['googleplus'] = esc_html__('Google Plus', 'eltd_core');
        $user_contact['linkedin']   = esc_html__('Linkedin', 'eltd_core');

        return $user_contact;
    }

    add_filter('user_contactmethods', 'eltd_core_add_user_custom_fields');
}
/*Function for adding custom meta boxes hooked to default action */

if ( class_exists( 'WP_Block_Type' ) && defined( 'ELATED_ROOT' ) ) {
	add_action( 'admin_head', 'malmo_elated_meta_box_add' );
} else {
	add_action('add_meta_boxes', 'malmo_elated_meta_box_add');
}

if (!function_exists('malmo_elated_create_meta_box_handler')) {

	function malmo_elated_create_meta_box_handler( $box, $key, $screen ) {
		add_meta_box(
			'eltdf-meta-box-'.$key,
			$box->title,
			'malmo_elated_render_meta_box',
			$screen,
			'advanced',
			'high',
			array( 'box' => $box)
		);
	}
}