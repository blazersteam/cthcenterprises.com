<?php
namespace ElatedCore\CPT\Portfolio\Shortcodes;

use ElatedCore\Lib;
use ElatedCore\CPT\Portfolio\Lib as PortfolioLib;

/**
 * Class PortfolioList
 * @package ElatedCore\CPT\Portfolio\Shortcodes
 */
class PortfolioList implements Lib\ShortcodeInterface {
    /**
     * @var string
     */
    private $base;

    /**
     * PortfolioList constructor.
     */
    public function __construct() {
        $this->base = 'eltd_portfolio_list';

        add_action('vc_before_init', array($this, 'vcMap'));

        add_action('wp_ajax_nopriv_eltd_core_portfolio_ajax_load_more', array($this, 'loadMorePortfolios'));
        add_action('wp_ajax_eltd_core_portfolio_ajax_load_more', array($this, 'loadMorePortfolios'));
    }


    /**
     * Loads portfolios via AJAX
     */
    public function loadMorePortfolios() {
        $shortcodeParams = $this->getShortcodeParamsFromPost();

        $html           = '';
        $portfolioQuery = PortfolioLib\PortfolioQuery::getInstance();
        $queryResults   = $portfolioQuery->buildQueryObject($shortcodeParams);

        if($shortcodeParams['type'] !== 'masonry') {
            $shortcodeParams['thumb_size']            = $this->getImageSize($shortcodeParams);
            $shortcodeParams['use_custom_image_size'] = false;
            if($shortcodeParams['thumb_size'] === 'custom' && !empty($shortcodeParams['custom_image_dimensions'])) {
                $shortcodeParams['use_custom_image_size'] = true;
                $shortcodeParams['custom_image_sizes']    = $this->getCustomImageSize($shortcodeParams['custom_image_dimensions']);
            }
        }

        if($queryResults->have_posts()) {
            while($queryResults->have_posts()) {
                $queryResults->the_post();

                $shortcodeParams['current_id'] = get_the_ID();

                if($shortcodeParams['type'] === 'masonry') {
                    $shortcodeParams['thumb_size'] = $this->getImageSize($shortcodeParams);
                }

                $shortcodeParams['icon_html']            = $this->getPortfolioIconsHtml($shortcodeParams);
                $shortcodeParams['category_html']        = $this->getItemCategoriesHtml($shortcodeParams);
                $shortcodeParams['categories']           = $this->getItemCategories($shortcodeParams);
                $shortcodeParams['article_masonry_size'] = $this->getMasonrySize($shortcodeParams);
				$shortcodeParams['item_link']            = $this->getItemLink($shortcodeParams)['portfolio_link'];
				$shortcodeParams['item_target']          = $this->getItemLink($shortcodeParams)['portfolio_target'];

                $html .= eltd_core_get_shortcode_module_template_part('portfolio-list/templates/'.$shortcodeParams['type'], 'portfolio', '', $shortcodeParams);
            }

            wp_reset_postdata();
        } else {
            $html .= '<p>'.esc_html__('Sorry, no posts matched your criteria.', 'eltd_core').'</p>';
        }

        $returnObj = array(
            'html' => $html,
        );

        echo json_encode($returnObj);
        exit;
    }

    /**
     * Prepares shortcode params array from $_POST and returns it
     *
     * @return array
     */
    private function getShortcodeParamsFromPost() {
        $shortcodeParams = array();

        if(!empty($_POST['type'])) {
            $shortcodeParams['type'] = $_POST['type'];
        }

        if(!empty($_POST['columns'])) {
            $shortcodeParams['columns'] = $_POST['columns'];
        }

        if(!empty($_POST['gridSize'])) {
            $shortcodeParams['grid_size'] = $_POST['gridSize'];
        }

        if(!empty($_POST['orderBy'])) {
            $shortcodeParams['order_by'] = $_POST['orderBy'];
        }

        if(!empty($_POST['order'])) {
            $shortcodeParams['order'] = $_POST['order'];
        }

        if(!empty($_POST['number'])) {
            $shortcodeParams['number'] = $_POST['number'];
        }
        if(!empty($_POST['imageSize'])) {
            $shortcodeParams['image_size'] = $_POST['imageSize'];
        }

        if(!empty($_POST['customImageDimensions'])) {
            $shortcodeParams['custom_image_dimensions'] = $_POST['customImageDimensions'];
        }

        if(!empty($_POST['filter'])) {
            $shortcodeParams['filter'] = $_POST['filter'];
        }

        if(!empty($_POST['filterOrderBy'])) {
            $shortcodeParams['filter_order_by'] = $_POST['filterOrderBy'];
        }

        if(!empty($_POST['category'])) {
            $shortcodeParams['category'] = $_POST['category'];
        }

        if(!empty($_POST['selectedProjectes'])) {
            $shortcodeParams['selected_projectes'] = $_POST['selectedProjectes'];
        }

        if(!empty($_POST['showLoadMore'])) {
            $shortcodeParams['show_load_more'] = $_POST['showLoadMore'];
        }

        if(!empty($_POST['titleTag'])) {
            $shortcodeParams['title_tag'] = $_POST['titleTag'];
        }

        if(!empty($_POST['nextPage'])) {
            $shortcodeParams['next_page'] = $_POST['nextPage'];
        }

        if(!empty($_POST['activeFilterCat'])) {
            $shortcodeParams['active_filter_cat'] = $_POST['activeFilterCat'];
        }

        if(!empty($_POST['showExcerpt'])) {
            $shortcodeParams['show_excerpt'] = $_POST['showExcerpt'];
        }

        if(!empty($_POST['space_between_portfolio_items'])) {
            $shortcodeParams['space_between_portfolio_items'] = $_POST['spaceBetweenPortfolioItems'];
        }

        return $shortcodeParams;
    }

    /**
     * Returns base for shortcode
     * @return string
     */
    public function getBase() {
        return $this->base;
    }

    /**
     * Maps shortcode to Visual Composer
     *
     * @see vc_map
     */
    public function vcMap() {
        if(function_exists('vc_map')) {
            vc_map(array(
                    'name'                      => esc_html__('Portfolio List', 'eltd_core'),
                    'base'                      => $this->getBase(),
                    'category'                  => esc_html__('by ELATED', 'eltd_core'),
                    'icon'                      => 'icon-wpb-portfolio extended-custom-icon',
                    'allowed_container_element' => 'vc_row',
                    'params'                    => array_merge(
                        array(
                            array(
                                'type'        => 'dropdown',
                                'heading'     => esc_html__('Portfolio List Template', 'eltd_core'),
                                'param_name'  => 'type',
                                'value'       => array(
                                    esc_html__('Standard', 'eltd_core')          => 'standard',
                                    esc_html__('Gallery', 'eltd_core')           => 'gallery',
                                    esc_html__('Masonry', 'eltd_core')           => 'masonry',
                                    esc_html__('Pinterest', 'eltd_core')         => 'pinterest',
                                    esc_html__('Justified Gallery', 'eltd_core') => 'justified-gallery'
                                ),
                                'admin_label' => true,
                                'description' => '',
                                'group'       => esc_html__('Layout Options', 'eltd_core')
                            ),
                            array(
                                'type'        => 'dropdown',
                                'heading'     => esc_html__('Masonry Mode', 'eltd_core'),
                                'param_name'  => 'masonry_mode',
                                'value'       => array(
                                    esc_html__('Normal', 'eltd_core')   => 'normal',
                                    esc_html__('Parallax', 'eltd_core') => 'parallax'
                                ),
                                'save_always' => true,
                                'dependency'  => array('element' => 'type', 'value' => array('masonry')),
                                'description' => esc_html__('In Parallax mode, the chosen portfolio items will float as you scroll.', 'eltd_core'),
                                'group'       => esc_html__('Layout Options', 'eltd_core')
                            ),
                            array(
                                'type'        => 'dropdown',
                                'heading'     => esc_html__('Title Tag', 'eltd_core'),
                                'param_name'  => 'title_tag',
                                'value'       => array(
                                    ''   => '',
                                    'h2' => 'h2',
                                    'h3' => 'h3',
                                    'h4' => 'h4',
                                    'h5' => 'h5',
                                    'h6' => 'h6',
                                ),
                                'admin_label' => true,
                                'description' => '',
                                'group'       => esc_html__('Layout Options', 'eltd_core')
                            ),
                            array(
                                'type'        => 'dropdown',
                                'heading'     => esc_html__('Masonry Columns', 'eltd_core'),
                                'param_name'  => 'masonry_columns',
                                'default'     => 'four',
                                'value'       => array(
                                    esc_html__('Three', 'eltd_core') => 'three',
                                    esc_html__('Four', 'eltd_core')  => 'four',
                                    esc_html__('Five', 'eltd_core')  => 'five'
                                ),
                                'save_always' => true,
                                'admin_label' => true,
                                'description' => '',
                                'dependency'  => array('element' => 'type', 'value' => array('masonry')),
                                'group'       => esc_html__('Layout Options', 'eltd_core')
                            ),
                            array(
                                'type'        => 'dropdown',
                                'heading'     => esc_html__('Image Proportions', 'eltd_core'),
                                'param_name'  => 'image_size',
                                'value'       => array(
                                    esc_html__('Original', 'eltd_core')  => 'full',
                                    esc_html__('Square', 'eltd_core')    => 'square',
                                    esc_html__('Landscape', 'eltd_core') => 'landscape',
                                    esc_html__('Portrait', 'eltd_core')  => 'portrait',
                                    esc_html__('Custom', 'eltd_core')    => 'custom'
                                ),
                                'save_always' => true,
                                'admin_label' => true,
                                'description' => '',
                                'dependency'  => array('element' => 'type', 'value' => array('standard', 'gallery')),
                                'group'       => esc_html__('Layout Options', 'eltd_core')
                            ),
                            array(
                                'type'        => 'textfield',
                                'admin_label' => true,
                                'heading'     => esc_html__('Image Dimensions', 'eltd_core'),
                                'param_name'  => 'custom_image_dimensions',
                                'value'       => '',
                                'description' => esc_html__('Enter custom image dimensions. Enter image size in pixels: 200x100 (Width x Height)', 'eltd_core'),
                                'group'       => esc_html__('Layout Options', 'eltd_core'),
                                'dependency'  => array('element' => 'image_size', 'value' => 'custom')
                            ),
                            array(
                                "type"        => "textfield",
                                "heading"     => esc_html__('Row Height (px)', 'eltd_core'),
                                "param_name"  => "row_height",
                                "value"       => "200",
                                "save_always" => true,
                                "description" => esc_html__('Targeted row height, which may vary depending on the proportions of the images.', 'eltd_core'),
                                'group'       => esc_html__('Layout Options', 'eltd_core'),
                                "dependency"  => array('element' => "type", 'value' => array('justified-gallery'))
                            ),
                            array(
                                "type"        => "dropdown",
                                "heading"     => esc_html__('Last Row Behavior', 'eltd_core'),
                                "param_name"  => "justify_last_row",
                                "value"       => array(
                                    esc_html__('Align left', 'eltd_core')      => 'nojustify',
                                    esc_html__('Align right', 'eltd_core')     => 'right',
                                    esc_html__('Align centrally', 'eltd_core') => 'center',
                                    esc_html__('Justify', 'eltd_core')         => 'justify',
                                    esc_html__('Hide', 'eltd_core')            => 'hide'
                                ),
                                "save_always" => true,
                                "description" => esc_html__('Defines whether to justify the last row, align it in a certain way, or hide it.', 'eltd_core'),
                                'group'       => esc_html__('Layout Options', 'eltd_core'),
                                "dependency"  => array('element' => "type", 'value' => array('justified-gallery'))
                            ),
                            array(
                                "type"        => "textfield",
                                "heading"     => esc_html__('Justify Threshold (0-1)', 'eltd_core'),
                                "param_name"  => "justify_threshold",
                                "value"       => "0.75",
                                "save_always" => true,
                                "description" => esc_html__('If the last row takes up more than this part of available width, it will be justified despite the defined alignment. Enter 1 to never justify the last row.', 'eltd_core'),
                                'group'       => esc_html__('Layout Options', 'eltd_core'),
                                "dependency"  => array(
                                    'element' => "justify_last_row",
                                    'value'   => array('nojustify', 'right', 'center')
                                )
                            ),
                            array(
                                'type'        => 'dropdown',
                                'heading'     => esc_html__('Show Load More', 'eltd_core'),
                                'param_name'  => 'show_load_more',
                                'value'       => array(
                                    esc_html__('Yes', 'eltd_core') => 'yes',
                                    esc_html__('No', 'eltd_core')  => 'no'
                                ),
                                'save_always' => true,
                                'admin_label' => true,
                                'description' => esc_html__('Default value is Yes', 'eltd_core'),
                                'group'       => esc_html__('Layout Options', 'eltd_core')
                            ),
                            array(
                                'type'        => 'dropdown',
                                'heading'     => esc_html__('Number of Columns', 'eltd_core'),
                                'param_name'  => 'columns',
                                'value'       => array(
                                    ''                           => '',
                                    esc_html__('One', 'eltd_core')   => 'one',
                                    esc_html__('Two', 'eltd_core')   => 'two',
                                    esc_html__('Three', 'eltd_core') => 'three',
                                    esc_html__('Four', 'eltd_core')  => 'four',
                                    esc_html__('Five', 'eltd_core')  => 'five',
                                    esc_html__('Six', 'eltd_core')   => 'six'
                                ),
                                'admin_label' => true,
                                'description' => esc_html__('Default value is Three', 'eltd_core'),
                                'dependency'  => array('element' => 'type', 'value' => array('standard', 'gallery')),
                                'group'       => esc_html__('Layout Options', 'eltd_core')
                            ),
                            array(
                                'type'        => 'dropdown',
                                'heading'     => esc_html__('Grid Size', 'eltd_core'),
                                'param_name'  => 'grid_size',
                                'value'       => array(
                                    esc_html__('Default', 'eltd_core')        => '',
                                    esc_html__('3 Columns Grid', 'eltd_core') => 'three',
                                    esc_html__('4 Columns Grid', 'eltd_core') => 'four',
                                    esc_html__('5 Columns Grid', 'eltd_core') => 'five'
                                ),
                                'admin_label' => true,
                                'description' => esc_html__('This option is only for Full Width Page Template', 'eltd_core'),
                                'dependency'  => array('element' => 'type', 'value' => array('pinterest')),
                                'group'       => esc_html__('Layout Options', 'eltd_core')
                            ),
                            array(
                                'type'        => 'dropdown',
                                'heading'     => esc_html__('Enable Category Filter', 'eltd_core'),
                                'param_name'  => 'filter',
                                'value'       => array(
                                    esc_html__('No', 'eltd_core')  => 'no',
                                    esc_html__('Yes', 'eltd_core') => 'yes'
                                ),
                                'admin_label' => true,
                                'save_always' => true,
                                'description' => esc_html__('Default value is No', 'eltd_core'),
                                'group'       => esc_html__('Layout Options', 'eltd_core')
                            ),
                            array(
                                'type'        => 'dropdown',
                                'heading'     => esc_html__('Filter Order By', 'eltd_core'),
                                'param_name'  => 'filter_order_by',
                                'value'       => array(
                                    esc_html__('Name', 'eltd_core')  => 'name',
                                    esc_html__('Count', 'eltd_core') => 'count',
                                    esc_html__('Id', 'eltd_core')    => 'id',
                                    esc_html__('Slug', 'eltd_core')  => 'slug'
                                ),
                                'admin_label' => true,
                                'save_always' => true,
                                'description' => esc_html__('Default value is Name', 'eltd_core'),
                                'dependency'  => array('element' => 'filter', 'value' => array('yes')),
                                'group'       => esc_html__('Layout Options', 'eltd_core')
                            ),
                            array(
                                'type'        => 'dropdown',
                                'heading'     => esc_html__('Space Between Portfolio Items', 'eltd_core'),
                                'param_name'  => 'space_between_portfolio_items',
                                'value'       => array(
                                    ''                         => '',
                                    esc_html__('Yes', 'eltd_core') => 'yes',
                                    esc_html__('No', 'eltd_core')  => 'no'
                                ),
                                'admin_label' => true,
                                'save_always' => true,
                                'dependency'  => array(
                                    'element' => 'type',
                                    'value'   => array('gallery', 'justified-gallery', 'masonry')
                                ),
                                'group'       => esc_html__('Layout Options', 'eltd_core')
                            ),
                            array(
                                'type'        => 'dropdown',
                                'heading'     => esc_html__('Type of Hover Effect', 'eltd_core'),
                                'param_name'  => 'hover_type',
                                'value'       => array(
                                    esc_html__('Full Hover', 'eltd_core') => 'full-hover',
                                    esc_html__('Partial Hover', 'eltd_core')  => 'partial-hover'
                                ),
                                'admin_label' => true,
                                'save_always' => true,
                                'dependency'  => array(
                                    'element' => 'type',
                                    'value'   => array('gallery', 'justified-gallery', 'masonry')
                                ),
                                'group'       => esc_html__('Layout Options', 'eltd_core')
                            ),
                            array(
                                'type'        => 'dropdown',
                                'heading'     => esc_html__('Show Excerpt?', 'eltd_core'),
                                'param_name'  => 'show_excerpt',
                                'value'       => array(
                                    ''                         => '',
                                    esc_html__('Yes', 'eltd_core') => 'yes',
                                    esc_html__('No', 'eltd_core')  => 'no'
                                ),
                                'admin_label' => true,
                                'save_always' => true,
                                'dependency'  => array('element' => 'type', 'value' => array('standard')),
                                'group'       => esc_html__('Layout Options', 'eltd_core')
                            )
                        ),
                        PortfolioLib\PortfolioQuery::getInstance()->queryVCParams()
                    )
                )
            );
        }
    }

    /**
     * Renders shortcodes HTML
     *
     * @param $atts array of shortcode params
     * @param $content string shortcode content
     *
     * @return string
     */
    public function render($atts, $content = null) {

        $args = array(
            'type'                          => 'standard',
            'masonry_mode'                  => 'normal',
            'columns'                       => 'three',
            'row_height'                    => '',
            'justify_last_row'              => '',
            'justify_threshold'             => '',
            'masonry_columns'               => 'four',
            'grid_size'                     => 'three',
            'image_size'                    => 'full',
            'filter'                        => 'no',
            'filter_order_by'               => 'name',
            'show_load_more'                => 'yes',
            'title_tag'                     => 'h3',
            'portfolio_slider'              => '',
            'portfolios_shown'              => '',
            'space_between_portfolio_items' => '',
            'hover_type'                    => '',
            'show_excerpt'                  => 'yes',
            'custom_image_dimensions'       => ''
        );

        $portfolioQuery = PortfolioLib\PortfolioQuery::getInstance();

        $args   = array_merge($args, $portfolioQuery->getShortcodeAtts());
        $params = shortcode_atts($args, $atts);

        extract($params);

        $queryResults            = $portfolioQuery->buildQueryObject($params);
        $params['query_results'] = $queryResults;

        $classes  = $this->getPortfolioClasses($params);
        $dataAtts = $this->getDataAtts($params);
        $dataAtts .= ' data-max-num-pages = '.$queryResults->max_num_pages;
        $params['filter_class'] = '';

        $html = '';

        if($filter == 'yes' && ($type == 'masonry' || $type == 'pinterest' || $type == 'justified-gallery')) {
            $params['filter_categories'] = $this->getFilterCategories($params);
            $params['filter_class']      = $type == 'justified-gallery' ? 'eltd-justified-gallery-filter' : 'eltd-masonry-filter';
            $html .= eltd_core_get_shortcode_module_template_part('portfolio-list/templates/portfolio-filter', 'portfolio', '', $params);
        }

        $html .= '<div class = "eltd-portfolio-list-holder-outer '.$classes.'" '.$dataAtts.'>';

        if($filter == 'yes' && ($type == 'standard' || $type == 'gallery')) {
            $params['filter_categories'] = $this->getFilterCategories($params);
            $html .= eltd_core_get_shortcode_module_template_part('portfolio-list/templates/portfolio-filter', 'portfolio', '', $params);
        }

        $html .= '<div class = "eltd-portfolio-list-holder clearfix" >';
        if($type == 'masonry' || $type == 'pinterest') {
            $html .= '<div class="eltd-portfolio-list-masonry-grid-sizer"></div>';
            $html .= '<div class="eltd-portfolio-list-masonry-grid-gutter"></div>';
        }

        if($type !== 'masonry') {
            $params['thumb_size']            = $this->getImageSize($params);
            $params['use_custom_image_size'] = false;
            if($params['thumb_size'] === 'custom' && !empty($params['custom_image_dimensions'])) {
                $params['use_custom_image_size'] = true;
                $params['custom_image_sizes']    = $this->getCustomImageSize($params['custom_image_dimensions']);
            }
        }

        if($queryResults->have_posts()) {
            while($queryResults->have_posts()) {
                $queryResults->the_post();

                $params['current_id'] = get_the_ID();

                if($type === 'masonry') {
                    $params['thumb_size'] = $this->getImageSize($params);
                }

                $params['icon_html']                 = $this->getPortfolioIconsHtml($params);
                $params['category_html']             = $this->getItemCategoriesHtml($params);
                $params['categories']                = $this->getItemCategories($params);
                $params['article_masonry_size']      = $this->getMasonrySize($params);
                $params['masonry_parallax_behavior'] = $this->getMasonryParallaxBehavior($params);
				$params['item_link']            = $this->getItemLink($params)['portfolio_link'];
				$params['item_target']          = $this->getItemLink($params)['portfolio_target'];

                $html .= eltd_core_get_shortcode_module_template_part('portfolio-list/templates/'.$type, 'portfolio', '', $params);
            }

            if($type == 'standard' || ($params['space_between_portfolio_items'] == 'yes' && $type == 'gallery')) {
                switch($columns) {
                    case 'two':
                        $html .= '<div class="eltd-ptf-gap"></div>';
                        break;
                    case 'three':
                        $html .= '<div class="eltd-ptf-gap"></div>';
                        $html .= '<div class="eltd-ptf-gap"></div>';
                        break;
                    case 'four':
                        $html .= '<div class="eltd-ptf-gap"></div>';
                        $html .= '<div class="eltd-ptf-gap"></div>';
                        $html .= '<div class="eltd-ptf-gap"></div>';
                        break;
                    case 'five':
                        $html .= '<div class="eltd-ptf-gap"></div>';
                        $html .= '<div class="eltd-ptf-gap"></div>';
                        $html .= '<div class="eltd-ptf-gap"></div>';
                        $html .= '<div class="eltd-ptf-gap"></div>';
                        break;
                    case 'six':
                        $html .= '<div class="eltd-ptf-gap"></div>';
                        $html .= '<div class="eltd-ptf-gap"></div>';
                        $html .= '<div class="eltd-ptf-gap"></div>';
                        $html .= '<div class="eltd-ptf-gap"></div>';
                        $html .= '<div class="eltd-ptf-gap"></div>';
                        break;
                    default:
                        break;
                }
            }
        } else {
            $html .= '<p>'.esc_html_e('Sorry, no posts matched your criteria.').'</p>';
        }

        $html .= '</div>'; //close eltd-portfolio-list-holder

        if($show_load_more == 'yes') {
            $html .= eltd_core_get_shortcode_module_template_part('portfolio-list/templates/load-more-template', 'portfolio', '', $params);
        }

        wp_reset_postdata();

        $html .= '</div>'; // close eltd-portfolio-list-holder-outer
        return $html;
    }

    /**
     * Generates portfolio icons html
     *
     * @param $params
     *
     * @return html
     */
    public function getPortfolioIconsHtml($params) {

        $html       = '';
        $id         = $params['current_id'];
        $slug_list_ = 'pretty_photo_gallery';

        $featured_image_array = wp_get_attachment_image_src(get_post_thumbnail_id($id), 'full'); //original size
        $large_image          = $featured_image_array[0];

        $html .= '<div class="eltd-item-icons-holder">';

        $html .= '<a class="eltd-portfolio-lightbox" title="'.get_the_title($id).'" href="'.$large_image.'" data-rel="prettyPhoto['.$slug_list_.']"></a>';


        if(function_exists('malmo_elated_like_portfolio_list')) {
            $html .= malmo_elated_like_portfolio_list($id);
        }

        $html .= '<a class="eltd-preview" title="Go to Project" href="'.$this->getItemLink($params)['portfolio_link'].'" data-type="portfolio_list"></a>';

        $html .= '</div>';

        return $html;

    }

    /**
     * Generates portfolio classes
     *
     * @param $params
     *
     * @return string
     */
    public function getPortfolioClasses($params) {
        $classes         = array();
        $type            = $params['type'];
        $columns         = $params['columns'];
        $masonry_columns = $params['masonry_columns'];
        $grid_size       = $params['grid_size'];
        switch($type):
            case 'standard':
                $classes[] = 'eltd-ptf-standard';
                $classes[] = 'eltd-ptf-with-spaces';
                break;
            case 'gallery':
                $classes[] = 'eltd-ptf-gallery';

                if($params['space_between_portfolio_items'] == 'yes') {
                    $classes[] = 'eltd-ptf-with-spaces';
                }

                $classes[] = 'eltd-portfolio-gallery-hover';

                break;
            case 'masonry':
                $classes[] = 'eltd-ptf-masonry';
                $classes[] = 'eltd-portfolio-gallery-hover';
                $classes[] = $params['masonry_mode'] == 'parallax' ? 'eltd-ptf-masonry-with-parallax' : '';
                $classes[] = $params['space_between_portfolio_items'] == 'yes' ? 'eltd-ptf-masonry-with-space' : '';

                break;
            case 'pinterest':
                $classes[] = 'eltd-ptf-pinterest';

                $classes[] = 'eltd-portfolio-gallery-hover';
                break;
            case 'justified-gallery':
                $classes[] = 'eltd-ptf-justified-gallery';

                $classes[] = 'eltd-portfolio-gallery-hover';

                $classes[] = $params['space_between_portfolio_items'] == 'yes' ? 'eltd-ptf-jg-with-space' : '';
                break;
        endswitch;

        if(empty($params['portfolio_slider'])) { // portfolio slider mustn't have this classes

            if($type == 'standard' || $type == 'gallery') {
                switch($columns):
                    case 'one':
                        $classes[] = 'eltd-ptf-one-column';
                        break;
                    case 'two':
                        $classes[] = 'eltd-ptf-two-columns';
                        break;
                    case 'three':
                        $classes[] = 'eltd-ptf-three-columns';
                        break;
                    case 'four':
                        $classes[] = 'eltd-ptf-four-columns';
                        break;
                    case 'five':
                        $classes[] = 'eltd-ptf-five-columns';
                        break;
                    case 'six':
                        $classes[] = 'eltd-ptf-six-columns';
                        break;
                endswitch;
            }
            if($type == 'masonry') {
                switch($masonry_columns):
                    case 'three':
                        $classes[] = 'eltd-ptf-masonry-three-columns';
                        break;
                    case 'four':
                        $classes[] = 'eltd-ptf-masonry-four-columns';
                        break;
                    case 'five':
                        $classes[] = 'eltd-ptf-masonry-five-columns';
                        break;
                endswitch;
            }
            if($params['show_load_more'] == 'yes') {
                $classes[] = 'eltd-ptf-load-more';
            }

        }

        if($type == 'pinterest') {
            switch($grid_size):
                case 'three':
                    $classes[] = 'eltd-ptf-pinterest-three-columns';
                    break;
                case 'four':
                    $classes[] = 'eltd-ptf-pinterest-four-columns';
                    break;
                case 'five':
                    $classes[] = 'eltd-ptf-pinterest-five-columns';
                    break;
            endswitch;
        }
        if($params['filter'] == 'yes') {
            $classes[] = 'eltd-ptf-has-filter';
            if($params['type'] == 'masonry' || $params['type'] == 'pinterest') {
                if($params['filter'] == 'yes') {
                    $classes[] = 'eltd-ptf-masonry-filter';
                }
            }
        }

        if(!empty($params['portfolio_slider']) && $params['portfolio_slider'] == 'yes') {
            $classes[] = 'eltd-portfolio-slider-holder';
        }

        return implode(' ', $classes);

    }

    /**
     * Generates portfolio image size
     *
     * @param $params
     *
     * @return string
     */
    public function getImageSize($params) {

        $thumb_size = 'full';
        $type       = $params['type'];

        if($type == 'standard' || $type == 'gallery') {
            if(!empty($params['image_size'])) {
                $image_size = $params['image_size'];

                switch($image_size) {
                    case 'landscape':
                        $thumb_size = 'malmo_elated_landscape';
                        break;
                    case 'portrait':
                        $thumb_size = 'malmo_elated_portrait';
                        break;
                    case 'square':
                        $thumb_size = 'malmo_elated_square';
                        break;
                    case 'full':
                        $thumb_size = 'full';
                        break;
                    case 'custom':
                        $thumb_size = 'custom';
                        break;
                    default:
                        $thumb_size = 'full';
                        break;
                }
            }
        } elseif($type == 'masonry') {

            $id           = $params['current_id'];
            $masonry_size = get_post_meta($id, 'portfolio_masonry_dimenisions', true);

            switch($masonry_size):
                case 'default' :
                    $thumb_size = 'malmo_elated_square';
                    break;
                case 'large_width' :
                    $thumb_size = 'malmo_elated_large_width';
                    break;
                case 'large_height' :
                    $thumb_size = 'malmo_elated_large_height';
                    break;
                case 'large_width_height' :
                    $thumb_size = 'malmo_elated_large_width_height';
                    break;
            endswitch;
        }


        return $thumb_size;
    }

    /**
     * Generates portfolio item categories ids.This function is used for filtering
     *
     * @param $params
     *
     * @return array
     */
    public function getItemCategories($params) {
        $id                    = $params['current_id'];
        $category_return_array = array();

        $categories = wp_get_post_terms($id, 'portfolio-category');

        foreach($categories as $cat) {
            $category_return_array[] = 'portfolio_category_'.$cat->term_id;
        }

        return implode(' ', $category_return_array);
    }

    /**
     * Generates portfolio item categories html based on id
     *
     * @param $params
     *
     * @return html
     */
    public function getItemCategoriesHtml($params) {
        $id = $params['current_id'];

        $categories    = wp_get_post_terms($id, 'portfolio-category');
        $category_html = '<h5 class="eltd-ptf-category-holder">';
        $k             = 1;
        foreach($categories as $cat) {
            $category_html .= '<span>'.$cat->name.'</span>';
            if(count($categories) != $k) {
                $category_html .= ', ';
            }
            $k++;
        }
        $category_html .= '</h5>';

        return $category_html;
    }

    /**
     * Generates masonry size class for each article( based on id)
     *
     * @param $params
     *
     * @return string
     */
    public function getMasonrySize($params) {
        $masonry_size_class = '';

        if($params['type'] == 'masonry') {

            $id           = $params['current_id'];
            $masonry_size = get_post_meta($id, 'portfolio_masonry_dimenisions', true);
            switch($masonry_size):
                case 'default' :
                    $masonry_size_class = 'eltd-default-masonry-item';
                    break;
                case 'large_width' :
                    $masonry_size_class = 'eltd-large-width-masonry-item';
                    break;
                case 'large_height' :
                    $masonry_size_class = 'eltd-large-height-masonry-item';
                    break;
                case 'large_width_height' :
                    $masonry_size_class = 'eltd-large-width-height-masonry-item';
                    break;
            endswitch;
        }

        return $masonry_size_class;
    }

    /**
     * Generates masonry parallax class for each article( based on id)
     *
     * @param $params
     *
     * @return string
     */
    public function getMasonryParallaxBehavior($params) {
        $behavior = '';

        if($params['type'] == 'masonry' && $params['masonry_mode'] == 'parallax') {

            $id               = $params['current_id'];
            $masonry_behavior = get_post_meta($id, 'portfolio_masonry_parallax_behavior', true);

            switch($masonry_behavior):
                case 'fixed' :
                    $behavior = 'eltd-masonry-parallax-fixed-item';
                    break;
                case 'floating' :
                    $behavior = 'eltd-masonry-parallax-floating-item';
                    break;
                default :
                    $behavior = 'eltd-masonry-parallax-fixed-item';
                    break;
            endswitch;
        }

        return $behavior;
    }

    /**
     * Generates filter categories array
     *
     * @param $params
     *
     *
     *
     *
     * * @return array
     */
    public function getFilterCategories($params) {

        $cat_id       = 0;
        $top_category = '';

        if(!empty($params['category'])) {

            $top_category = get_term_by('slug', $params['category'], 'portfolio-category');
            if(isset($top_category->term_id)) {
                $cat_id = $top_category->term_id;
            }

        }

        $args = array(
            'taxonomy' => 'portfolio-category',
            'child_of' => $cat_id,
            'orderby'  => $params['filter_order_by'],
        );

        $filter_categories = get_terms($args);

        return $filter_categories;

    }

    /**
     * Generates datta attributes array
     *
     * @param $params
     *
     * @return array
     */
    public function getDataAtts($params) {

        $data_attr          = array();
        $data_return_string = '';

        if(get_query_var('paged')) {
            $paged = get_query_var('paged');
        } elseif(get_query_var('page')) {
            $paged = get_query_var('page');
        } else {
            $paged = 1;
        }

        if(!empty($paged)) {
            $data_attr['data-next-page'] = $paged + 1;
        }

        if(!empty($params['type'])) {
            $data_attr['data-type'] = $params['type'];
        }

        if(!empty($params['columns'])) {
            $data_attr['data-columns'] = $params['columns'];
        }

        if(!empty($params['grid_size'])) {
            $data_attr['data-grid-size'] = $params['grid_size'];
        }

        if(!empty($params['order_by'])) {
            $data_attr['data-order-by'] = $params['order_by'];
        }

        if(!empty($params['order'])) {
            $data_attr['data-order'] = $params['order'];
        }

        if(!empty($params['number'])) {
            $data_attr['data-number'] = $params['number'];
        }

        if(!empty($params['image_size'])) {
            $data_attr['data-image-size'] = $params['image_size'];
        }

        if(!empty($params['custom_image_dimensions'])) {
            $data_attr['data-custom-image-dimensions'] = $params['custom_image_dimensions'];
        }

        if(!empty($params['filter'])) {
            $data_attr['data-filter'] = $params['filter'];
        }

        if(!empty($params['filter_order_by'])) {
            $data_attr['data-filter-order-by'] = $params['filter_order_by'];
        }

        if(!empty($params['category'])) {
            $data_attr['data-category'] = $params['category'];
        }

        if(!empty($params['selected_projectes'])) {
            $data_attr['data-selected-projects'] = $params['selected_projectes'];
        }

        if(!empty($params['show_load_more'])) {
            $data_attr['data-show-load-more'] = $params['show_load_more'];
        }

        if(!empty($params['title_tag'])) {
            $data_attr['data-title-tag'] = $params['title_tag'];
        }

        if(!empty($params['portfolio_slider']) && $params['portfolio_slider'] == 'yes') {
            $data_attr['data-items'] = $params['portfolios_shown'];
        }

        if($params['type'] == 'justified-gallery' && !empty($params['row_height']) && $params['row_height'] !== '') {
            $data_attr['data-row-height'] = $params['row_height'];
        }

        if($params['type'] == 'justified-gallery' && !empty($params['justify_last_row'])) {
            $data_attr['data-justify-last-row'] = $params['justify_last_row'];
        }

        if($params['type'] == 'justified-gallery' && !empty($params['justify_threshold']) && $params['justify_threshold'] !== '') {
            $data_attr['data-justify-treshold'] = $params['justify_threshold'];
        }

        $data_attr['data-show-excerpt'] = empty($params['show_excerpt']) ? 'no' : $params['show_excerpt'];

        foreach($data_attr as $key => $value) {
            if($key !== '') {
                $data_return_string .= $key.'= '.esc_attr($value).' ';
            }
        }

        return $data_return_string;
    }


    /**
     * Checks if portfolio has external link and returns it. Else returns link to portfolio single page
     *
     * @param $params
     *
     * @return false|mixed|string
     */
	public function getItemLink($params) {

		$portfolio_link_array = array();

		$id             = $params['current_id'];
		$portfolio_link = get_permalink($id);
		$portfolio_target = '';
		$is_external = false;

		if(get_post_meta($id, 'portfolio_external_link', true) !== '') {
			$portfolio_link = get_post_meta($id, 'portfolio_external_link', true);
			$portfolio_target = get_post_meta($id, 'portfolio_external_link_target', true);
			$is_external = true;
		}

		$portfolio_link_array['portfolio_link'] = $portfolio_link;
		$portfolio_link_array['portfolio_target'] = $portfolio_target;
		$portfolio_link_array['is_external'] = $is_external;

		return $portfolio_link_array;

	}

    private function getCustomImageSize($customImageSize) {
        $imageSize = trim($customImageSize);
        //Find digits
        preg_match_all('/\d+/', $imageSize, $matches);
        if(!empty($matches[0])) {
            return array(
                $matches[0][0],
                $matches[0][1]
            );
        }

        return false;
    }
}