<?php
namespace ElatedCore\CPT\Portfolio\Shortcodes;

use ElatedCore\Lib;
use ElatedCore\CPT\Portfolio\Lib\PortfolioQuery;

/**
 * Class PortfolioSlider
 * @package ElatedCore\CPT\Portfolio\Shortcodes
 */
class PortfolioSlider implements Lib\ShortcodeInterface {
    /**
     * @var string
     */
    private $base;

    public function __construct() {
        $this->base = 'eltd_portfolio_slider';

        add_action('vc_before_init', array($this, 'vcMap'));
    }

    /**
     * Returns base for shortcode
     * @return string
     */
    public function getBase() {
        return $this->base;
    }


    /**
     * Maps shortcode to Visual Composer
     *
     * @see vc_map()
     */
    public function vcMap() {
        if(function_exists('vc_map')) {
            vc_map(array(
                    'name'                      => esc_html__('Portfolio Slider', 'eltd_core'),
                    'base'                      => $this->base,
                    'category'                  => esc_html__('by ELATED', 'eltd_core'),
                    'icon'                      => 'icon-wpb-portfolio-slider extended-custom-icon',
                    'allowed_container_element' => 'vc_row',
                    'params'                    => array_merge(
                        array(
                            array(
                                'type'        => 'dropdown',
                                'admin_label' => true,
                                'heading'     => esc_html__('Image size', 'eltd_core'),
                                'param_name'  => 'image_size',
                                'value'       => array(
                                    esc_html__('Default', 'eltd_core')       => '',
                                    esc_html__('Original Size', 'eltd_core') => 'full',
                                    esc_html__('Square', 'eltd_core')        => 'square',
                                    esc_html__('Landscape', 'eltd_core')     => 'landscape',
                                    esc_html__('Portrait', 'eltd_core')      => 'portrait',
                                    esc_html__('Custom', 'eltd_core')        => 'custom'
                                ),
                                'description' => '',
                                'group'       => esc_html__('Layout Options', 'eltd_core')
                            ),
                            array(
                                'type'        => 'textfield',
                                'admin_label' => true,
                                'heading'     => esc_html__('Image Dimensions', 'eltd_core'),
                                'param_name'  => 'custom_image_dimensions',
                                'value'       => '',
                                'description' => esc_html__('Enter custom image dimensions. Enter image size in pixels: 200x100 (Width x Height)', 'eltd_core'),
                                'group'       => esc_html__('Layout Options', 'eltd_core'),
                                'dependency'  => array('element' => 'image_size', 'value' => 'custom')
                            ),
                            array(
                                'type'        => 'dropdown',
                                'heading'     => esc_html__('Number of Columns', 'eltd_core'),
                                'param_name'  => 'columns',
                                'admin_label' => true,
                                'value'       => array(
                                    esc_html__('One', 'eltd_core')   => '1',
                                    esc_html__('Two', 'eltd_core')   => '2',
                                    esc_html__('Three', 'eltd_core') => '3',
                                    esc_html__('Four', 'eltd_core')  => '4'
                                ),
                                'description' => esc_html__('Number of portfolios that are showing at the same time in full width (on smaller screens is responsive so there will be less items shown)', 'eltd_core'),
                                'group'       => esc_html__('Layout Options', 'eltd_core')
                            ),
                            array(
                                'type'        => 'dropdown',
                                'class'       => '',
                                'heading'     => esc_html__('Title Tag', 'eltd_core'),
                                'param_name'  => 'title_tag',
                                'value'       => array(
                                    ''   => '',
                                    'h2' => 'h2',
                                    'h3' => 'h3',
                                    'h4' => 'h4',
                                    'h5' => 'h5',
                                    'h6' => 'h6',
                                ),
                                'description' => '',
                                'group'       => 'Layout Options'
                            ),
                            array(
                                'type'        => 'dropdown',
                                'class'       => '',
                                'heading'     => esc_html__('Enable Pagination?', 'eltd_core'),
                                'param_name'  => 'enable_pagination',
                                'value'       => array(
                                    esc_html__('Yes', 'eltd_core') => 'yes',
                                    esc_html__('No', 'eltd_core')  => 'no'
                                ),
                                'save_always' => true,
                                'description' => '',
                                'group'       => esc_html__('Layout Options', 'eltd_core')
                            ),
                            array(
                                'type'        => 'textfield',
                                'class'       => '',
                                'heading'     => esc_html__('Excerpt Length', 'eltd_core'),
                                'param_name'  => 'excerpt_length',
                                'value'       => '',
                                'save_always' => true,
                                'description' => '',
                                'group'       => esc_html__('Layout Options', 'eltd_core')
                            ),
                            array(
                                'type'        => 'dropdown',
                                'class'       => '',
                                'heading'     => esc_html__('Style', 'eltd_core'),
                                'param_name'  => 'style',
                                'value'       => array(
                                    esc_html__('Default', 'eltd_core') => '',
                                    esc_html__('Light', 'eltd_core')   => 'light',
                                    esc_html__('Dark', 'eltd_core')    => 'dark'
                                ),
                                'save_always' => true,
                                'description' => '',
                                'group'       => esc_html__('Layout Options', 'eltd_core')
                            )
                        ),
                        PortfolioQuery::getInstance()->queryVCParams()
                    )
                )
            );
        }
    }

    /**
     * Renders shortcodes HTML
     *
     * @param $atts array of shortcode params
     * @param $content string shortcode content
     *
     * @return string
     */
    public function render($atts, $content = null) {
        $args = array(
            'image_size'              => 'full',
            'title_tag'               => 'h4',
            'columns'                 => '1',
            'enable_pagination'       => 'yes',
            'excerpt_length'          => '90',
            'style'                   => '',
            'custom_image_dimensions' => ''
        );

        $args   = array_merge($args, PortfolioQuery::getInstance()->getShortcodeAtts());
        $params = shortcode_atts($args, $atts);

        $query = PortfolioQuery::getInstance()->buildQueryObject($params);

        $params['query']          = $query;
        $params['holder_data']    = $this->getHolderData($params);
        $params['thumb_size']     = $this->getThumbSize($params);
        $params['caller']         = $this;
        $params['holder_classes'] = $this->getHolderClasses($params);

        $params['use_custom_image_size'] = false;
        if($params['thumb_size'] === 'custom' && !empty($params['custom_image_dimensions'])) {
            $params['use_custom_image_size'] = true;
            $params['custom_image_sizes']    = $this->getCustomImageSize($params['custom_image_dimensions']);
        }

        return eltd_core_get_shortcode_module_template_part('portfolio-slider/templates/portfolio-slider-holder', 'portfolio', '', $params);
    }

    private function getHolderData($params) {
        $data = array();

        $data['data-columns']           = $params['columns'];
        $data['data-enable-pagination'] = $params['enable_pagination'];

        return $data;
    }

    public function getThumbSize($params) {
        switch($params['image_size']) {
            case 'landscape':
                $thumbSize = 'malmo_elated_landscape';
                break;
            case 'portrait':
                $thumbSize = 'malmo_elated_portrait';
                break;
            case 'square':
                $thumbSize = 'malmo_elated_square';
                break;
            case 'full':
                $thumbSize = 'full';
                break;
            case 'custom':
                $thumbSize = 'custom';
                break;
            default:
                $thumbSize = 'full';
                break;
        }

        return $thumbSize;
    }

    public function itemExcerpt($textLength) {
        $excerpt = ($textLength > 0) ? substr(get_the_excerpt(), 0, intval($textLength)) : get_the_excerpt();

        return $excerpt;
    }

    private function getHolderClasses($params) {
        $classes = array(
            'eltd-portfolio-slider-holder',
            'eltd-carousel-pagination'
        );

        if($params['style'] !== '') {
            $classes[] = 'eltd-portfolio-slider-'.$params['style'];
        }

        return $classes;
    }

    private function getCustomImageSize($customImageSize) {
        $imageSize = trim($customImageSize);
        //Find digits
        preg_match_all('/\d+/', $imageSize, $matches);
        if(!empty($matches[0])) {
            return array(
                $matches[0][0],
                $matches[0][1]
            );
        }

        return false;
    }
}