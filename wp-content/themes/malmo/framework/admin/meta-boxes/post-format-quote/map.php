<?php

/*** Quote Post Format ***/

$quote_post_format_meta_box = malmo_elated_create_meta_box(
    array(
        'scope' => array('post'),
        'title' => esc_html__('Quote Post Format', 'malmo'),
        'name'  => 'post_format_quote_meta'
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_post_quote_text_meta',
        'type'        => 'text',
        'label'       => esc_html__('Quote Text', 'malmo'),
        'description' => esc_html__('Enter Quote text', 'malmo'),
        'parent'      => $quote_post_format_meta_box,

    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_post_quote_author_meta',
        'type'        => 'text',
        'label'       => esc_html__('Author', 'malmo'),
        'description' => esc_html__('Enter the name of the author of the quote', 'malmo'),
        'parent'      => $quote_post_format_meta_box,

    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_post_quote_position_meta',
        'type'        => 'text',
        'label'       => esc_html__('Author Role', 'malmo'),
        'description' => esc_html__('Enter the author\'s role, job, position, profession...', 'malmo'),
        'parent'      => $quote_post_format_meta_box,

    )
);
