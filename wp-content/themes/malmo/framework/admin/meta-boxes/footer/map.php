<?php

$footer_meta_box = malmo_elated_create_meta_box(
    array(
        'scope' => array('page', 'portfolio-item', 'post'),
        'title' => esc_html__('Footer', 'malmo'),
        'name'  => 'footer_meta'
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'          => 'eltd_enable_footer_image_meta',
        'type'          => 'yesno',
        'default_value' => 'no',
        'label'         => esc_html__('Disable Footer Image for this Page', 'malmo'),
        'description'   => esc_html__('Enabling this option will hide footer image on this page', 'malmo'),
        'parent'        => $footer_meta_box,
        'args'          => array(
            'dependence'             => true,
            'dependence_hide_on_yes' => '#eltd_eltd_footer_background_image_meta',
            'dependence_show_on_yes' => ''
        )
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'          => 'eltd_footer_style_meta',
        'label'         => esc_html__('Footer Skin', 'malmo'),
        'type'          => 'select',
        'default_value' => '',
        'description'   => esc_html__('Choose Footer Skin on single page', 'malmo'),
        'parent'        => $footer_meta_box,
        'options'       => array(
            ''               => '',
            'dark-footer'    => esc_html__('Dark', 'malmo'),
            'light-footer'   => esc_html__('Light', 'malmo')
        )
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'            => 'eltd_footer_background_image_meta',
        'type'            => 'image',
        'label'           => esc_html__('Background Image', 'malmo'),
        'description'     => esc_html__('Choose Background Image for Footer Area on this page', 'malmo'),
        'parent'          => $footer_meta_box,
        'hidden_property' => 'eltd_enable_footer_background_image_meta',
        'hidden_value'    => 'yes'
    )
);
malmo_elated_create_meta_box_field(
    array(
        'name'          => 'eltd_disable_footer_meta',
        'type'          => 'yesno',
        'default_value' => 'no',
        'label'         => esc_html__('Disable Footer for this Page', 'malmo'),
        'description'   => esc_html__('Enabling this option will hide footer on this page', 'malmo'),
        'parent'        => $footer_meta_box,
    )
);