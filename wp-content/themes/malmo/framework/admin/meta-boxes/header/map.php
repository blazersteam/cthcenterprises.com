<?php

$header_meta_box = malmo_elated_create_meta_box(
    array(
        'scope' => array('page', 'portfolio-item', 'post'),
        'title' => esc_html__('Header', 'malmo'),
        'name'  => 'header_meta'
    )
);

$temp_holder_show             = '';
$temp_holder_hide             = '';
$temp_array_standard          = array();
$temp_array_minimal           = array();
$temp_array_centered          = array();
$temp_array_vertical          = array();
$temp_array_top_header        = array();
$temp_array_behaviour         = array();
switch(malmo_elated_options()->getOptionValue('header_type')) {

    case 'header-standard':
        $temp_holder_show = '#eltd_eltd_header_standard_type_meta_container, #eltd_eltd_header_behaviour_meta';
        $temp_holder_hide = '#eltd_eltd_header_vertical_type_meta_container, #eltd_eltd_header_minimal_type_meta_container, #eltd_eltd_header_centered_type_meta_container';

        $temp_array_standard = array(
            'hidden_value'  => 'default',
            'hidden_values' => array(
                'header-vertical',
                'header-minimal',
                'header-centered'
            )
        );

        $temp_array_minimal = array(
            'hidden_value'  => 'default',
            'hidden_values' => array(
                '',
                'header-standard',
                'header-vertical',
                'header-centered'
            )
        );

        $temp_array_centered = array(
            'hidden_value'  => 'default',
            'hidden_values' => array(
                '',
                'header-standard',
                'header-minimal',
                'header-vertical'
            )
        );

        $temp_array_vertical = array(
            'hidden_values' => array(
                '',
                'header-standard',
                'header-minimal',
                'header-centered'
            )
        );

        $temp_array_behaviour = array(
            'hidden_values' => array('header-vertical')
        );

        $temp_array_top_header = array(
            'hidden_value'  => 'default',
            'hidden_values' => array('header-vertical')
        );
        break;

    case 'header-minimal':
        $temp_holder_show = '#eltd_eltd_header_minimal_type_meta_container, #eltd_eltd_header_behaviour_meta';
        $temp_holder_hide = '#eltd_eltd_header_vertical_type_meta_container, #eltd_eltd_header_standard_type_meta_container, #eltd_eltd_header_centered_type_meta_container';

        $temp_array_standard = array(
            'hidden_value'  => 'default',
            'hidden_values' => array(
                '',
                'header-vertical',
                'header-minimal',
                'header-centered'
            )
        );

        $temp_array_minimal = array(
            'hidden_value'  => 'default',
            'hidden_values' => array(
                'header-standard',
                'header-vertical',
                'header-centered'
            )
        );

        $temp_array_centered = array(
            'hidden_value'  => 'default',
            'hidden_values' => array(
                '',
                'header-standard',
                'header-minimal',
                'header-vertical'
            )
        );

        $temp_array_vertical = array(
            'hidden_values' => array(
                '',
                'header-standard',
                'header-minimal',
                'header-centered'
            )
        );

        $temp_array_behaviour = array(
            'hidden_values' => array('header-vertical')
        );

        $temp_array_top_header = array(
            'hidden_value'  => 'default',
            'hidden_values' => array('header-vertical')
        );
        break;

    case 'header-centered':
        $temp_holder_show = '#eltd_eltd_header_centered_type_meta_container, #eltd_eltd_header_behaviour_meta';
        $temp_holder_hide = '#eltd_eltd_header_vertical_type_meta_container, #eltd_eltd_header_standard_type_meta_container, #eltd_eltd_header_minimal_type_meta_container';

        $temp_array_standard = array(
            'hidden_value'  => 'default',
            'hidden_values' => array(
                '',
                'header-vertical',
                'header-minimal',
                'header-centered'
            )
        );

        $temp_array_minimal = array(
            'hidden_value'  => 'default',
            'hidden_values' => array(
                '',
                'header-standard',
                'header-vertical',
                'header-centered'
            )
        );

        $temp_array_centered = array(
            'hidden_value'  => 'default',
            'hidden_values' => array(
                'header-standard',
                'header-minimal',
                'header-vertical'
            )
        );

        $temp_array_vertical = array(
            'hidden_values' => array(
                '',
                'header-standard',
                'header-minimal',
                'header-centered'
            )
        );

        $temp_array_behaviour = array(
            'hidden_values' => array('header-vertical')
        );

        $temp_array_top_header = array(
            'hidden_value'  => 'default',
            'hidden_values' => array('header-vertical')
        );
        break;

    case 'header-vertical':
        $temp_holder_show = '#eltd_eltd_header_vertical_type_meta_container';
        $temp_holder_hide = '#eltd_eltd_header_standard_type_meta_container, #eltd_eltd_header_minimal_type_meta_container, #eltd_eltd_header_behaviour_meta, #eltd_eltd_header_centered_type_meta_container';

        $temp_array_standard = array(
            'hidden_values' => array(
                '',
                'header-vertical',
                'header-minimal',
                'header-centered',
            )
        );

        $temp_array_minimal = array(
            'hidden_values' => array(
                '',
                'header-vertical',
                'header-standard',
                'header-centered',
            )
        );

        $temp_array_centered = array(
            'hidden_values' => array(
                '',
                'header-standard',
                'header-minimal',
                'header-vertical',
            )
        );

        $temp_array_vertical = array(
            'hidden_value'  => 'default',
            'hidden_values' => array(
                'header-standard',
                'header-minimal',
                'header-centered',
            )
        );

        $temp_array_behaviour = array(
            'hidden_values' => array('', 'header-vertical')
        );

        $temp_array_top_header = array(
            'hidden_value'  => 'default',
            'hidden_values' => array('', 'header-vertical')
        );
        break;
}

malmo_elated_create_meta_box_field(
    array(
        'name'          => 'eltd_header_type_meta',
        'type'          => 'select',
        'default_value' => '',
        'label'         => esc_html__('Choose Header Type', 'malmo'),
        'description'   => esc_html__('Select header type layout', 'malmo'),
        'parent'        => $header_meta_box,
        'options'       => array(
            ''                         => 'Default',
            'header-standard'          => esc_html__('Standard Header', 'malmo'),
            'header-minimal'           => esc_html__('Minimal Header', 'malmo'),
            'header-centered'          => esc_html__('Centered Header', 'malmo'),
            'header-vertical'          => esc_html__('Vertical Header', 'malmo')
        ),
        'args'          => array(
            "dependence" => true,
            "hide"       => array(
                ""                         => $temp_holder_hide,
                'header-standard'          => '#eltd_eltd_header_vertical_type_meta_container, #eltd_eltd_header_minimal_type_meta_container, #eltd_eltd_header_centered_type_meta_container',
                'header-minimal'           => '#eltd_eltd_header_vertical_type_meta_container, #eltd_eltd_header_standard_type_meta_container, #eltd_eltd_header_centered_type_meta_container',
                'header-centered'          => '#eltd_eltd_header_standard_type_meta_container, #eltd_eltd_header_vertical_type_meta_container, #eltd_eltd_header_minimal_type_meta_container',
                'header-vertical'          => '#eltd_eltd_header_standard_type_meta_container, #eltd_eltd_header_minimal_type_meta_container, #eltd_eltd_top_bar_container_meta_container, #eltd_eltd_header_behaviour_meta, #eltd_eltd_header_centered_type_meta_container',
            ),
            "show"       => array(
                ""                         => $temp_holder_show,
                "header-standard"          => '#eltd_eltd_header_standard_type_meta_container, #eltd_eltd_top_bar_container_meta_container, #eltd_eltd_header_behaviour_meta',
                "header-minimal"           => '#eltd_eltd_header_minimal_type_meta_container, #eltd_eltd_top_bar_container_meta_container, #eltd_eltd_header_behaviour_meta',
                'header-centered'          => '#eltd_eltd_header_centered_type_meta_container, #eltd_eltd_top_bar_container_meta_container, #eltd_eltd_header_behaviour_meta',
                "header-vertical"          => '#eltd_eltd_header_vertical_type_meta_container',
            )
        )
    )
);

malmo_elated_create_meta_box_field(
    array_merge(
        array(
            'parent'          => $header_meta_box,
            'type'            => 'select',
            'name'            => 'eltd_header_behaviour_meta',
            'default_value'   => '',
            'label'           => esc_html__('Choose Header behaviour', 'malmo'),
            'description'     => esc_html__('Select the behaviour of header when you scroll down to page', 'malmo'),
            'options'         => array(
                ''                                => '',
                'no-behavior'                     => esc_html__('No Behavior', 'malmo'),
                'sticky-header-on-scroll-up'      => esc_html__('Sticky on scrol up', 'malmo'),
                'sticky-header-on-scroll-down-up' => esc_html__('Sticky on scrol up/down', 'malmo'),
                'fixed-on-scroll'                 => esc_html__('Fixed on scroll', 'malmo')
            ),
            'hidden_property' => 'eltd_header_type_meta',
            'hidden_value'    => '',
            'args'            => array(
                'dependence' => true,
                'show'       => array(
                    ''                                => '',
                    'sticky-header-on-scroll-up'      => '',
                    'sticky-header-on-scroll-down-up' => '#eltd_eltd_sticky_amount_container_meta_container',
                    'no-behavior'                     => ''
                ),
                'hide'       => array(
                    ''                                => '#eltd_eltd_sticky_amount_container_meta_container',
                    'sticky-header-on-scroll-up'      => '#eltd_eltd_sticky_amount_container_meta_container',
                    'sticky-header-on-scroll-down-up' => '',
                    'no-behavior'                     => '#eltd_eltd_sticky_amount_container_meta_container'
                )
            )
        ),
        $temp_array_behaviour
    )
);

$sticky_amount_container = malmo_elated_add_admin_container(
    array(
        'parent'          => $header_meta_box,
        'name'            => 'eltd_sticky_amount_container_meta_container',
        'hidden_property' => 'eltd_header_behaviour_meta',
        'hidden_value'    => '',
        'hidden_values'   => array('', 'no-behavior', 'sticky-header-on-scroll-up'),
    )
);

$sticky_amount_group = malmo_elated_add_admin_group(array(
    'name'        => 'sticky_amount_group',
    'title'       => esc_html__('Scroll Amount for Sticky Header Appearance', 'malmo'),
    'parent'      => $sticky_amount_container,
    'description' => esc_html__('Enter the amount of pixels for sticky header appearance, or set browser height to "Yes" for predefined sticky header appearance amount', 'malmo')
));

$sticky_amount_row = malmo_elated_add_admin_row(array(
    'name'   => 'sticky_amount_group',
    'parent' => $sticky_amount_group
));

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_scroll_amount_for_sticky_meta',
        'type'   => 'textsimple',
        'label'  => esc_html__('Amount in px', 'malmo'),
        'parent' => $sticky_amount_row,
        'args'   => array(
            'suffix' => 'px'
        )
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'          => 'eltd_scroll_amount_for_sticky_fullscreen_meta',
        'type'          => 'yesnosimple',
        'label'         => esc_html__('Browser Height', 'malmo'),
        'default_value' => 'no',
        'parent'        => $sticky_amount_row
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'          => 'eltd_header_style_meta',
        'type'          => 'select',
        'default_value' => '',
        'label'         => esc_html__('Header Skin', 'malmo'),
        'description'   => esc_html__('Choose a header style to make header elements (logo, main menu, side menu button) in that predefined style', 'malmo'),
        'parent'        => $header_meta_box,
        'options'       => array(
            ''             => '',
            'light-header' => esc_html__('Light', 'malmo'),
            'dark-header'  => esc_html__('Dark', 'malmo')
        )
    )
);

malmo_elated_create_meta_box_field(
    array(
        'parent'        => $header_meta_box,
        'type'          => 'select',
        'name'          => 'eltd_enable_header_style_on_scroll_meta',
        'default_value' => '',
        'label'         => esc_html__('Enable Header Style on Scroll', 'malmo'),
        'description'   => esc_html__('Enabling this option, header will change style depending on row settings for dark/light style', 'malmo'),
        'options'       => array(
            ''    => '',
            'no'  => esc_html__('No', 'malmo'),
            'yes' => esc_html__('Yes', 'malmo')
        )
    )
);

$header_standard_type_meta_container = malmo_elated_add_admin_container(
    array_merge(
        array(
            'parent'          => $header_meta_box,
            'name'            => 'eltd_header_standard_type_meta_container',
            'hidden_property' => 'eltd_header_type_meta',

        ),
        $temp_array_standard
    )
);

malmo_elated_create_meta_box_field(array(
    'name'          => 'eltd_menu_area_in_grid_header_standard_meta',
    'type'          => 'select',
    'label'         => esc_html__('Header In Grid', 'malmo'),
    'description'   => esc_html__('Set header content to be in grid', 'malmo'),
    'parent'        => $header_standard_type_meta_container,
    'default_value' => '',
    'options'       => array(
        ''    => esc_html__('Default', 'malmo'),
        'no'  => esc_html__('No', 'malmo'),
        'yes' => esc_html__('Yes', 'malmo')
    ),
    'args'          => array(
        'dependence' => true,
        'hide'       => array(
            ''    => '#eltd_menu_area_in_grid_header_standard_container',
            'no'  => '#eltd_menu_area_in_grid_header_standard_container',
            'yes' => ''
        ),
        'show'       => array(
            ''    => '',
            'no'  => '',
            'yes' => '#eltd_menu_area_in_grid_header_standard_container'
        )
    )
));

$menu_area_in_grid_header_standard_container = malmo_elated_add_admin_container(array(
    'type'            => 'container',
    'name'            => 'menu_area_in_grid_header_standard_container',
    'parent'          => $header_standard_type_meta_container,
    'hidden_property' => 'eltd_menu_area_in_grid_header_standard_meta',
    'hidden_value'    => 'no',
    'hidden_values'   => array('', 'no')
));


malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_menu_area_grid_background_color_header_standard_meta',
        'type'        => 'color',
        'label'       => esc_html__('Grid Background Color', 'malmo'),
        'description' => esc_html__('Set grid background color for header area', 'malmo'),
        'parent'      => $menu_area_in_grid_header_standard_container
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_menu_area_grid_background_transparency_header_standard_meta',
        'type'        => 'text',
        'label'       => esc_html__('Grid Background Transparency', 'malmo'),
        'description' => esc_html__('Set grid background transparency for header (0 = fully transparent, 1 = opaque)', 'malmo'),
        'parent'      => $menu_area_in_grid_header_standard_container,
        'args'        => array(
            'col_width' => 2
        )
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_menu_area_background_color_header_standard_meta',
        'type'        => 'color',
        'label'       => esc_html__('Background Color', 'malmo'),
        'description' => esc_html__('Choose a background color for header area', 'malmo'),
        'parent'      => $header_standard_type_meta_container
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_menu_area_background_transparency_header_standard_meta',
        'type'        => 'text',
        'label'       => esc_html__('Transparency', 'malmo'),
        'description' => esc_html__('Choose a transparency for the header background color (0 = fully transparent, 1 = opaque)', 'malmo'),
        'parent'      => $header_standard_type_meta_container,
        'args'        => array(
            'col_width' => 2
        )
    )
);

$header_minimal_type_meta_container = malmo_elated_add_admin_container(
    array_merge(
        array(
            'parent'          => $header_meta_box,
            'name'            => 'eltd_header_minimal_type_meta_container',
            'hidden_property' => 'eltd_header_type_meta',

        ),
        $temp_array_minimal
    )
);

malmo_elated_create_meta_box_field(array(
    'name'          => 'eltd_menu_area_in_grid_header_minimal_meta',
    'type'          => 'select',
    'label'         => esc_html__('Header In Grid', 'malmo'),
    'description'   => esc_html__('Set header content to be in grid', 'malmo'),
    'parent'        => $header_minimal_type_meta_container,
    'default_value' => '',
    'options'       => array(
        ''    => esc_html__('Default', 'malmo'),
        'no'  => esc_html__('No', 'malmo'),
        'yes' => esc_html__('Yes', 'malmo')
    ),
    'args'          => array(
        'dependence' => true,
        'hide'       => array(
            ''    => '#eltd_menu_area_in_grid_header_minimal_container',
            'no'  => '#eltd_menu_area_in_grid_header_minimal_container',
            'yes' => ''
        ),
        'show'       => array(
            ''    => '',
            'no'  => '',
            'yes' => '#eltd_menu_area_in_grid_header_minimal_container'
        )
    )
));

$menu_area_in_grid_header_minimal_container = malmo_elated_add_admin_container(array(
    'type'            => 'container',
    'name'            => 'menu_area_in_grid_header_minimal_container',
    'parent'          => $header_minimal_type_meta_container,
    'hidden_property' => 'eltd_menu_area_in_grid_header_minimal_meta',
    'hidden_value'    => 'no',
    'hidden_values'   => array('', 'no')
));


malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_menu_area_grid_background_color_header_minimal_meta',
        'type'        => 'color',
        'label'       => esc_html__('Grid Background Color', 'malmo'),
        'description' => esc_html__('Set grid background color for header area', 'malmo'),
        'parent'      => $menu_area_in_grid_header_minimal_container
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_menu_area_grid_background_transparency_header_minimal_meta',
        'type'        => 'text',
        'label'       => esc_html__('Grid Background Transparency', 'malmo'),
        'description' => esc_html__('Set grid background transparency for header (0 = fully transparent, 1 = opaque)', 'malmo'),
        'parent'      => $menu_area_in_grid_header_minimal_container,
        'args'        => array(
            'col_width' => 2
        )
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_menu_area_background_color_header_minimal_meta',
        'type'        => 'color',
        'label'       => esc_html__('Background Color', 'malmo'),
        'description' => esc_html__('Choose a background color for header area', 'malmo'),
        'parent'      => $header_minimal_type_meta_container
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_menu_area_background_transparency_header_minimal_meta',
        'type'        => 'text',
        'label'       => esc_html__('Transparency', 'malmo'),
        'description' => esc_html__('Choose a transparency for the header background color (0 = fully transparent, 1 = opaque)', 'malmo'),
        'parent'      => $header_minimal_type_meta_container,
        'args'        => array(
            'col_width' => 2
        )
    )
);

$header_centered_type_meta_container = malmo_elated_add_admin_container(
    array_merge(
        array(
            'parent'          => $header_meta_box,
            'name'            => 'eltd_header_centered_type_meta_container',
            'hidden_property' => 'eltd_header_type_meta',

        ),
        $temp_array_centered
    )
);

malmo_elated_add_admin_section_title(array(
    'name'   => 'logo_area_centered_title',
    'parent' => $header_centered_type_meta_container,
    'title'  => esc_html__('Logo Area', 'malmo')
));

malmo_elated_create_meta_box_field(array(
    'name'          => 'eltd_logo_area_in_grid_header_centered_meta',
    'type'          => 'select',
    'label'         => esc_html__('Logo Area In Grid', 'malmo'),
    'description'   => esc_html__('Set logo area content to be in grid', 'malmo'),
    'parent'        => $header_centered_type_meta_container,
    'default_value' => '',
    'options'       => array(
        ''    => esc_html__('Default', 'malmo'),
        'no'  => esc_html__('No', 'malmo'),
        'yes' => esc_html__('Yes', 'malmo')
    ),
    'args'          => array(
        'dependence' => true,
        'hide'       => array(
            ''    => '#eltd_logo_area_in_grid_header_centered_container',
            'no'  => '#eltd_logo_area_in_grid_header_centered_container',
            'yes' => ''
        ),
        'show'       => array(
            ''    => '',
            'no'  => '',
            'yes' => '#eltd_logo_area_in_grid_header_centered_container'
        )
    )
));

$logo_area_in_grid_header_centered_container = malmo_elated_add_admin_container(array(
    'type'            => 'container',
    'name'            => 'logo_area_in_grid_header_centered_container',
    'parent'          => $header_centered_type_meta_container,
    'hidden_property' => 'eltd_logo_area_in_grid_header_centered_meta',
    'hidden_value'    => 'no',
    'hidden_values'   => array('', 'no')
));


malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_logo_area_grid_background_color_header_centered_meta',
        'type'        => 'color',
        'label'       => esc_html__('Grid Background Color', 'malmo'),
        'description' => esc_html__('Set grid background color for logo area', 'malmo'),
        'parent'      => $logo_area_in_grid_header_centered_container
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_logo_area_grid_background_transparency_header_centered_meta',
        'type'        => 'text',
        'label'       => esc_html__('Grid Background Transparency', 'malmo'),
        'description' => esc_html__('Set grid background transparency for logo area (0 = fully transparent, 1 = opaque)', 'malmo'),
        'parent'      => $logo_area_in_grid_header_centered_container,
        'args'        => array(
            'col_width' => 2
        )
    )
);

malmo_elated_create_meta_box_field(array(
    'name'          => 'eltd_logo_area_in_grid_border_header_centered_meta',
    'type'          => 'select',
    'label'         => esc_html__('Grid Area Border', 'malmo'),
    'description'   => esc_html__('Set border on grid area', 'malmo'),
    'parent'        => $logo_area_in_grid_header_centered_container,
    'default_value' => '',
    'options'       => array(
        ''    => '',
        'no'  => esc_html__('No', 'malmo'),
        'yes' => esc_html__('Yes', 'malmo')
    ),
    'args'          => array(
        'dependence' => true,
        'hide'       => array(
            ''    => '#eltd_logo_area_in_grid_border_header_centered_container',
            'no'  => '#eltd_logo_area_in_grid_border_header_centered_container',
            'yes' => ''
        ),
        'show'       => array(
            ''    => '',
            'no'  => '',
            'yes' => '#eltd_logo_area_in_grid_border_header_centered_container'
        )
    )
));

$logo_area_in_grid_border_header_centered_container = malmo_elated_add_admin_container(array(
    'type'            => 'container',
    'name'            => 'logo_area_in_grid_border_header_centered_container',
    'parent'          => $logo_area_in_grid_header_centered_container,
    'hidden_property' => 'eltd_logo_area_in_grid_border_header_centered_meta',
    'hidden_value'    => 'no',
    'hidden_values'   => array('', 'no')
));

malmo_elated_create_meta_box_field(array(
    'name'        => 'eltd_logo_area_in_grid_border_color_header_centered_meta',
    'type'        => 'color',
    'label'       => esc_html__('Border Color', 'malmo'),
    'description' => esc_html__('Set border color for grid area', 'malmo'),
    'parent'      => $logo_area_in_grid_border_header_centered_container
));


malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_logo_area_background_color_header_centered_meta',
        'type'        => 'color',
        'label'       => esc_html__('Background Color', 'malmo'),
        'description' => esc_html__('Choose a background color for logo area', 'malmo'),
        'parent'      => $header_centered_type_meta_container
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_logo_area_background_transparency_header_centered_meta',
        'type'        => 'text',
        'label'       => esc_html__('Transparency', 'malmo'),
        'description' => esc_html__('Choose a transparency for the logo area background color (0 = fully transparent, 1 = opaque)', 'malmo'),
        'parent'      => $header_centered_type_meta_container,
        'args'        => array(
            'col_width' => 2
        )
    )
);

malmo_elated_create_meta_box_field(array(
    'name'          => 'eltd_logo_area_border_header_centered_meta',
    'type'          => 'select',
    'label'         => esc_html__('Logo Area Border', 'malmo'),
    'description'   => esc_html__('Set border on logo area', 'malmo'),
    'parent'        => $header_centered_type_meta_container,
    'default_value' => '',
    'options'       => array(
        ''    => '',
        'no'  => esc_html__('No', 'malmo'),
        'yes' => esc_html__('Yes', 'malmo')
    ),
    'args'          => array(
        'dependence' => true,
        'hide'       => array(
            ''    => '#eltd_logo_border_bottom_color_container',
            'no'  => '#eltd_logo_border_bottom_color_container',
            'yes' => ''
        ),
        'show'       => array(
            ''    => '',
            'no'  => '',
            'yes' => '#eltd_logo_border_bottom_color_container'
        )
    )
));

$border_bottom_color_centered_container = malmo_elated_add_admin_container(array(
    'type'            => 'container',
    'name'            => 'logo_border_bottom_color_container',
    'parent'          => $header_centered_type_meta_container,
    'hidden_property' => 'eltd_logo_area_border_header_centered_meta',
    'hidden_value'    => 'no',
    'hidden_values'   => array('', 'no')
));

malmo_elated_create_meta_box_field(array(
    'name'        => 'eltd_logo_area_border_color_header_centered_meta',
    'type'        => 'color',
    'label'       => esc_html__('Border Color', 'malmo'),
    'description' => esc_html__('Choose color of logo area bottom border', 'malmo'),
    'parent'      => $border_bottom_color_centered_container
));

malmo_elated_add_admin_section_title(array(
    'name'   => 'menu_area_centered_title',
    'parent' => $header_centered_type_meta_container,
    'title'  => esc_html__('Menu Area', 'malmo')
));

malmo_elated_create_meta_box_field(array(
    'name'          => 'eltd_menu_area_in_grid_header_centered_meta',
    'type'          => 'select',
    'label'         => esc_html__('Menu Area In Grid', 'malmo'),
    'description'   => esc_html__('Set menu area content to be in grid', 'malmo'),
    'parent'        => $header_centered_type_meta_container,
    'default_value' => '',
    'options'       => array(
        ''    => esc_html__('Default', 'malmo'),
        'no'  => esc_html__('No', 'malmo'),
        'yes' => esc_html__('Yes', 'malmo')
    ),
    'args'          => array(
        'dependence' => true,
        'hide'       => array(
            ''    => '#eltd_menu_area_in_grid_header_centered_container',
            'no'  => '#eltd_menu_area_in_grid_header_centered_container',
            'yes' => ''
        ),
        'show'       => array(
            ''    => '',
            'no'  => '',
            'yes' => '#eltd_menu_area_in_grid_header_centered_container'
        )
    )
));

$menu_area_in_grid_header_centered_container = malmo_elated_add_admin_container(array(
    'type'            => 'container',
    'name'            => 'menu_area_in_grid_header_centered_container',
    'parent'          => $header_centered_type_meta_container,
    'hidden_property' => 'eltd_menu_area_in_grid_header_centered_meta',
    'hidden_value'    => 'no',
    'hidden_values'   => array('', 'no')
));


malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_menu_area_grid_background_color_header_centered_meta',
        'type'        => 'color',
        'label'       => esc_html__('Grid Background Color', 'malmo'),
        'description' => esc_html__('Set grid background color for menu area', 'malmo'),
        'parent'      => $menu_area_in_grid_header_centered_container
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_menu_area_grid_background_transparency_header_centered_meta',
        'type'        => 'text',
        'label'       => esc_html__('Grid Background Transparency', 'malmo'),
        'description' => esc_html__('Set grid background transparency for menu area (0 = fully transparent, 1 = opaque)', 'malmo'),
        'parent'      => $menu_area_in_grid_header_centered_container,
        'args'        => array(
            'col_width' => 2
        )
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_menu_area_background_color_header_centered_meta',
        'type'        => 'color',
        'label'       => esc_html__('Background Color', 'malmo'),
        'description' => esc_html__('Choose a background color for menu area', 'malmo'),
        'parent'      => $header_centered_type_meta_container
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_menu_area_background_transparency_header_centered_meta',
        'type'        => 'text',
        'label'       => esc_html__('Transparency', 'malmo'),
        'description' => esc_html__('Choose a transparency for the menu area background color (0 = fully transparent, 1 = opaque)', 'malmo'),
        'parent'      => $header_centered_type_meta_container,
        'args'        => array(
            'col_width' => 2
        )
    )
);

$top_bar_container = malmo_elated_add_admin_container(
    array_merge(
        array(
            'parent'          => $header_meta_box,
            'name'            => 'eltd_top_bar_container_meta_container',
            'hidden_property' => 'eltd_header_type_meta',

        ),
        $temp_array_top_header
    )
);

malmo_elated_add_admin_section_title(array(
    'name'   => 'top_bar_section_title',
    'parent' => $top_bar_container,
    'title'  => esc_html__('Top Bar', 'malmo')
));

$top_bar_global_option = malmo_elated_options()->getOptionValue('top_bar');

$top_bar_default_dependency = array(
    '' => '#eltd_top_bar_container_no_style'
);

$top_bar_show_array = array(
    'yes' => '#eltd_top_bar_container_no_style'
);

$top_bar_hide_array = array(
    'no' => '#eltd_top_bar_container_no_style'
);

if($top_bar_global_option === 'yes') {
    $top_bar_show_array           = array_merge($top_bar_show_array, $top_bar_default_dependency);
    $top_bar_container_hide_array = array('no');
} else {
    $top_bar_hide_array           = array_merge($top_bar_hide_array, $top_bar_default_dependency);
    $top_bar_container_hide_array = array('', 'no');
}


malmo_elated_create_meta_box_field(array(
    'name'          => 'eltd_top_bar_meta',
    'type'          => 'select',
    'label'         => esc_html__('Enable Top Bar on This Page', 'malmo'),
    'description'   => esc_html__('Enabling this option will enable top bar on this page', 'malmo'),
    'parent'        => $top_bar_container,
    'default_value' => '',
    'options'       => array(
        ''    => esc_html__('Default', 'malmo'),
        'yes' => esc_html__('Yes', 'malmo'),
        'no'  => esc_html__('No', 'malmo')
    ),
    'args'          => array(
        'dependence' => true,
        'show'       => $top_bar_show_array,
        'hide'       => $top_bar_hide_array
    )
));

$top_bar_container = malmo_elated_add_admin_container_no_style(array(
    'name'            => 'top_bar_container_no_style',
    'parent'          => $top_bar_container,
    'hidden_property' => 'eltd_top_bar_meta',
    'hidden_value'    => 'no',
    'hidden_values'   => $top_bar_container_hide_array
));

malmo_elated_create_meta_box_field(array(
    'name'          => 'eltd_top_bar_in_grid_meta',
    'type'          => 'select',
    'label'         => esc_html__('Top Bar In Grid', 'malmo'),
    'description'   => esc_html__('Set top bar content to be in grid', 'malmo'),
    'parent'        => $top_bar_container,
    'default_value' => '',
    'options'       => array(
        ''    => '',
        'no'  => esc_html__('No', 'malmo'),
        'yes' => esc_html__('Yes', 'malmo')
    )
));

malmo_elated_create_meta_box_field(array(
    'name'    => 'eltd_top_bar_skin_meta',
    'type'    => 'select',
    'label'   => esc_html__('Top Bar Skin', 'malmo'),
    'options' => array(
        ''      => esc_html__('Default', 'malmo'),
        'light' => esc_html__('White', 'malmo'),
        'dark'  => esc_html__('Black', 'malmo'),
        'gray'  => esc_html__('Gray', 'malmo'),
    ),
    'parent'  => $top_bar_container
));

malmo_elated_create_meta_box_field(array(
    'name'   => 'eltd_top_bar_background_color_meta',
    'type'   => 'color',
    'label'  => esc_html__('Top Bar Background Color', 'malmo'),
    'parent' => $top_bar_container
));

malmo_elated_create_meta_box_field(array(
    'name'        => 'eltd_top_bar_background_transparency_meta',
    'type'        => 'text',
    'label'       => esc_html__('Top Bar Background Color Transparency', 'malmo'),
    'description' => esc_html__('Set top bar background color transparenct. Value should be between 0 and 1', 'malmo'),
    'parent'      => $top_bar_container,
    'args'        => array(
        'col_width' => 3
    )
));

malmo_elated_create_meta_box_field(array(
    'name'          => 'eltd_top_bar_border_meta',
    'type'          => 'select',
    'label'         => esc_html__('Top Bar Border', 'malmo'),
    'description'   => esc_html__('Set border on top bar', 'malmo'),
    'parent'        => $top_bar_container,
    'default_value' => '',
    'options'       => array(
        ''    => '',
        'no'  => esc_html__('No', 'malmo'),
        'yes' => esc_html__('Yes', 'malmo')
    ),
    'args'          => array(
        'dependence' => true,
        'hide'       => array(
            ''    => '#eltd_top_bar_border_container',
            'no'  => '#eltd_top_bar_border_container',
            'yes' => ''
        ),
        'show'       => array(
            ''    => '',
            'no'  => '',
            'yes' => '#eltd_top_bar_border_container'
        )
    )
));

$top_bar_border_container = malmo_elated_add_admin_container(array(
    'type'            => 'container',
    'name'            => 'top_bar_border_container',
    'parent'          => $top_bar_container,
    'hidden_property' => 'eltd_top_bar_border_meta',
    'hidden_value'    => 'no',
    'hidden_values'   => array('', 'no')
));

malmo_elated_create_meta_box_field(array(
    'name'        => 'eltd_top_bar_border_color_meta',
    'type'        => 'color',
    'label'       => esc_html__('Border Color', 'malmo'),
    'description' => esc_html__('Choose color for top bar border', 'malmo'),
    'parent'      => $top_bar_border_container
));

$header_vertical_type_meta_container = malmo_elated_add_admin_container(
    array_merge(
        array(
            'parent'          => $header_meta_box,
            'name'            => 'eltd_header_vertical_type_meta_container',
            'hidden_property' => 'eltd_header_type_meta, sticky_amount_group'
        ),
        $temp_array_vertical
    )
);

malmo_elated_create_meta_box_field(array(
    'name'        => 'eltd_vertical_header_background_color_meta',
    'type'        => 'color',
    'label'       => esc_html__('Background Color', 'malmo'),
    'description' => esc_html__('Set background color for vertical menu', 'malmo'),
    'parent'      => $header_vertical_type_meta_container
));

malmo_elated_create_meta_box_field(
    array(
        'name'          => 'eltd_vertical_header_background_image_meta',
        'type'          => 'image',
        'default_value' => '',
        'label'         => esc_html__('Background Image', 'malmo'),
        'description'   => esc_html__('Set background image for vertical menu', 'malmo'),
        'parent'        => $header_vertical_type_meta_container
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'          => 'eltd_disable_vertical_header_background_image_meta',
        'type'          => 'yesno',
        'default_value' => 'no',
        'label'         => esc_html__('Disable Background Image', 'malmo'),
        'description'   => esc_html__('Enabling this option will hide background image in Vertical Menu', 'malmo'),
        'parent'        => $header_vertical_type_meta_container
    )
);