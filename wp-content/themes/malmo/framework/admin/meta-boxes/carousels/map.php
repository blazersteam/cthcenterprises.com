<?php

//Carousels

$carousel_meta_box = malmo_elated_create_meta_box(
    array(
        'scope' => array('carousels'),
        'title' => esc_html__('Carousel', 'malmo'),
        'name'  => 'carousel_meta'
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_carousel_image',
        'type'        => 'image',
        'label'       => esc_html__('Carousel Image', 'malmo'),
        'description' => esc_html__('Choose carousel image (min width needs to be 215px)', 'malmo'),
        'parent'      => $carousel_meta_box
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_carousel_hover_image',
        'type'        => 'image',
        'label'       => esc_html__('Carousel Hover Image', 'malmo'),
        'description' => esc_html__('Choose carousel hover image (min width needs to be 215px)', 'malmo'),
        'parent'      => $carousel_meta_box
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_carousel_item_link',
        'type'        => 'text',
        'label'       => esc_html__('Link', 'malmo'),
        'description' => esc_html__('Enter the URL to which you want the image to link to (e.g. http://www.example.com)', 'malmo'),
        'parent'      => $carousel_meta_box
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_carousel_item_target',
        'type'        => 'selectblank',
        'label'       => esc_html__('Target', 'malmo'),
        'description' => esc_html__('Specify where to open the linked document', 'malmo'),
        'parent'      => $carousel_meta_box,
        'options'     => array(
            '_self'  => esc_html__('Self', 'malmo'),
            '_blank' => esc_html__('Blank', 'malmo')
        )
    )
);