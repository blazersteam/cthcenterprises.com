<?php

$title_meta_box = malmo_elated_create_meta_box(
    array(
        'scope' => array('page', 'portfolio-item', 'post'),
        'title' => esc_html__('Title', 'malmo'),
        'name'  => 'title_meta'
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'          => 'eltd_show_title_area_meta',
        'type'          => 'select',
        'default_value' => '',
        'label'         => esc_html__('Show Title Area', 'malmo'),
        'description'   => esc_html__('Disabling this option will turn off page title area', 'malmo'),
        'parent'        => $title_meta_box,
        'options'       => array(
            ''    => '',
            'no'  => esc_html__('No', 'malmo'),
            'yes' => esc_html__('Yes', 'malmo')
        ),
        'args'          => array(
            "dependence" => true,
            "hide"       => array(
                ""    => "",
                "no"  => "#eltd_eltd_show_title_area_meta_container",
                "yes" => ""
            ),
            "show"       => array(
                ""    => "#eltd_eltd_show_title_area_meta_container",
                "no"  => "",
                "yes" => "#eltd_eltd_show_title_area_meta_container"
            )
        )
    )
);

$show_title_area_meta_container = malmo_elated_add_admin_container(
    array(
        'parent'          => $title_meta_box,
        'name'            => 'eltd_show_title_area_meta_container',
        'hidden_property' => 'eltd_show_title_area_meta',
        'hidden_value'    => 'no'
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'          => 'eltd_title_area_type_meta',
        'type'          => 'select',
        'default_value' => '',
        'label'         => esc_html__('Title Area Type', 'malmo'),
        'description'   => esc_html__('Choose title type', 'malmo'),
        'parent'        => $show_title_area_meta_container,
        'options'       => array(
            ''           => '',
            'standard'   => esc_html__('Standard', 'malmo'),
            'breadcrumb' => esc_html__('Breadcrumb', 'malmo')
        ),
        'args'          => array(
            "dependence" => true,
            "hide"       => array(
                "standard"   => "",
                "standard"   => "",
                "breadcrumb" => "#eltd_eltd_title_area_type_meta_container"
            ),
            "show"       => array(
                ""           => "#eltd_eltd_title_area_type_meta_container",
                "standard"   => "#eltd_eltd_title_area_type_meta_container",
                "breadcrumb" => ""
            )
        )
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'          => 'eltd_title_area_enable_icon_meta',
        'type'          => 'select',
        'default_value' => '',
        'label'         => esc_html__('Enable Separator and Icon', 'malmo'),
        'description'   => esc_html__('This option will display Separator and Icon in Title Area on Page', 'malmo'),
        'parent'        => $show_title_area_meta_container,
        'options'       => array(
            ''    => '',
            'no'  => esc_html__('No', 'malmo'),
            'yes' => esc_html__('Yes', 'malmo'),
        ),
        'args'          => array(
            "dependence" => true,
            "hide"       => array(
                ""    => "#eltd_title_area_enable_icon_container",
                "no"  => "#eltd_title_area_enable_icon_container",
                "yes" => ""
            ),
            "show"       => array(
                ""    => "",
                "no"  => "",
                "yes" => "#eltd_title_area_enable_icon_container"
            )
        )
    )
);

//init icon pack hide and show array. It will be populated dinamically from collections array
$title_area_icon_pack_hide_array = array();
$title_area_icon_pack_show_array = array();

//do we have some collection added in collections array?
if(is_array(malmo_elated_icon_collections()->iconCollections) && count(malmo_elated_icon_collections()->iconCollections)) {
    //get collections params array. It will contain values of 'param' property for each collection
    $title_area_icon_collections_params = malmo_elated_icon_collections()->getIconCollectionsParams();

    //foreach collection generate hide and show array
    foreach(malmo_elated_icon_collections()->iconCollections as $dep_collection_key => $dep_collection_object) {
        $title_area_icon_pack_hide_array[$dep_collection_key] = '';


        //we need to include only current collection in show string as it is the only one that needs to show
        $title_area_icon_pack_show_array[$dep_collection_key] = '#eltd_eltd_title_area_icon_'.$dep_collection_object->param.'_meta_container';

        //for all collections param generate hide string
        foreach($title_area_icon_collections_params as $title_area_icon_collections_param) {
            //we don't need to include current one, because it needs to be shown, not hidden
            if($title_area_icon_collections_param !== $dep_collection_object->param) {
                $title_area_icon_pack_hide_array[$dep_collection_key] .= '#eltd_eltd_title_area_icon_'.$title_area_icon_collections_param.'_meta_container,';
            }
        }

        //remove remaining ',' character
        $title_area_icon_pack_hide_array[$dep_collection_key] = rtrim($title_area_icon_pack_hide_array[$dep_collection_key], ',');
    }

}

$title_area_enable_icon_container = malmo_elated_add_admin_container(
    array(
        'parent'          => $show_title_area_meta_container,
        'name'            => 'title_area_enable_icon_container',
        'hidden_property' => 'eltd_title_area_enable_icon_meta',
        'hidden_value'    => 'no',
        'hidden_values'   => array('', 'no')
    )
);

malmo_elated_create_meta_box_field(
    array(
        'parent'        => $title_area_enable_icon_container,
        'type'          => 'select',
        'name'          => 'eltd_title_icon_pack_meta',
        'default_value' => 'font_awesome',
        'label'         => esc_html__('Select Icon Pack', 'malmo'),
        'description'   => esc_html__('Choose icon pack for title area icon', 'malmo'),
        'options'       => malmo_elated_icon_collections()->getIconCollections(),
        'args'          => array(
            'dependence' => true,
            'hide'       => $title_area_icon_pack_hide_array,
            'show'       => $title_area_icon_pack_show_array
        )
    )
);

if(is_array(malmo_elated_icon_collections()->iconCollections) && count(malmo_elated_icon_collections()->iconCollections)) {
    //foreach icon collection we need to generate separate container that will have dependency set
    //it will have one field inside with icons dropdown
    foreach(malmo_elated_icon_collections()->iconCollections as $collection_key => $collection_object) {
        $icons_array = $collection_object->getIconsArray();

        //get icon collection keys (keys from collections array, e.g 'font_awesome', 'font_elegant' etc.)
        $icon_collections_keys = malmo_elated_icon_collections()->getIconCollectionsKeys();

        //unset current one, because it doesn't have to be included in dependency that hides icon container
        unset($icon_collections_keys[array_search($collection_key, $icon_collections_keys)]);

        $title_area_icon_hide_values = $icon_collections_keys;

        $title_area_icon_container = malmo_elated_add_admin_container(
            array(
                'parent'          => $title_area_enable_icon_container,
                'name'            => 'eltd_title_area_icon_'.$collection_object->param.'_meta_container',
                'hidden_property' => 'eltd_title_icon_pack_meta',
                'hidden_value'    => '',
                'hidden_values'   => $title_area_icon_hide_values
            )
        );

        malmo_elated_create_meta_box_field(
            array(
                'parent'        => $title_area_icon_container,
                'type'          => 'select',
                'name'          => 'eltd_title_area_icon_'.$collection_object->param.'_meta',
                'default_value' => '',
                'label'         => esc_html__('Title Area Icon', 'malmo'),
                'description'   => esc_html__('Choose Title Area Icon', 'malmo'),
                'options'       => $icons_array,
            )
        );

    }

}

$title_area_type_meta_container = malmo_elated_add_admin_container(
    array(
        'parent'          => $show_title_area_meta_container,
        'name'            => 'eltd_title_area_type_meta_container',
        'hidden_property' => 'eltd_title_area_type_meta',
        'hidden_value'    => '',
        'hidden_values'   => array('breadcrumb'),
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'          => 'eltd_title_area_enable_breadcrumbs_meta',
        'type'          => 'select',
        'default_value' => '',
        'label'         => esc_html__('Enable Breadcrumbs', 'malmo'),
        'description'   => esc_html__('This option will display Breadcrumbs in Title Area', 'malmo'),
        'parent'        => $title_area_type_meta_container,
        'options'       => array(
            ''    => '',
            'no'  => esc_html__('No', 'malmo'),
            'yes' => esc_html__('Yes', 'malmo')
        ),
    )
);

malmo_elated_create_meta_box_field(array(
    'name'        => 'eltd_title_text_size_meta',
    'type'        => 'select',
    'label'       => esc_html__('Choose Title Text Size', 'malmo'),
    'description' => esc_html__('Choose predefined size for title text', 'malmo'),
    'parent'      => $title_area_type_meta_container,
    'options'     => array(
        ''       => esc_html__('Default', 'malmo'),
        'medium' => esc_html__('Medium', 'malmo'),
        'large'  => esc_html__('Large', 'malmo')
    )
));

malmo_elated_create_meta_box_field(
    array(
        'name'          => 'eltd_title_area_animation_meta',
        'type'          => 'select',
        'default_value' => '',
        'label'         => esc_html__('Animations', 'malmo'),
        'description'   => esc_html__('Choose an animation for Title Area', 'malmo'),
        'parent'        => $show_title_area_meta_container,
        'options'       => array(
            ''           => '',
            'no'         => esc_html__('No Animation', 'malmo'),
            'right-left' => esc_html__('Text right to left', 'malmo'),
            'left-right' => esc_html__('Text left to right', 'malmo')
        )
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'          => 'eltd_title_area_vertial_alignment_meta',
        'type'          => 'select',
        'default_value' => '',
        'label'         => esc_html__('Vertical Alignment', 'malmo'),
        'description'   => esc_html__('Specify title vertical alignment', 'malmo'),
        'parent'        => $show_title_area_meta_container,
        'options'       => array(
            ''              => '',
            'header_bottom' => esc_html__('From Bottom of Header', 'malmo'),
            'window_top'    => esc_html__('From Window Top', 'malmo')
        )
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'          => 'eltd_title_area_content_alignment_meta',
        'type'          => 'select',
        'default_value' => '',
        'label'         => esc_html__('Horizontal Alignment', 'malmo'),
        'description'   => esc_html__('Specify title horizontal alignment', 'malmo'),
        'parent'        => $show_title_area_meta_container,
        'options'       => array(
            ''       => '',
            'left'   => esc_html__('Left', 'malmo'),
            'center' => esc_html__('Center', 'malmo'),
            'right'  => esc_html__('Right', 'malmo')
        )
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_title_text_color_meta',
        'type'        => 'color',
        'label'       => esc_html__('Title Color', 'malmo'),
        'description' => esc_html__('Choose a color for title text', 'malmo'),
        'parent'      => $show_title_area_meta_container
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_title_breadcrumb_color_meta',
        'type'        => 'color',
        'label'       => esc_html__('Breadcrumb Color', 'malmo'),
        'description' => esc_html__('Choose a color for breadcrumb text', 'malmo'),
        'parent'      => $show_title_area_meta_container
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_title_area_background_color_meta',
        'type'        => 'color',
        'label'       => esc_html__('Background Color', 'malmo'),
        'description' => esc_html__('Choose a background color for Title Area', 'malmo'),
        'parent'      => $show_title_area_meta_container
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'          => 'eltd_hide_background_image_meta',
        'type'          => 'yesno',
        'default_value' => 'no',
        'label'         => esc_html__('Hide Background Image', 'malmo'),
        'description'   => esc_html__('Enable this option to hide background image in Title Area', 'malmo'),
        'parent'        => $show_title_area_meta_container,
        'args'          => array(
            "dependence"             => true,
            "dependence_hide_on_yes" => "#eltd_eltd_hide_background_image_meta_container",
            "dependence_show_on_yes" => ""
        )
    )
);

$hide_background_image_meta_container = malmo_elated_add_admin_container(
    array(
        'parent'          => $show_title_area_meta_container,
        'name'            => 'eltd_hide_background_image_meta_container',
        'hidden_property' => 'eltd_hide_background_image_meta',
        'hidden_value'    => 'yes'
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_title_area_background_image_meta',
        'type'        => 'image',
        'label'       => esc_html__('Background Image', 'malmo'),
        'description' => esc_html__('Choose an Image for Title Area', 'malmo'),
        'parent'      => $hide_background_image_meta_container
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'          => 'eltd_title_area_background_image_responsive_meta',
        'type'          => 'select',
        'default_value' => '',
        'label'         => esc_html__('Background Responsive Image', 'malmo'),
        'description'   => esc_html__('Enabling this option will make Title background image responsive', 'malmo'),
        'parent'        => $hide_background_image_meta_container,
        'options'       => array(
            ''    => '',
            'no'  => esc_html__('No', 'malmo'),
            'yes' => esc_html__('Yes', 'malmo')
        ),
        'args'          => array(
            "dependence" => true,
            "hide"       => array(
                ""    => "",
                "no"  => "",
                "yes" => "#eltd_eltd_title_area_background_image_responsive_meta_container, #eltd_eltd_title_area_height_meta"
            ),
            "show"       => array(
                ""    => "#eltd_eltd_title_area_background_image_responsive_meta_container, #eltd_eltd_title_area_height_meta",
                "no"  => "#eltd_eltd_title_area_background_image_responsive_meta_container, #eltd_eltd_title_area_height_meta",
                "yes" => ""
            )
        )
    )
);

$title_area_background_image_responsive_meta_container = malmo_elated_add_admin_container(
    array(
        'parent'          => $hide_background_image_meta_container,
        'name'            => 'eltd_title_area_background_image_responsive_meta_container',
        'hidden_property' => 'eltd_title_area_background_image_responsive_meta',
        'hidden_value'    => 'yes'
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'          => 'eltd_title_area_background_image_parallax_meta',
        'type'          => 'select',
        'default_value' => '',
        'label'         => esc_html__('Background Image in Parallax', 'malmo'),
        'description'   => esc_html__('Enabling this option will make Title background image parallax', 'malmo'),
        'parent'        => $title_area_background_image_responsive_meta_container,
        'options'       => array(
            ''         => '',
            'no'       => esc_html__('No', 'malmo'),
            'yes'      => esc_html__('Yes', 'malmo'),
            'yes_zoom' => esc_html__('Yes, with zoom out', 'malmo')
        )
    )
);

malmo_elated_create_meta_box_field(array(
    'name'        => 'eltd_title_area_height_meta',
    'type'        => 'text',
    'label'       => esc_html__('Height', 'malmo'),
    'description' => esc_html__('Set a height for Title Area', 'malmo'),
    'parent'      => $show_title_area_meta_container,
    'args'        => array(
        'col_width' => 2,
        'suffix'    => 'px'
    )
));

malmo_elated_create_meta_box_field(array(
    'name'          => 'eltd_disable_title_bottom_border_meta',
    'type'          => 'yesno',
    'label'         => esc_html__('Disable Title Bottom Border', 'malmo'),
    'description'   => esc_html__('This option will disable title area bottom border', 'malmo'),
    'parent'        => $show_title_area_meta_container,
    'default_value' => 'no'
));

malmo_elated_create_meta_box_field(array(
    'name'          => 'eltd_title_area_subtitle_meta',
    'type'          => 'text',
    'default_value' => '',
    'label'         => esc_html__('Subtitle Text', 'malmo'),
    'description'   => esc_html__('Enter your subtitle text', 'malmo'),
    'parent'        => $show_title_area_meta_container,
    'args'          => array(
        'col_width' => 6
    )
));

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_subtitle_color_meta',
        'type'        => 'color',
        'label'       => esc_html__('Subtitle Color', 'malmo'),
        'description' => esc_html__('Choose a color for subtitle text', 'malmo'),
        'parent'      => $show_title_area_meta_container
    )
);