<?php

if(!function_exists('malmo_elated_error_404_options_map')) {

    function malmo_elated_error_404_options_map() {

        malmo_elated_add_admin_page(array(
            'slug'  => '__404_error_page',
            'title' => esc_html__('404 Error Page', 'malmo'),
            'icon'  => 'fa fa-exclamation-triangle'
        ));

        $panel_404_options = malmo_elated_add_admin_panel(array(
            'page'  => '__404_error_page',
            'name'  => 'panel_404_options',
            'title' => esc_html__('404 Page Option', 'malmo')
        ));

        malmo_elated_add_admin_field(array(
            'parent'        => $panel_404_options,
            'type'          => 'text',
            'name'          => '404_title',
            'default_value' => '',
            'label'         => esc_html__('Title', 'malmo'),
            'description'   => esc_html__('Enter title for 404 page', 'malmo')
        ));

        malmo_elated_add_admin_field(array(
            'parent'        => $panel_404_options,
            'type'          => 'text',
            'name'          => '404_text',
            'default_value' => '',
            'label'         => esc_html__('Text', 'malmo'),
            'description'   => esc_html__('Enter text for 404 page', 'malmo')
        ));

        malmo_elated_add_admin_field(array(
            'parent'        => $panel_404_options,
            'type'          => 'text',
            'name'          => '404_back_to_home',
            'default_value' => '',
            'label'         => esc_html__('Back to Home Button Label', 'malmo'),
            'description'   => esc_html__('Enter label for "Back to Home" button', 'malmo')
        ));

    }

    add_action('malmo_elated_options_map', 'malmo_elated_error_404_options_map', 15);

}