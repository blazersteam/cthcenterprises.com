<?php

/**
 * Widget that adds search icon that triggers opening of search form
 *
 * Class Elated_Search_Opener
 */
class MalmoElatedSearchOpener extends MalmoElatedWidget {
    /**
     * Set basic widget options and call parent class construct
     */
    public function __construct() {
        parent::__construct(
            'eltd_search_opener', // Base ID
            esc_html__('Elated Search Opener', 'malmo') // Name
        );

        $this->setParams();
    }

    /**
     * Sets widget options
     */
    protected function setParams() {
        $this->params = array(
            array(
                'name'        => 'search_icon_size',
                'type'        => 'textfield',
                'title'       => esc_html__('Search Icon Size (px)', 'malmo'),
                'description' => esc_html__('Define size for Search icon', 'malmo')
            ),
            array(
                'name'        => 'search_icon_color',
                'type'        => 'textfield',
                'title'       => esc_html__('Search Icon Color', 'malmo'),
                'description' => esc_html__('Define color for Search icon', 'malmo')
            ),
            array(
                'name'        => 'search_icon_hover_color',
                'type'        => 'textfield',
                'title'       => esc_html__('Search Icon Hover Color', 'malmo'),
                'description' => esc_html__('Define hover color for Search icon', 'malmo')
            ),
            array(
                'name'        => 'show_label',
                'type'        => 'dropdown',
                'title'       => esc_html__('Enable Search Icon Text', 'malmo'),
                'description' => esc_html__('Enable this option to show \'Search\' text next to search icon in header', 'malmo'),
                'options'     => array(
                    ''    => '',
                    'yes' => esc_html__('Yes', 'malmo'),
                    'no'  => esc_html__('No', 'malmo')
                )
            ),
        );
    }

    /**
     * Generates widget's HTML
     *
     * @param array $args args from widget area
     * @param array $instance widget's options
     */
    public function widget($args, $instance) {
        global $malmo_options, $malmo_IconCollections;

        $search_type_class         = 'eltd-search-opener';
        $fullscreen_search_overlay = false;
        $search_opener_styles      = array();
        $show_search_text          = $instance['show_label'] == 'yes' || $malmo_options['enable_search_icon_text'] == 'yes' ? true : false;

        if(isset($malmo_options['search_type']) && $malmo_options['search_type'] == 'fullscreen-search') {
            if(isset($malmo_options['search_animation']) && $malmo_options['search_animation'] == 'search-from-circle') {
                $fullscreen_search_overlay = true;
            }
        }

        if(isset($malmo_options['search_type']) && $malmo_options['search_type'] == 'search_covers_header') {
            if(isset($malmo_options['search_cover_only_bottom_yesno']) && $malmo_options['search_cover_only_bottom_yesno'] == 'yes') {
                $search_type_class .= ' search_covers_only_bottom';
            }
        }

        if(!empty($instance['search_icon_size'])) {
            $search_opener_styles[] = 'font-size: '.$instance['search_icon_size'].'px';
        }

        if(!empty($instance['search_icon_color'])) {
            $search_opener_styles[] = 'color: '.$instance['search_icon_color'];
        }

		echo malmo_elated_get_module_part($args['before_widget']);

        ?>

        <a <?php echo malmo_elated_get_inline_attr($instance['search_icon_hover_color'], 'data-hover-color'); ?>
            <?php malmo_elated_inline_style($search_opener_styles); ?>
            <?php malmo_elated_class_attribute($search_type_class); ?> href="javascript:void(0)">
            <?php if(isset($malmo_options['search_icon_pack'])) {
                $malmo_IconCollections->getSearchIcon($malmo_options['search_icon_pack'], false);
            } ?>
            <?php if($show_search_text) { ?>
                <span class="eltd-search-icon-text"><?php esc_html_e('Search', 'malmo'); ?></span>
            <?php } ?>
        </a>

        <?php if($fullscreen_search_overlay) { ?>
            <div class="eltd-fullscreen-search-overlay"></div>
        <?php } ?>

        <?php do_action('malmo_elated_after_search_opener'); ?>

        <?php echo malmo_elated_get_module_part($args['after_widget']); ?>
    <?php }
}