<?php

class MalmoElatedLatestPosts extends MalmoElatedWidget {
    protected $params;

    public function __construct() {
        parent::__construct(
            'eltd_latest_posts_widget', // Base ID
            esc_html__('Elated Latest Post', 'malmo'), // Name
            array('description' => esc_html__('Display posts from your blog', 'malmo'),) // Args
        );

        $this->setParams();
    }

    protected function setParams() {
        $this->params = array(
            array(
                'name'  => 'title',
                'type'  => 'textfield',
                'title' => esc_html__('Title', 'malmo')
            ),
            array(
                'name'    => 'type',
                'type'    => 'dropdown',
                'title'   => esc_html__('Type', 'malmo'),
                'options' => array(
                    'minimal'      => esc_html__('Minimal', 'malmo'),
                    'image-in-box' => esc_html__('Image in box', 'malmo')
                )
            ),
            array(
                'name'  => 'number_of_posts',
                'type'  => 'textfield',
                'title' => esc_html__('Number of posts', 'malmo')
            ),
            array(
                'name'    => 'order_by',
                'type'    => 'dropdown',
                'title'   => esc_html__('Order By', 'malmo'),
                'options' => array(
                    'title' => esc_html__('Title', 'malmo'),
                    'date'  => esc_html__('Date', 'malmo')
                )
            ),
            array(
                'name'    => 'order',
                'type'    => 'dropdown',
                'title'   => esc_html__('Order', 'malmo'),
                'options' => array(
                    'ASC'  => esc_html__('ASC', 'malmo'),
                    'DESC' => esc_html__('DESC', 'malmo')
                )
            ),
            array(
                'name'    => 'image_size',
                'type'    => 'dropdown',
                'title'   => esc_html__('Image Size', 'malmo'),
                'options' => array(
                    'original'  => esc_html__('Original', 'malmo'),
                    'landscape' => esc_html__('Landscape', 'malmo'),
                    'square'    => esc_html__('Square', 'malmo'),
                    'custom'    => esc_html__('Custom', 'malmo')
                )
            ),
            array(
                'name'  => 'custom_image_size',
                'type'  => 'textfield',
                'title' => esc_html__('Custom Image Size', 'malmo')
            ),
            array(
                'name'  => 'category',
                'type'  => 'textfield',
                'title' => esc_html__('Category Slug', 'malmo')
            ),
            array(
                'name'  => 'text_length',
                'type'  => 'textfield',
                'title' => esc_html__('Number of characters', 'malmo')
            ),
            array(
                'name'    => 'title_tag',
                'type'    => 'dropdown',
                'title'   => esc_html__('Title Tag', 'malmo'),
                'options' => array(
                    ""   => "",
                    "h2" => "h2",
                    "h3" => "h3",
                    "h4" => "h4",
                    "h5" => "h5",
                    "h6" => "h6"
                )
            )
        );
    }

    public function widget($args, $instance) {
        extract($args);

        //prepare variables
        $content        = '';
        $params         = array();
        $params['type'] = 'image_in_box';

        //is instance empty?
        if(is_array($instance) && count($instance)) {
            //generate shortcode params
            foreach($instance as $key => $value) {
                $params[$key] = $value;
            }
        }
        if(empty($params['title_tag'])) {
            $params['title_tag'] = 'h6';
        }
        echo '<div class="widget eltd-latest-posts-widget">';

        if(!empty($instance['title'])) {
			echo malmo_elated_get_module_part($args['before_title'] . $instance['title'] . $args['after_title']);
        }

        echo malmo_elated_execute_shortcode('eltd_blog_list', $params);

        echo '</div>'; //close eltd-latest-posts-widget
    }
}
