<?php

class MalmoElatedFullScreenMenuOpener extends MalmoElatedWidget {
    public function __construct() {
        parent::__construct(
            'eltd_full_screen_menu_opener', // Base ID
            esc_html__('Elated Full Screen Menu Opener', 'malmo') // Name
        );

        $this->setParams();
    }

    protected function setParams() {

        $this->params = array(
            array(
                'name'        => 'fullscreen_menu_opener_icon_color',
                'type'        => 'textfield',
                'title'       => esc_html__('Icon Color', 'malmo'),
                'description' => esc_html__('Define color for Side Area opener icon', 'malmo')
            )
        );

    }

    public function widget($args, $instance) {
        global $malmo_options;

        $fullscreen_icon_styles = array();

        if(!empty($instance['fullscreen_menu_opener_icon_color'])) {
            $fullscreen_icon_styles[] = 'background-color: '.$instance['fullscreen_menu_opener_icon_color'];
        }


        $icon_size = '';
        if(isset($malmo_options['eltd_fullscreen_menu_icon_size']) && $malmo_options['eltd_fullscreen_menu_icon_size'] !== '') {
            $icon_size = $malmo_options['eltd_fullscreen_menu_icon_size'];
        }
        ?>
        <!--        <a href="javascript:void(0)" class="popup_menu">-->
        <a href="javascript:void(0)" class="eltd-fullscreen-menu-opener <?php echo esc_attr($icon_size) ?>">
            <!--            <span class="popup_menu_inner">-->
            <span class="eltd-fullscreen-menu-opener-inner">
                <i class="eltd-line" <?php malmo_elated_inline_style($fullscreen_icon_styles); ?>>&nbsp;</i>
            </span>
        </a>
    <?php }

}