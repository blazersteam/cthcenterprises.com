<?php

if(!function_exists('malmo_elated_search_body_class')) {
	/**
	 * Function that adds body classes for different search types
	 *
	 * @param $classes array original array of body classes
	 *
	 * @return array modified array of classes
	 */
	function malmo_elated_search_body_class($classes) {

		if(is_active_widget(false, false, 'eltd_search_opener')) {

			$classes[] = 'eltd-'.malmo_elated_options()->getOptionValue('search_type');

			if(malmo_elated_options()->getOptionValue('search_type') == 'fullscreen-search') {

				$classes[] = 'eltd-search-fade';

			}

		}

		return $classes;

	}

	add_filter('body_class', 'malmo_elated_search_body_class');
}

if(!function_exists('malmo_elated_get_search')) {
	/**
	 * Loads search HTML based on search type option.
	 */
	function malmo_elated_get_search() {

		if(malmo_elated_active_widget(false, false, 'eltd_search_opener')) {

			$search_type = malmo_elated_options()->getOptionValue('search_type');

			if($search_type == 'search-covers-header') {
				malmo_elated_set_position_for_covering_search();

				return;
			} else if($search_type == 'search-slides-from-window-top' || $search_type == 'search-slides-from-header-bottom') {
				malmo_elated_set_search_position_in_menu($search_type);
				if(malmo_elated_is_responsive_on()) {
					malmo_elated_set_search_position_mobile();
				}

				return;
			} elseif($search_type === 'search-dropdown') {
				malmo_elated_set_dropdown_search_position();

				return;
			}

			malmo_elated_load_search_template();

		}
	}

}

if(!function_exists('malmo_elated_set_position_for_covering_search')) {
	/**
	 * Finds part of header where search template will be loaded
	 */
	function malmo_elated_set_position_for_covering_search() {

		$containing_sidebar = malmo_elated_active_widget(false, false, 'eltd_search_opener');

		foreach($containing_sidebar as $sidebar) {

			if(strpos($sidebar, 'top-bar') !== false) {
				add_action('malmo_elated_after_header_top_html_open', 'malmo_elated_load_search_template');
			} else if(strpos($sidebar, 'main-menu') !== false) {
				add_action('malmo_elated_after_header_menu_area_html_open', 'malmo_elated_load_search_template');
			} else if(strpos($sidebar, 'mobile-logo') !== false) {
				add_action('malmo_elated_after_mobile_header_html_open', 'malmo_elated_load_search_template');
			} else if(strpos($sidebar, 'logo') !== false) {
				add_action('malmo_elated_after_header_logo_area_html_open', 'malmo_elated_load_search_template');
			} else if(strpos($sidebar, 'sticky') !== false) {
				add_action('malmo_elated_after_sticky_menu_html_open', 'malmo_elated_load_search_template');
			}

		}

	}

}

if(!function_exists('malmo_elated_set_search_position_in_menu')) {
	/**
	 * Finds part of header where search template will be loaded
	 */
	function malmo_elated_set_search_position_in_menu($type) {

		add_action('malmo_elated_after_header_menu_area_html_open', 'malmo_elated_load_search_template');
		if($type == 'search-slides-from-header-bottom') {
			add_action('malmo_elated_after_sticky_menu_html_open', 'malmo_elated_load_search_template');
		}

	}
}

if(!function_exists('malmo_elated_set_search_position_mobile')) {
	/**
	 * Hooks search template to mobile header
	 */
	function malmo_elated_set_search_position_mobile() {

		add_action('malmo_elated_after_mobile_header_html_open', 'malmo_elated_load_search_template');

	}

}

if(!function_exists('malmo_elated_load_search_template')) {
	/**
	 * Loads HTML template with parameters
	 */
	function malmo_elated_load_search_template() {
		global $malmo_IconCollections;

		$search_type = malmo_elated_options()->getOptionValue('search_type');

		$search_icon       = '';
		$search_icon_close = '';
		if(malmo_elated_options()->getOptionValue('search_icon_pack') !== '') {
			$search_icon       = $malmo_IconCollections->getSearchIcon(malmo_elated_options()->getOptionValue('search_icon_pack'), true);
			$search_icon_close = $malmo_IconCollections->getSearchClose(malmo_elated_options()->getOptionValue('search_icon_pack'), true);
		}

		$parameters = array(
			'search_in_grid'    => malmo_elated_options()->getOptionValue('search_in_grid') == 'yes' ? true : false,
			'search_icon'       => $search_icon,
			'search_icon_close' => $search_icon_close
		);

		malmo_elated_get_module_template_part('templates/types/'.$search_type, 'search', '', $parameters);

	}

}

if(!function_exists('malmo_elated_set_dropdown_search_position')) {
	function malmo_elated_set_dropdown_search_position() {
		add_action('malmo_elated_after_search_opener', 'malmo_elated_load_search_template');
	}
}