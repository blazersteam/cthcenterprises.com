<?php
/**
 * Woocommerce helper functions
 */

if(!function_exists('malmo_elated_disable_woocommerce_pretty_photo')) {
    /**
     * Function that disable WooCommerce pretty photo script and style
     */
    function malmo_elated_disable_woocommerce_pretty_photo() {
        //is woocommerce installed?
        if(malmo_elated_is_woocommerce_installed()) {
            if(malmo_elated_load_woo_assets()) {

                wp_deregister_style('woocommerce_prettyPhoto_css');
            }
        }
    }

    add_action('wp_enqueue_scripts', 'malmo_elated_disable_woocommerce_pretty_photo');
}

if(!function_exists('malmo_elated_woocommerce_body_class')) {
    /**
     * Function that adds class on body for Woocommerce
     *
     * @param $classes
     *
     * @return array
     */
    function malmo_elated_woocommerce_body_class($classes) {
        if(malmo_elated_is_woocommerce_page()) {
            $classes[] = 'eltd-woocommerce-page';

            if(function_exists('is_shop') && is_shop()) {
                $classes[] = 'eltd-woo-main-page';
            }

            if(is_singular('product')) {
                $classes[] = 'eltd-woo-single-page';
            }
        }

        return $classes;
    }

    add_filter('body_class', 'malmo_elated_woocommerce_body_class');
}

if(!function_exists('malmo_elated_woocommerce_columns_class')) {
    /**
     * Function that adds number of columns class to header tag
     *
     * @param array array of classes from main filter
     *
     * @return array array of classes with added woocommerce class
     */
    function malmo_elated_woocommerce_columns_class($classes) {

        if(malmo_elated_is_woocommerce_installed()) {

            $products_list_number = malmo_elated_options()->getOptionValue('eltd_woo_product_list_columns');
            $classes[]            = $products_list_number;

        }

        return $classes;
    }

    add_filter('body_class', 'malmo_elated_woocommerce_columns_class');
}

if(!function_exists('malmo_elated_is_woocommerce_page')) {
    /**
     * Function that checks if current page is woocommerce shop, product or product taxonomy
     * @return bool
     *
     * @see is_woocommerce()
     */
    function malmo_elated_is_woocommerce_page() {
        if(function_exists('is_woocommerce') && is_woocommerce()) {
            return is_woocommerce();
        } elseif(function_exists('is_cart') && is_cart()) {
            return is_cart();
        } elseif(function_exists('is_checkout') && is_checkout()) {
            return is_checkout();
        } elseif(function_exists('is_account_page') && is_account_page()) {
            return is_account_page();
        }
    }
}

if(!function_exists('malmo_elated_is_woocommerce_shop')) {
    /**
     * Function that checks if current page is shop or product page
     * @return bool
     *
     * @see is_shop()
     */
    function malmo_elated_is_woocommerce_shop() {
        return function_exists('is_shop') && (is_shop() || is_product());
    }
}

if(!function_exists('malmo_elated_get_woo_shop_page_id')) {
    /**
     * Function that returns shop page id that is set in WooCommerce settings page
     * @return int id of shop page
     */
    function malmo_elated_get_woo_shop_page_id() {
        if(malmo_elated_is_woocommerce_installed()) {
            return get_option('woocommerce_shop_page_id');
        }
    }
}

if(!function_exists('malmo_elated_is_product_category')) {
    function malmo_elated_is_product_category() {
        return function_exists('is_product_category') && is_product_category();
    }
}

if(!function_exists('malmo_elated_is_product_tag')) {
    function malmo_elated_is_product_tag() {
        return function_exists('is_product_tag') && is_product_tag();
    }
}

if(!function_exists('malmo_elated_is_yith_wishlist_install')) {
    function malmo_elated_is_yith_wishlist_install() {
        return defined('YITH_WCWL');
    }
}

if(!function_exists('malmo_elated_is_yith_wcqv_install')) {
    function malmo_elated_is_yith_wcqv_install() {
        return defined('YITH_WCQV');
    }
}

if(!function_exists('malmo_elated_load_woo_assets')) {
    /**
     * Function that checks whether WooCommerce assets needs to be loaded.
     *
     * @see malmo_elated_is_woocommerce_page()
     * @see malmo_elated_has_woocommerce_shortcode()
     * @see malmo_elated_has_woocommerce_widgets()
     * @return bool
     */

    function malmo_elated_load_woo_assets() {
        return malmo_elated_is_woocommerce_installed() && (malmo_elated_is_woocommerce_page() || malmo_elated_has_woocommerce_shortcode() || malmo_elated_has_woocommerce_widgets());
    }
}

if(!function_exists('malmo_elated_return_woocommerce_global_variable')) {
    function malmo_elated_return_woocommerce_global_variable() {
        if(malmo_elated_is_woocommerce_installed()) {
            global $product;

            return $product;
        }
    }
}

if(!function_exists('malmo_elated_has_woocommerce_shortcode')) {
    /**
     * Function that checks if current page has at least one of WooCommerce shortcodes added
     * @return bool
     */
    function malmo_elated_has_woocommerce_shortcode() {
        $woocommerce_shortcodes = array(
            'add_to_cart',
            'add_to_cart_url',
            'product_page',
            'product',
            'products',
            'product_categories',
            'product_category',
            'recent_products',
            'featured_products',
            'sale_products',
            'best_selling_products',
            'top_rated_products',
            'product_attribute',
            'related_products',
            'woocommerce_messages',
            'woocommerce_cart',
            'woocommerce_checkout',
            'woocommerce_order_tracking',
            'woocommerce_my_account',
            'woocommerce_edit_address',
            'woocommerce_change_password',
            'woocommerce_view_order',
            'woocommerce_pay',
            'woocommerce_thankyou',
            'eltd_product_list '
        );

        foreach($woocommerce_shortcodes as $woocommerce_shortcode) {
            $has_shortcode = malmo_elated_has_shortcode($woocommerce_shortcode);

            if($has_shortcode) {
                return true;
            }
        }

        return false;
    }
}

if(!function_exists('malmo_elated_has_woocommerce_widgets')) {
    /**
     * Function that checks if current page has at least one of WooCommerce shortcodes added
     * @return bool
     */
    function malmo_elated_has_woocommerce_widgets() {
        $widgets_array = array(
            'eltd_woocommerce_dropdown_cart',
            'woocommerce_widget_cart',
            'woocommerce_layered_nav',
            'woocommerce_layered_nav_filters',
            'woocommerce_price_filter',
            'woocommerce_product_categories',
            'woocommerce_product_search',
            'woocommerce_product_tag_cloud',
            'woocommerce_products',
            'woocommerce_recent_reviews',
            'woocommerce_recently_viewed_products',
            'woocommerce_top_rated_products'
        );

        foreach($widgets_array as $widget) {
            $active_widget = is_active_widget(false, false, $widget);

            if($active_widget) {
                return true;
            }
        }

        return false;
    }
}

if(!function_exists('malmo_elated_add_woocommerce_shortcode_class')) {
    /**
     * Function that checks if current page has at least one of WooCommerce shortcodes added
     * @return string
     */
    function malmo_elated_add_woocommerce_shortcode_class($classes) {
        $woocommerce_shortcodes = array(
            'woocommerce_order_tracking'
        );

        foreach($woocommerce_shortcodes as $woocommerce_shortcode) {
            $has_shortcode = malmo_elated_has_shortcode($woocommerce_shortcode);

            if($has_shortcode) {
                $classes[] = 'eltd-woocommerce-page woocommerce-account eltd-'.str_replace('_', '-', $woocommerce_shortcode);
            }
        }

        return $classes;
    }

    add_filter('body_class', 'malmo_elated_add_woocommerce_shortcode_class');
}

if(!function_exists('malmo_elated_get_woocommerce_pages')) {
    /**
     * Function that returns all url woocommerce pages
     * @return array array of WooCommerce pages
     *
     * @version 0.1
     */
    function malmo_elated_get_woocommerce_pages() {
        $woo_pages_array = array();

        if(malmo_elated_is_woocommerce_installed()) {
            if(get_option('woocommerce_shop_page_id') != '') {
                $woo_pages_array[] = get_permalink(get_option('woocommerce_shop_page_id'));
            }
            if(get_option('woocommerce_cart_page_id') != '') {
                $woo_pages_array[] = get_permalink(get_option('woocommerce_cart_page_id'));
            }
            if(get_option('woocommerce_checkout_page_id') != '') {
                $woo_pages_array[] = get_permalink(get_option('woocommerce_checkout_page_id'));
            }
            if(get_option('woocommerce_pay_page_id') != '') {
                $woo_pages_array[] = get_permalink(get_option(' woocommerce_pay_page_id '));
            }
            if(get_option('woocommerce_thanks_page_id') != '') {
                $woo_pages_array[] = get_permalink(get_option(' woocommerce_thanks_page_id '));
            }
            if(get_option('woocommerce_myaccount_page_id') != '') {
                $woo_pages_array[] = get_permalink(get_option(' woocommerce_myaccount_page_id '));
            }
            if(get_option('woocommerce_edit_address_page_id') != '') {
                $woo_pages_array[] = get_permalink(get_option(' woocommerce_edit_address_page_id '));
            }
            if(get_option('woocommerce_view_order_page_id') != '') {
                $woo_pages_array[] = get_permalink(get_option(' woocommerce_view_order_page_id '));
            }
            if(get_option('woocommerce_terms_page_id') != '') {
                $woo_pages_array[] = get_permalink(get_option(' woocommerce_terms_page_id '));
            }

            $woo_products = get_posts(array(
                'post_type'      => 'product',
                'post_status'    => 'publish',
                'posts_per_page' => '-1'
            ));

            foreach($woo_products as $product) {
                $woo_pages_array[] = get_permalink($product->ID);
            }
        }

        return $woo_pages_array;
    }
}

if(!function_exists('malmo_elated_woocommerce_share')) {
    /**
     * Function that social share for product page
     * Return array array of WooCommerce pages
     */
    function malmo_elated_woocommerce_share() {
        if(malmo_elated_is_woocommerce_installed()) {

            if( malmo_elated_core_plugin_installed() && malmo_elated_options()->getOptionValue('enable_social_share') == 'yes' && malmo_elated_options()->getOptionValue('enable_social_share_on_product') == 'yes') :
                print '<div class="eltd-woo-social-share-holder">';
                print '<span>'.esc_html__('Share:', 'malmo').'</span>';
                echo malmo_elated_get_social_share_html();
                print '</div>';
            endif;
        }
    }
}

if(!function_exists('malmo_elated_woocommerce_product_accordions')) {
    /**
     * Function that social share for product page
     * Return array array of WooCommerce pages
     */
    function malmo_elated_woocommerce_product_accordions($tabs) {
        if(malmo_elated_is_woocommerce_installed() && !empty($tabs)) { ?>
            <div class="eltd-accordion-holder eltd-accordion clearfix eltd-woocommerce-tabs wc-tabs-wrapper">
                <?php foreach($tabs as $key => $tab) : ?>

                    <h4 class="eltd-title-holder tab-<?php echo esc_attr($key); ?>">
                        <span class="eltd-accordion-mark eltd-left-mark">
                            <span class="eltd-accordion-mark-icon">
                                <span class="eltd-acc-plus">+</span>
                                <span class="eltd-acc-minus">-</span>
                            </span>
                        </span>
                        <span class="eltd-tab-title"><?php echo apply_filters('woocommerce_product_'.$key.'_tab_title', esc_html($tab['title']), $key); ?></span>
                    </h4>

                    <div class="eltd-accordion-content">
                        <div class="eltd-accordion-content-inner">
                            <?php call_user_func($tab['callback'], $key, $tab) ?>
                        </div>
                    </div>

                <?php endforeach; ?>
            </div>
            <?php
        }
    }
}

/**
 * Function that set number of columns for thumbnail on shop single page
 * Return array array of WooCommerce pages
 */
if(!function_exists('malmo_elated_woocommerce_product_thumbnails_columns')) {
    function malmo_elated_woocommerce_product_thumbnails_columns() {
        return '6';
    }
}

if(!function_exists('malmo_elated_woocommerce_image_html_part')) {
    /**
     * Function for adding Image html part for shortcodes
     *
     * @return string
     */
    function malmo_elated_woocommerce_image_html_part($class_name = '', $image_size = '', $new_layout = '') {
        global $product;

        $html = '';

        if($product->is_on_sale()) {
            $html .= '<span class="eltd-'.esc_attr($class_name).'-onsale">'.esc_html__("SALE", "malmo").'</span>';
        }
        if(!$product->is_in_stock()) {
            $html .= '<span class="eltd-'.esc_attr($class_name).'-out-of-stock">'.esc_html__("OUT OF STOCK", "malmo").'</span>';
        }
        if($new_layout === 'yes') {
            $html .= '<span class="eltd-'.esc_attr($class_name).'-new-product">'.esc_html__("NEW", "malmo").'</span>';
        }

        $product_image_size = 'shop_single';
        if($image_size === 'full') {
            $product_image_size = 'full';
        } else if($image_size === 'square') {
            $product_image_size = 'malmo_elated_image_square';
        } else if($image_size === 'landscape') {
            $product_image_size = 'malmo_elated_image_landscape';
        } else if($image_size === 'portrait') {
            $product_image_size = 'malmo_elated_image_portrait';
        } else if($image_size === 'medium') {
            $product_image_size = 'medium';
        } else if($image_size === 'large') {
            $product_image_size = 'large';
        }

        $html .= get_the_post_thumbnail(get_the_ID(), apply_filters('malmo_elated_product_list_image_size', $product_image_size));

        return $html;
    }
}

if(!function_exists('malmo_elated_woocommerce_title_html_part')) {
    /**
     * Function for adding Title html part for shortcodes
     *
     * @return string
     */
    function malmo_elated_woocommerce_title_html_part($class_name = '', $title_tag = '', $has_link = '', $title_styles = '') {

        if($has_link === 'yes') {
            $html = '<'.esc_attr($title_tag).' itemprop="name" class="entry-title eltd-'.esc_attr($class_name).'-title" '.malmo_elated_get_inline_style($title_styles).'><a itemprop="url" href="'.get_the_permalink().'">'.get_the_title().'</a></'.esc_attr($title_tag).'>';
        } else {
            $html = '<'.esc_attr($title_tag).' itemprop="name" class="entry-title eltd-'.esc_attr($class_name).'-title" '.malmo_elated_get_inline_style($title_styles).'>'.get_the_title().'</'.esc_attr($title_tag).'>';
        }

        return $html;
    }
}

//if(!function_exists('malmo_elated_woocommerce_excerpt_html_part')) {
//    /**
//     * Function for adding Excerpt html part for shortcodes
//     *
//     * @return string
//     */
//    function malmo_elated_woocommerce_excerpt_html_part($class_name = '', $excerpt_length = 0) {
//
//        $excerpt = ($excerpt_length > 0) ? substr(get_the_excerpt(), 0, intval($excerpt_length)) : get_the_excerpt();
//
//        $html = '<p class="eltd-'.esc_attr($class_name).'-excerpt" itemprop="description">'.esc_html($excerpt).'</p>';
//
//        return $html;
//    }
//}

if(!function_exists('malmo_elated_woocommerce_no_products_found_html_part')) {
    /**
     * Function for adding No products were found html part for shortcodes
     *
     * @return string
     */
    function malmo_elated_woocommerce_no_products_found_html_part($class_name = '') {

        $html = '<div class="eltd-'.esc_attr($class_name).'-messsage"><p>'.esc_html__("No products were found!", "malmo").'</p></div>';

        return $html;
    }
}

if(!function_exists('malmo_elated_woocommerce_rating_html_part')) {
    /**
     * Function for adding Rating html part for shortcodes
     *
     * @return string
     */
    function malmo_elated_woocommerce_rating_html_part($class_name = '') {
        global $product;

        $html = '';

        if(get_option('woocommerce_enable_review_rating') !== 'no') {
            $average = $product->get_average_rating();

            $html = '<div class="eltd-'.esc_attr($class_name).'-rating-holder"><div class="eltd-'.esc_attr($class_name).'-rating" title="'.sprintf(esc_attr__("Rated %s out of 5", "malmo"), $average).'"><span style="width:'.(($average / 5) * 100).'%"></span></div></div>';
        }

        return $html;
    }
}

if(!function_exists('malmo_elated_woocommerce_wishlist_shortcode')) {
    function malmo_elated_woocommerce_wishlist_shortcode() {

        if(malmo_elated_is_yith_wishlist_install()) {
            echo do_shortcode('[yith_wcwl_add_to_wishlist]');
        }
    }
}

if(!function_exists('malmo_elated_woocommerce_quick_view_button')) {
    function malmo_elated_woocommerce_quick_view_button() {

        if(malmo_elated_is_yith_wcqv_install()) {
            global $product;

            print '<div class="eltd-yith-wcqv-holder"><a href="#" class="yith-wcqv-button" data-product_id="'.$product->id.'"><span class="eltd-yith-wcqv-icon ion-eye"></span></a></div>';
        }
    }
}


if(!function_exists('malmo_elated_woocommerce_price_html_part')) {
    /**
     * Function for adding Price html part for shortcodes
     *
     * @return string
     */
    function malmo_elated_woocommerce_price_html_part($class_name = '') {
        global $product;

        $html = '';

        if($price_html = $product->get_price_html()) {
            $html = '<div class="eltd-'.esc_attr($class_name).'-price">'.$price_html.'</div>';
        }

        return $html;
    }
}