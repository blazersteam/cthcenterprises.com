<?php

if(!function_exists('malmo_elated_blog_options_map')) {

    function malmo_elated_blog_options_map() {

        malmo_elated_add_admin_page(
            array(
                'slug'  => '_blog_page',
                'title' => esc_html__('Blog', 'malmo'),
                'icon'  => 'fa fa-files-o'
            )
        );

        /**
         * Blog Lists
         */

        $custom_sidebars = malmo_elated_get_custom_sidebars();

        $panel_blog_lists = malmo_elated_add_admin_panel(
            array(
                'page'  => '_blog_page',
                'name'  => 'panel_blog_lists',
                'title' => esc_html__('Blog Lists', 'malmo')
            )
        );

        malmo_elated_add_admin_field(array(
            'name'          => 'blog_list_type',
            'type'          => 'select',
            'label'         => esc_html__('Blog Layout for Archive Pages', 'malmo'),
            'description'   => esc_html__('Choose a default blog layout', 'malmo'),
            'default_value' => 'standard',
            'parent'        => $panel_blog_lists,
            'options'       => array(
                'standard'              => esc_html__('Blog: Standard', 'malmo'),
                'split-column'          => esc_html__('Blog: Split Column', 'malmo'),
                'masonry'               => esc_html__('Blog: Masonry', 'malmo'),
                'masonry-full-width'    => esc_html__('Blog: Masonry Full Width', 'malmo'),
                'standard-whole-post'   => esc_html__('Blog: Standard Whole Post', 'malmo'),
                'standard-date-on-side' => esc_html__('Blog: Standard Date On Side', 'malmo')
            )
        ));

        malmo_elated_add_admin_field(array(
            'name'        => 'archive_sidebar_layout',
            'type'        => 'select',
            'label'       => esc_html__('Archive and Category Sidebar', 'malmo'),
            'description' => esc_html__('Choose a sidebar layout for archived Blog Post Lists and Category Blog Lists', 'malmo'),
            'parent'      => $panel_blog_lists,
            'options'     => array(
                'default'          => esc_html__('No Sidebar', 'malmo'),
                'sidebar-33-right' => esc_html__('Sidebar 1/3 Right', 'malmo'),
                'sidebar-25-right' => esc_html__('Sidebar 1/4 Right', 'malmo'),
                'sidebar-33-left'  => esc_html__('Sidebar 1/3 Left', 'malmo'),
                'sidebar-25-left'  => esc_html__('Sidebar 1/4 Left', 'malmo'),
            )
        ));


        if(count($custom_sidebars) > 0) {
            malmo_elated_add_admin_field(array(
                'name'        => 'blog_custom_sidebar',
                'type'        => 'selectblank',
                'label'       => esc_html__('Sidebar to Display', 'malmo'),
                'description' => esc_html__('Choose a sidebar to display on Blog Post Lists and Category Blog Lists. Default sidebar is "Sidebar Page"', 'malmo'),
                'parent'      => $panel_blog_lists,
                'options'     => malmo_elated_get_custom_sidebars()
            ));
        }

        malmo_elated_add_admin_field(
            array(
                'type'          => 'yesno',
                'name'          => 'pagination',
                'default_value' => 'yes',
                'label'         => esc_html__('Pagination', 'malmo'),
                'parent'        => $panel_blog_lists,
                'description'   => esc_html__('Enabling this option will display pagination links on bottom of Blog Post List', 'malmo'),
                'args'          => array(
                    'dependence'             => true,
                    'dependence_hide_on_yes' => '',
                    'dependence_show_on_yes' => '#eltd_eltd_pagination_container'
                )
            )
        );

        $pagination_container = malmo_elated_add_admin_container(
            array(
                'name'            => 'eltd_pagination_container',
                'hidden_property' => 'pagination',
                'hidden_value'    => 'no',
                'parent'          => $panel_blog_lists,
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $pagination_container,
                'type'          => 'text',
                'name'          => 'blog_page_range',
                'default_value' => '',
                'label'         => esc_html__('Pagination Range limit', 'malmo'),
                'description'   => esc_html__('Enter a number that will limit pagination to a certain range of links', 'malmo'),
                'args'          => array(
                    'col_width' => 3
                )
            )
        );

        malmo_elated_add_admin_field(array(
            'name'        => 'masonry_pagination',
            'type'        => 'select',
            'label'       => esc_html__('Pagination on Masonry', 'malmo'),
            'description' => esc_html__('Choose a pagination style for Masonry Blog List', 'malmo'),
            'parent'      => $pagination_container,
            'options'     => array(
                'standard'        => esc_html__('Standard', 'malmo'),
                'load-more'       => esc_html__('Load More', 'malmo'),
                'infinite-scroll' => esc_html__('Infinite Scroll', 'malmo')
            ),

        ));
        malmo_elated_add_admin_field(
            array(
                'type'          => 'yesno',
                'name'          => 'enable_load_more_pag',
                'default_value' => 'no',
                'label'         => esc_html__('Load More Pagination on Other Lists', 'malmo'),
                'parent'        => $pagination_container,
                'description'   => esc_html__('Enable Load More Pagination on other lists', 'malmo'),
                'args'          => array(
                    'col_width' => 3
                )
            )
        );

        malmo_elated_add_admin_field(
            array(
                'type'          => 'yesno',
                'name'          => 'masonry_filter',
                'default_value' => 'no',
                'label'         => esc_html__('Masonry Filter', 'malmo'),
                'parent'        => $panel_blog_lists,
                'description'   => esc_html__('Enabling this option will display category filter on Masonry and Masonry Full Width Templates', 'malmo'),
                'args'          => array(
                    'col_width' => 3
                )
            )
        );
        malmo_elated_add_admin_field(
            array(
                'type'          => 'text',
                'name'          => 'number_of_chars',
                'default_value' => '',
                'label'         => esc_html__('Number of Words in Excerpt', 'malmo'),
                'parent'        => $panel_blog_lists,
                'description'   => esc_html__('Enter a number of words in excerpt (article summary)', 'malmo'),
                'args'          => array(
                    'col_width' => 3
                )
            )
        );
        malmo_elated_add_admin_field(
            array(
                'type'          => 'text',
                'name'          => 'standard_number_of_chars',
                'default_value' => '45',
                'label'         => esc_html__('Standard Type Number of Words in Excerpt', 'malmo'),
                'parent'        => $panel_blog_lists,
                'description'   => esc_html__('Enter a number of words in excerpt (article summary)', 'malmo'),
                'args'          => array(
                    'col_width' => 3
                )
            )
        );
        malmo_elated_add_admin_field(
            array(
                'type'          => 'text',
                'name'          => 'masonry_number_of_chars',
                'default_value' => '45',
                'label'         => esc_html__('Masonry Type Number of Words in Excerpt', 'malmo'),
                'parent'        => $panel_blog_lists,
                'description'   => esc_html__('Enter a number of words in excerpt (article summary)', 'malmo'),
                'args'          => array(
                    'col_width' => 3
                )
            )
        );
        malmo_elated_add_admin_field(
            array(
                'type'          => 'text',
                'name'          => 'split_column_number_of_chars',
                'default_value' => '45',
                'label'         => esc_html__('Split Column Type Number of Words in Excerpt', 'malmo'),
                'parent'        => $panel_blog_lists,
                'description'   => esc_html__('Enter a number of words in excerpt (article summary)', 'malmo'),
                'args'          => array(
                    'col_width' => 3
                )
            )
        );

        /**
         * Blog Single
         */
        $panel_blog_single = malmo_elated_add_admin_panel(
            array(
                'page'  => '_blog_page',
                'name'  => 'panel_blog_single',
                'title' => esc_html__('Blog Single', 'malmo')
            )
        );

        malmo_elated_add_admin_field(array(
            'name'        => 'blog_single_type',
            'type'        => 'select',
            'label'       => esc_html__('Blog Single Type', 'malmo'),
            'description' => esc_html__('Choose a layout type for Blog Single pages', 'malmo'),
            'parent'      => $panel_blog_single,
            'options'     => array(
                'standard'     => esc_html__('Standard', 'malmo'),
                'date-on-side' => esc_html__('Date On Side', 'malmo'),
            ),
        ));

        malmo_elated_add_admin_field(array(
            'name'          => 'blog_single_sidebar_layout',
            'type'          => 'select',
            'label'         => esc_html__('Sidebar Layout', 'malmo'),
            'description'   => esc_html__('Choose a sidebar layout for Blog Single pages', 'malmo'),
            'parent'        => $panel_blog_single,
            'options'       => array(
                'default'          => esc_html__('No Sidebar', 'malmo'),
                'sidebar-33-right' => esc_html__('Sidebar 1/3 Right', 'malmo'),
                'sidebar-25-right' => esc_html__('Sidebar 1/4 Right', 'malmo'),
                'sidebar-33-left'  => esc_html__('Sidebar 1/3 Left', 'malmo'),
                'sidebar-25-left'  => esc_html__('Sidebar 1/4 Left', 'malmo'),
            ),
            'default_value' => 'default'
        ));


        if(count($custom_sidebars) > 0) {
            malmo_elated_add_admin_field(array(
                'name'        => 'blog_single_custom_sidebar',
                'type'        => 'selectblank',
                'label'       => esc_html__('Sidebar to Display', 'malmo'),
                'description' => esc_html__('Choose a sidebar to display on Blog Single pages. Default sidebar is "Sidebar"', 'malmo'),
                'parent'      => $panel_blog_single,
                'options'     => malmo_elated_get_custom_sidebars()
            ));
        }

        malmo_elated_add_admin_field(array(
            'name'          => 'blog_single_title_in_title_area',
            'type'          => 'yesno',
            'label'         => esc_html__('Show Post Title in Title Area', 'malmo'),
            'description'   => esc_html__('Enabling this option will show post title in title area on single post pages', 'malmo'),
            'parent'        => $panel_blog_single,
            'default_value' => 'no'
        ));

        malmo_elated_add_admin_field(array(
            'name'          => 'blog_single_comments',
            'type'          => 'yesno',
            'label'         => esc_html__('Show Comments', 'malmo'),
            'description'   => esc_html__('Enabling this option will show comments on your page.', 'malmo'),
            'parent'        => $panel_blog_single,
            'default_value' => 'yes'
        ));

        malmo_elated_add_admin_field(array(
            'name'          => 'blog_single_related_posts',
            'type'          => 'yesno',
            'label'         => esc_html__('Show Related Posts', 'malmo'),
            'description'   => esc_html__('Enabling this option will show related posts on your single post.', 'malmo'),
            'parent'        => $panel_blog_single,
            'default_value' => 'no'
        ));

        malmo_elated_add_admin_field(
            array(
                'type'          => 'yesno',
                'name'          => 'blog_single_navigation',
                'default_value' => 'no',
                'label'         => esc_html__('Enable Prev/Next Single Post Navigation Links', 'malmo'),
                'parent'        => $panel_blog_single,
                'description'   => esc_html__('Enable navigation links through the blog posts (left and right arrows will appear)', 'malmo'),
                'args'          => array(
                    'dependence'             => true,
                    'dependence_hide_on_yes' => '',
                    'dependence_show_on_yes' => '#eltd_eltd_blog_single_navigation_container'
                )
            )
        );

        $blog_single_navigation_container = malmo_elated_add_admin_container(
            array(
                'name'            => 'eltd_blog_single_navigation_container',
                'hidden_property' => 'blog_single_navigation',
                'hidden_value'    => 'no',
                'parent'          => $panel_blog_single,
            )
        );

        malmo_elated_add_admin_field(
            array(
                'type'          => 'yesno',
                'name'          => 'blog_navigation_through_same_category',
                'default_value' => 'no',
                'label'         => esc_html__('Enable Navigation Only in Current Category', 'malmo'),
                'description'   => esc_html__('Limit your navigation only through current category', 'malmo'),
                'parent'        => $blog_single_navigation_container,
                'args'          => array(
                    'col_width' => 3
                )
            )
        );

        malmo_elated_add_admin_field(array(
            'type'          => 'yesno',
            'name'          => 'blog_enable_single_tags',
            'default_value' => 'yes',
            'label'         => esc_html__('Enable Tags on Single Post', 'malmo'),
            'description'   => esc_html__('Enabling this option will display posts\s tags on single post page', 'malmo'),
            'parent'        => $panel_blog_single
        ));


        malmo_elated_add_admin_field(
            array(
                'type'          => 'yesno',
                'name'          => 'blog_author_info',
                'default_value' => 'no',
                'label'         => esc_html__('Show Author Info Box', 'malmo'),
                'parent'        => $panel_blog_single,
                'description'   => esc_html__('Enabling this option will display author name and descriptions on Blog Single pages', 'malmo'),
                'args'          => array(
                    'dependence'             => true,
                    'dependence_hide_on_yes' => '',
                    'dependence_show_on_yes' => '#eltd_eltd_blog_single_author_info_container'
                )
            )
        );

        $blog_single_author_info_container = malmo_elated_add_admin_container(
            array(
                'name'            => 'eltd_blog_single_author_info_container',
                'hidden_property' => 'blog_author_info',
                'hidden_value'    => 'no',
                'parent'          => $panel_blog_single,
            )
        );

        malmo_elated_add_admin_field(
            array(
                'type'          => 'yesno',
                'name'          => 'blog_author_info_email',
                'default_value' => 'no',
                'label'         => esc_html__('Show Author Email', 'malmo'),
                'description'   => esc_html__('Enabling this option will show author email', 'malmo'),
                'parent'        => $blog_single_author_info_container,
                'args'          => array(
                    'col_width' => 3
                )
            )
        );

    }

    add_action('malmo_elated_options_map', 'malmo_elated_blog_options_map', 12);

}











