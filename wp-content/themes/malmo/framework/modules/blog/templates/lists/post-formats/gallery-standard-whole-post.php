<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="eltd-post-content">
        <?php malmo_elated_get_module_template_part('templates/lists/parts/gallery', 'blog'); ?>
        <div class="eltd-post-text">
            <div class="eltd-post-text-inner clearfix">
                <?php malmo_elated_get_module_template_part('templates/lists/parts/title', 'blog'); ?>
                <div class="eltd-post-info">
                    <?php malmo_elated_post_info(array(
                        'date'     => 'yes',
                        'author'   => 'yes',
                        'category' => 'yes',
                        'comments' => 'yes',
                        'share'    => 'yes',
                        'like'     => 'yes'
                    )) ?>
                </div>
                <?php
                the_content();
                ?>
            </div>
        </div>
    </div>
</article>