<div class="eltd-blog-holder eltd-blog-date-on-side">
    <?php
    if($blog_query->have_posts()) : while($blog_query->have_posts()) : $blog_query->the_post();
        malmo_elated_get_post_format_html($blog_type);
    endwhile;
    else:
        malmo_elated_get_module_template_part('templates/parts/no-posts', 'blog');
    endif;
    ?>

    <?php if(malmo_elated_options()->getOptionValue('pagination') == 'yes') {
        malmo_elated_pagination($blog_query->max_num_pages, $blog_page_range, $paged);
    } ?>
</div>
