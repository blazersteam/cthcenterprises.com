<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="eltd-post-content">
        <div class="eltd-audio-image-holder">
            <?php malmo_elated_get_module_template_part('templates/lists/parts/image', 'blog'); ?>

            <?php if(has_post_thumbnail()) : ?>
                <div class="eltd-audio-player-holder">
                    <?php malmo_elated_get_module_template_part('templates/parts/audio', 'blog'); ?>
                </div>
            <?php endif; ?>
        </div>

        <?php if(!has_post_thumbnail()) : ?>
            <?php malmo_elated_get_module_template_part('templates/parts/audio', 'blog'); ?>
        <?php endif; ?>
        <div class="eltd-date-format">
            <?php if(!malmo_elated_post_has_title()) : ?>
            <a href="<?php esc_url(the_permalink()); ?>">
                <?php endif; ?>

                <span class="eltd-day"><?php the_time($eltd_j) ?></span>
                <span class="eltd-month"><?php the_time($eltd_M) ?></span>

                <?php if(!malmo_elated_post_has_title()) : ?>
            </a>
        <?php endif; ?>
        </div>
        <div class="eltd-post-text">
            <div class="eltd-post-text-inner">
                <div class="eltd-post-info">
                    <?php malmo_elated_post_info(array(
                        'category' => 'yes',
                        'comments' => 'yes',
                        'like'     => 'yes'
                    )) ?>
                </div>
                <?php malmo_elated_get_module_template_part('templates/lists/parts/title', 'blog'); ?>
                <?php
                malmo_elated_excerpt($excerpt_length);
                ?>
            </div>
        </div>
        <div class="eltd-author-desc clearfix">
            <div class="eltd-image-name">
                <div class="eltd-author-image">
                    <?php echo malmo_elated_kses_img(get_avatar(get_the_author_meta('ID'), 102)); ?>
                </div>
                <div class="eltd-author-name-holder">
                    <h6 class="eltd-author-name">
                        <a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))); ?>">
                            <span><?php esc_html_e('by', 'malmo'); ?></span>
                            <?php
                            if(get_the_author_meta('first_name') != "" || get_the_author_meta('last_name') != "") {
                                echo esc_attr(get_the_author_meta('first_name'))." ".esc_attr(get_the_author_meta('last_name'));
                            } else {
                                echo esc_attr(get_the_author_meta('display_name'));
                            }
                            ?>
                        </a>
                    </h6>
                </div>
            </div>
            <div class="eltd-share-icons">
                <?php $post_info_array['share'] = malmo_elated_options()->getOptionValue('enable_social_share') == 'yes'; ?>
                <?php if($post_info_array['share'] == 'yes'): ?>
                    <span class="eltd-share"><?php esc_html_e('Share', 'malmo'); ?></span>
                <?php endif; ?>
                <?php echo malmo_elated_get_social_share_html(array(
                    'type'      => 'list',
                    'icon_type' => 'circle'
                )); ?>
            </div>
        </div>
    </div>
</article>