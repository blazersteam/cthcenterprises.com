<?php if(malmo_elated_options()->getOptionValue('blog_single_navigation') == 'yes') { ?>
    <?php $navigation_blog_through_category = malmo_elated_options()->getOptionValue('blog_navigation_through_same_category') ?>
    <div class="eltd-blog-single-navigation clearfix">
        <div class="eltd-blog-single-navigation-inner clearfix">
            <?php if($has_prev_post) : ?>
                <div class="eltd-blog-single-prev clearfix">
                    <?php if($prev_post_has_image) : ?>
                        <div class="eltd-single-nav-image-holder">
                            <a href="<?php echo esc_url($prev_post['link']); ?>">
                            </a>
                        </div>
                    <?php endif; ?>

                    <div class="eltd-single-nav-content-holder clearfix">
                        <div class="eltd-single-nav-text">
                            <h3 class="eltd-single-nav-post-title">
                                <a href="<?php echo esc_url($prev_post['link']); ?>">
                                    <?php echo esc_html($prev_post['title']); ?>
                                </a>
                            </h3>
                            <a href="<?php echo esc_url($prev_post['link']) ?>">
                                <span class="eltd-single-nav-post-sub"><?php esc_html_e('Previous post', 'malmo') ?></span>
                            </a>
                            <div class="eltd-single-nav-arrow">
                                <a href="<?php echo esc_url($prev_post['link']) ?>"><?php echo malmo_elated_icon_collections()->renderIcon('lnr-chevron-left', 'linear_icons') ?></a>
                            </div>
                        </div>
                    </div>
                </div> <!-- close div.blog_prev -->
            <?php endif; ?>
            <?php if($has_next_post) : ?>
                <div class="eltd-blog-single-next clearfix">
                    <?php if($next_post_has_image) : ?>
                        <div class="eltd-single-nav-image-holder clearfix">
                            <a href="<?php echo esc_url($next_post['link']); ?>">
                            </a>
                        </div>
                    <?php endif; ?>

                    <div class="eltd-single-nav-content-holder clearfix">
                        <div class="eltd-single-nav-text">
                            <h3 class="eltd-single-nav-post-title">
                                <a href="<?php echo esc_url($next_post['link']); ?>">
                                    <?php echo esc_html($next_post['title']); ?>
                                </a>
                            </h3>
                            <a href="<?php echo esc_url($next_post['link']) ?>">
                                <span class="eltd-single-nav-post-sub"><?php esc_html_e('Next post', 'malmo') ?></span>
                            </a>
                            <div class="eltd-single-nav-arrow">
                                <a href="<?php echo esc_url($next_post['link']) ?>"><?php echo malmo_elated_icon_collections()->renderIcon('lnr-chevron-right', 'linear_icons') ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <?php if($has_prev_post || $has_next_post) : ?>
                <div class="eltd-single-nav-separator"></div>
            <?php endif; ?>
        </div>
    </div>
<?php } ?>