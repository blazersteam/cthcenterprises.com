<div class="eltd-grid-row">
    <div <?php echo malmo_elated_get_content_sidebar_class(); ?>>
        <div class="eltd-blog-holder eltd-blog-single <?php echo esc_attr($single_template); ?>">
            <?php malmo_elated_get_single_html(); ?>
        </div>
    </div>

    <?php if(!in_array($sidebar, array('default', ''))) : ?>
        <div <?php echo malmo_elated_get_sidebar_holder_class(); ?>>
            <?php get_sidebar(); ?>
        </div>
    <?php endif; ?>
</div>