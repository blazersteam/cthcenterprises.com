<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="eltd-post-content">
        <div class="eltd-post-text">
            <div class="eltd-post-text-inner clearfix">
                <div class="eltd-post-quote-holder">
                    <?php 
                    $quote_text = get_post_meta(get_the_ID(), "eltd_post_quote_text_meta", true);
                    $quote_author = get_post_meta(get_the_ID(), "eltd_post_quote_author_meta", true);
                    $quote_position = get_post_meta(get_the_ID(), "eltd_post_quote_position_meta", true);
                    ?>
                    <div class="eltd-post-mark"><span aria-hidden="true" class="icon_quotations"></span></div>
                    <!--<div class="eltd-post-quote-text"><h3><a href="<?php echo get_permalink(); ?>" target="_blank"><?php echo esc_html($quote_text); ?></a></h3></div>-->
                    <?php malmo_elated_get_module_template_part('templates/single/parts/title', 'blog'); ?>
                    <div class="eltd-post-quote-author">
                        <h5 class="eltd-post-quote-author-text">
                        <span class="eltd-post-quote-author-name"><?php echo esc_html($quote_author) ?></span>
                        <?php if($quote_position !== '') { ?>
                            <span class="eltd-post-quote-position"><?php echo esc_attr($quote_position) ?></span>
                        <?php } ?>
                        </h5>
                    </div>
                    <div class="eltd-post-info">
                        <?php malmo_elated_post_info(array(
                                 'date'     => 'yes',
                                 'like'     => 'yes',
                                 'comments' => 'yes',
                                 'category' => 'yes'
                        )) ?>
                    </div>
                    <div class="eltd-share-icons-single">
                        <?php $post_info_array['share'] = malmo_elated_options()->getOptionValue('enable_social_share') == 'yes'; ?>
                        <?php if($post_info_array['share'] == 'yes'): ?>
                            <span class="eltd-share-label"><?php esc_html_e('Share:', 'malmo'); ?></span>
                        <?php endif; ?>
                        <?php echo malmo_elated_get_social_share_html(array(
                            'type'      => 'list',
                            'icon_type' => 'normal'
                        )); ?>
                    </div>
                </div>
                <?php the_content(); ?>
            </div>
            <div class="eltd-tags-share-holder clearfix">
                <?php
                if(has_tag() && malmo_elated_options()->getOptionValue('blog_enable_single_tags') == 'yes') {
                    malmo_elated_get_module_template_part('templates/single/parts/tags', 'blog');
                }
                ?>
            </div>
        </div>
    </div>
</article>