<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="eltd-post-content">
        <?php malmo_elated_get_module_template_part('templates/single/parts/gallery', 'blog'); ?>
        <div class="eltd-post-text">
            <div class="eltd-post-text-inner clearfix">
                <?php malmo_elated_get_module_template_part('templates/single/parts/title', 'blog'); ?>
                <div class="eltd-post-info">
                    <?php malmo_elated_post_info(array('author'   => 'yes',
                                                             'like'     => 'yes',
                                                             'comments' => 'yes',
                                                             'category' => 'yes'
                    )) ?>
                </div>
                <?php the_content(); ?>
            </div>
            <div class="eltd-tags-share-holder clearfix">
                <?php
                if(has_tag() && malmo_elated_options()->getOptionValue('blog_enable_single_tags') == 'yes') {
                    malmo_elated_get_module_template_part('templates/single/parts/tags', 'blog');
                }
                ?>
                <div class="eltd-share-icons-single">
                    <?php $post_info_array['share'] = malmo_elated_options()->getOptionValue('enable_social_share') == 'yes'; ?>
                    <?php if($post_info_array['share'] == 'yes'): ?>
                        <span class="eltd-share-label"><?php esc_html_e('Share', 'malmo'); ?></span>
                    <?php endif; ?>
                    <?php echo malmo_elated_get_social_share_html(array(
                        'type'      => 'list',
                        'icon_type' => 'normal'
                    )); ?>
                </div>
            </div>
        </div>
    </div>
    <?php do_action('malmo_elated_before_blog_article_closed_tag'); ?>
</article>