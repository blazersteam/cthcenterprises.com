<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <a href="<?php the_permalink(); ?>">
        <div class="eltd-post-content clearfix">
            <div class="eltd-post-text">
                <div class="eltd-post-text-inner">
                    <div class="eltd-post-mark">
                        <?php echo malmo_elated_icon_collections()->renderIcon('icon_link', 'font_elegant'); ?>
                    </div>
                    <h4 class="eltd-post-title"><?php the_title(); ?></h4>

                    <div class="eltd-post-info">
                        <?php malmo_elated_post_info(array(
                            'date'     => 'yes',
                            'like'     => 'yes',
                            'comments' => 'yes'
                        )) ?>
                    </div>
                </div>
            </div>
        </div>
    </a>

    <div class="eltd-link-content">
        <?php the_content(); ?>
    </div>
    <div class="eltd-share-icons-single">
        <?php $post_info_array['share'] = malmo_elated_options()->getOptionValue('enable_social_share') == 'yes'; ?>
        <?php if($post_info_array['share'] == 'yes'): ?>
            <span class="eltd-share-label"><?php esc_html_e('Share', 'malmo'); ?></span>
        <?php endif; ?>
        <?php echo malmo_elated_get_social_share_html(array(
            'type'      => 'list',
            'icon_type' => 'normal'
        )); ?>
    </div>
</article>