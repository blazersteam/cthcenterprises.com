<?php

if(!function_exists('malmo_elated_footer_bg_image_styles')) {
    /**
     * Outputs background image styles for footer
     */
    function malmo_elated_footer_bg_image_styles() {
        $background_image = malmo_elated_options()->getOptionValue('footer_background_image');

        if($background_image !== '') {
            $footer_bg_image_styles['background-image'] = 'url('.$background_image.')';

            echo malmo_elated_dynamic_css('body.eltd-footer-with-bg-image .eltd-page-footer', $footer_bg_image_styles);
        }
    }

    add_action('malmo_elated_style_dynamic', 'malmo_elated_footer_bg_image_styles');
}
