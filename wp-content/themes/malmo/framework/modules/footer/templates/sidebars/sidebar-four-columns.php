<?php $cols = 4; ?>

<div class="eltd-grid-row eltd-footer-top-four-cols">
	<?php for($i = 1; $i <= $cols; $i++) : ?>
		<div class="eltd-grid-col-3 eltd-grid-col-ipad-landscape-6 eltd-grid-col-ipad-portrait-12">
			<?php if(is_active_sidebar('footer_column_'.$i)) :
				dynamic_sidebar('footer_column_'.$i);
			endif; ?>
		</div>
	<?php endfor; ?>
</div>