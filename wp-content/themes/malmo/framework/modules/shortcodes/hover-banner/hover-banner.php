<?php
namespace Malmo\Modules\HoverBanner;

use Malmo\Modules\Shortcodes\Lib\ShortcodeInterface;

/**
 * Class HoverBanner
 */
class HoverBanner implements ShortcodeInterface {

    /**
     * @var string
     */
    private $base;

    public function __construct() {
        $this->base = 'eltd_hover_banner';

        add_action('vc_before_init', array($this, 'vcMap'));
    }

    /**
     * Returns base for shortcode
     * @return string
     */
    public function getBase() {
        return $this->base;
    }

    /**
     * Maps shortcode to Visual Composer. Hooked on vc_before_init
     *
     * @see eltd_core_get_carousel_slider_array_vc()
     */
    public function vcMap() {

        vc_map(array(
            'name'                      => esc_html__('Hover Banner', 'malmo'),
            'base'                      => $this->getBase(),
            'category'                  => esc_html__('by ELATED', 'malmo'),
            'icon'                      => 'icon-wpb-hover-banner extended-custom-icon',
            'allowed_container_element' => 'vc_row',
            'params'                    => array(
                array(
                    'type'        => 'attach_image',
                    'heading'     => esc_html__('Image', 'malmo'),
                    'param_name'  => 'image',
                    'value'       => '',
                    'save_always' => true,
                    'admin_label' => true
                ),
                array(
                    'type'        => 'colorpicker',
                    'heading'     => esc_html__('Hover Sheet Color', 'malmo'),
                    'param_name'  => 'hover_sheet_color',
                    'admin_label' => true,
                ),
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__('Hover Sheet Opacity', 'malmo'),
                    'param_name'  => 'hover_sheet_opacity',
                    'description' => esc_html__('Values 0-1. Default is 1.', 'malmo'),
                ),
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__('Text', 'malmo'),
                    'param_name'  => 'text',
                ),
                array(
                    'type'        => 'dropdown',
                    'heading'     => esc_html__('Text Tag', 'malmo'),
                    'param_name'  => 'text_tag',
                    'admin_label' => true,
                    'value'       => array(
                        ''      => '',
                        'h2'    => 'h2',
                        'h3'    => 'h3',
                        'h4'    => 'h4',
                        'h5'    => 'h5',
                        'h6'    => 'h6',
                        'p'     => 'p'
                    ),
                    'dependency'  => array('element' => 'text', 'not_empty' => true)
                ),
                array(
                    'type'        => 'colorpicker',
                    'heading'     => esc_html__('Text Color', 'malmo'),
                    'param_name'  => 'text_color',
                    'admin_label' => true,
                    'dependency'  => array('element' => 'text', 'not_empty' => true)
                ),
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__('Link', 'malmo'),
                    'param_name'  => 'link',
                ),
                array(
                    'type'        => 'dropdown',
                    'heading'     => esc_html__('Link Target', 'malmo'),
                    'param_name'  => 'link_target',
                    'admin_label' => true,
                    'value'       => array(
                        esc_html__('Self', 'malmo') => '_self',
                        esc_html__('Blank', 'malmo')  => '_blank'
                    ),
                    'save_always' => true,
                    'dependency'  => array('element' => 'link', 'not_empty' => true)
                ),
            )
        ));

    }

    /**
     * Renders shortcodes HTML
     *
     * @param $atts array of shortcode params
     * @param $content string shortcode content
     *
     * @return string
     */
    public function render($atts, $content = null) {

        $args = array(
            'image'                   => '',
            'hover_sheet_color'       => '',
            'hover_sheet_opacity'     => '1',
            'text'                    => '',
            'text_tag'                => 'h2',
            'text_color'              => '',
            'link'                    => '',
            'link_target'             => '_self'
        );

        $params = shortcode_atts($args, $atts);

        //Get HTML from template
        $html = malmo_elated_get_shortcode_module_template_part('templates/hover-banner', 'hover-banner', '', $params);

        return $html;

    }
}