<div class="eltd-team-list eltd-team-list-<?php echo esc_attr($columns); ?>-columns <?php if ($space_between=='yes') echo 'eltd-team-list-space-between'; ?>">
	<div class="eltd-team-list-inner clearfix">
		<?php echo do_shortcode($content); ?>
	</div>
</div>