<?php
namespace Malmo\Modules\UnorderedList;

use Malmo\Modules\Shortcodes\Lib\ShortcodeInterface;

/**
 * Class unordered List
 */
class UnorderedList implements ShortcodeInterface {

    private $base;

    function __construct() {
        $this->base = 'eltd_unordered_list';

        add_action('vc_before_init', array($this, 'vcMap'));
    }

    /**\
     * Returns base for shortcode
     * @return string
     */
    public function getBase() {
        return $this->base;
    }

    public function vcMap() {

        vc_map(array(
            'name'                      => esc_html__('List - Unordered', 'malmo'),
            'base'                      => $this->base,
            'icon'                      => 'icon-wpb-unordered-list extended-custom-icon',
            'category'                  => esc_html__('by ELATED', 'malmo'),
            'allowed_container_element' => 'vc_row',
            'params'                    => array(
                array(
                    'type'        => 'dropdown',
                    'admin_label' => true,
                    'heading'     => esc_html__('Style', 'malmo'),
                    'param_name'  => 'style',
                    'value'       => array(
                        esc_html__('Circle', 'malmo') => 'circle',
                        esc_html__('Line', 'malmo')   => 'line'
                    ),
                    'description' => ''
                ),
                array(
                    'type'        => 'dropdown',
                    'admin_label' => true,
                    'heading'     => esc_html__('Animate List', 'malmo'),
                    'param_name'  => 'animate',
                    'value'       => array(
                        esc_html__('No', 'malmo')  => 'no',
                        esc_html__('Yes', 'malmo') => 'yes'
                    ),
                    'description' => ''
                ),
                array(
                    'type'        => 'dropdown',
                    'heading'     => esc_html__('Font Weight', 'malmo'),
                    'param_name'  => 'font_weight',
                    'value'       => array(
                        esc_html__('Default', 'malmo') => '',
                        esc_html__('Light', 'malmo')   => 'light',
                        esc_html__('Normal', 'malmo')  => 'normal',
                        esc_html__('Bold', 'malmo')    => 'bold'
                    ),
                    'description' => ''
                ),
                array(
                    'type'       => 'textfield',
                    'heading'    => esc_html__('Padding left (px)', 'malmo'),
                    'param_name' => 'padding_left',
                    'value'      => ''
                ),
                array(
                    'type'        => 'textarea_html',
                    'heading'     => esc_html__('Content', 'malmo'),
                    'param_name'  => 'content',
                    'value'       => '<ul><li>Lorem Ipsum</li><li>Lorem Ipsum</li><li>Lorem Ipsum</li></ul>',
                    'description' => ''
                )
            )
        ));

    }


    public function render($atts, $content = null) {
        $args   = array(
            'style'        => '',
            'animate'      => '',
            'font_weight'  => '',
            'padding_left' => ''
        );
        $params = shortcode_atts($args, $atts);

        //Extract params for use in method
        extract($params);

        $list_item_classes = "";

        if($style != '') {
            if($style == 'circle') {
                $list_item_classes .= ' eltd-circle';
            } elseif($style == 'line') {
                $list_item_classes .= ' eltd-line';
            }
        }

        if($animate == 'yes') {
            $list_item_classes .= ' eltd-animate-list';
        }

        $list_style = '';
        if($padding_left != '') {
            $list_style .= 'padding-left: '.$padding_left.'px;';
        }
        $html = '';

        $html .= '<div class="eltd-unordered-list '.$list_item_classes.'" '.malmo_elated_get_inline_style($list_style).'>';
        $html .= malmo_elated_remove_auto_ptag($content, true);
        $html .= '</div>';

        return $html;
    }

}