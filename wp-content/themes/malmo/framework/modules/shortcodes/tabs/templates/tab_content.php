<?php
$tab_data_str = '';
$icon_html    = '';
$tab_data_str .= 'data-icon-pack="'.$icon_pack.'" ';
$icon_html .= malmo_elated_icon_collections()->renderIcon($icon, $icon_pack, array());
$tab_data_str .= 'data-icon-html="'.esc_attr($icon_html).'"';
?>
<div <?php malmo_elated_class_attribute($tab_classes); ?> <?php malmo_elated_inline_style($tab_styles); ?> id="tab-<?php echo sanitize_title( $tab_title )?>" <?php echo malmo_elated_get_module_part($tab_data_str);?>>
	<?php echo malmo_elated_remove_auto_ptag($content); ?>
</div>
