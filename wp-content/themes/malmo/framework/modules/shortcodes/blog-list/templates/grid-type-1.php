<li <?php post_class('eltd-blog-list-item clearfix'); ?>>
	<div class="eltd-blog-list-item-inner">
		<?php if(!$hide_image) : ?>
			<div class="eltd-item-image <?php if (has_post_thumbnail()) echo 'eltd-post-with-image'; ?>">
				<div class="eltd-blog-list-image-wrapper">
					<a href="<?php echo esc_url(get_permalink()) ?>">
						<?php if($use_custom_image_size) : ?>
							<?php echo malmo_elated_generate_thumbnail(
								get_post_thumbnail_id(get_the_ID()),
								null,
								$custom_image_dimensions[0],
								$custom_image_dimensions[1]
							); ?>
						<?php else: ?>
							<?php the_post_thumbnail($thumb_image_size); ?>
						<?php endif; ?>
					</a>
				</div>
				<div class="eltd-post-info-date-wrapper">
					<?php malmo_elated_post_boxed_date(); ?>
				</div>
			</div>
		<?php endif; ?>
		<div class="eltd-item-text-holder">
			<<?php echo esc_html($title_tag) ?> class="eltd-item-title">
			<a href="<?php echo esc_url(get_permalink()) ?>">
				<?php echo esc_attr(get_the_title()) ?>
			</a>
			</<?php echo esc_html($title_tag) ?>>
			<div class="eltd-post-info">
                <?php the_tags('', ', ', ''); ?>
            </div>

			<?php if($text_length != '0') {
				$excerpt = ($text_length > 0) ? substr(get_the_excerpt(), 0, intval($text_length)) : get_the_excerpt(); ?>
				<p class="eltd-excerpt"><?php echo esc_html($excerpt) ?></p>
			<?php } ?>
		</div>
	</div>
</li>