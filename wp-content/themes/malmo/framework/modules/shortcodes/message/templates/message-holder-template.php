<div class="eltd-message  <?php echo esc_attr($message_classes) ?>" <?php echo malmo_elated_get_inline_style($message_styles) ?>>
	<div class="eltd-message-inner">
		<?php
		if($type == 'with_icon') {
			$icon_html = malmo_elated_get_shortcode_module_template_part('templates/'.$type, 'message', '', $params);
			echo malmo_elated_get_module_part($icon_html);
		}
		?>
		<a href="#" class="eltd-close">
			<i class="eltd_font_elegant_icon icon_close" <?php malmo_elated_inline_style($close_icon_style); ?>></i>
		</a>

		<div class="eltd-message-text-holder">
			<div class="eltd-message-text">
				<div class="eltd-message-text-inner">
					<?php echo malmo_elated_remove_auto_ptag($content, true) ?>
				</div>
			</div>
		</div>
	</div>
</div>
