<?php
/**
 * Blockquote shortcode template
 */
?>

<blockquote class="eltd-blockquote-shortcode" <?php malmo_elated_inline_style($blockquote_style); ?> >
	<span class="eltd-icon-quotations-holder">
		<span aria-hidden="true" class="icon_quotations"></span>
	</span>
	<h6 class="eltd-blockquote-text">
		<span><?php echo esc_attr($text); ?></span>
	</h6>
</blockquote>