<div <?php malmo_elated_class_attribute($classes); ?> <?php malmo_elated_inline_style($holder_styles); ?>>
	<p <?php malmo_elated_inline_style($styles); ?> class="eltd-section-subtitle"><?php echo wp_kses_post($text); ?></p>
</div>