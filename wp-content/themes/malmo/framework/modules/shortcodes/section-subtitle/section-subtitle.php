<?php
namespace Malmo\Modules\Shortcodes\SectionSubtitle;

use Malmo\Modules\Shortcodes\Lib;

class SectionSubtitle implements Lib\ShortcodeInterface {
    private $base;

    /**
     * SectionSubtitle constructor.
     */
    public function __construct() {
        $this->base = 'eltd_section_subtitle';

        add_action('vc_before_init', array($this, 'vcMap'));
    }


    public function getBase() {
        return $this->base;
    }

    public function vcMap() {
        vc_map(array(
            'name'                      => esc_html__('Section Subtitle', 'malmo'),
            'base'                      => $this->base,
            'category'                  => esc_html__('by ELATED', 'malmo'),
            'icon'                      => 'icon-wpb-section-subtitle extended-custom-icon',
            'allowed_container_element' => 'vc_row',
            'params'                    => array(
                array(
                    'type'        => 'colorpicker',
                    'heading'     => esc_html__('Color', 'malmo'),
                    'param_name'  => 'color',
                    'value'       => '',
                    'save_always' => true,
                    'admin_label' => true,
                    'description' => esc_html__('Choose color of your subtitle', 'malmo')
                ),
                array(
                    'type'        => 'dropdown',
                    'heading'     => esc_html__('Text Align', 'malmo'),
                    'param_name'  => 'text_align',
                    'value'       => array(
                        ''                                  => '',
                        esc_html__('Center', 'malmo') => 'center',
                        esc_html__('Left', 'malmo')   => 'left',
                        esc_html__('Right', 'malmo')  => 'right'
                    ),
                    'save_always' => true,
                    'admin_label' => true,
                    'description' => esc_html__('Choose color of your subtitle', 'malmo')
                ),
                array(
                    'type'        => 'textarea',
                    'heading'     => esc_html__('Text', 'malmo'),
                    'param_name'  => 'text',
                    'value'       => '',
                    'save_always' => true,
                    'admin_label' => true
                ),
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__('Width (%)', 'malmo'),
                    'param_name'  => 'width',
                    'description' => esc_html__('Adjust the width of section subtitle in percentages. Ommit the unit', 'malmo'),
                    'value'       => '',
                    'save_always' => true,
                    'admin_label' => true
                )
            )
        ));
    }

    public function render($atts, $content = null) {
        $default_atts = array(
            'text'       => '',
            'color'      => '',
            'text_align' => '',
            'width'      => ''
        );

        $params = shortcode_atts($default_atts, $atts);

        if($params['text'] !== '') {

            $params['styles']  = array();
            $params['classes'] = array('eltd-section-subtitle-holder');

            if($params['color'] !== '') {
                $params['styles'][] = 'color: '.$params['color'];
            }

            if($params['text_align'] !== '') {
                $params['styles'][] = 'text-align: '.$params['text_align'];

                $params['classes'][] = 'eltd-section-subtitle-'.$params['text_align'];
            }

            $params['holder_styles'] = array();

            if($params['width'] !== '') {
                $params['holder_styles'][] = 'width: '.$params['width'].'%';
            }

            return malmo_elated_get_shortcode_module_template_part('templates/section-subtitle-template', 'section-subtitle', '', $params);
        }
    }

}
