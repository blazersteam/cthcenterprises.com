<a href="<?php echo esc_url($link); ?>" target="<?php echo esc_attr($target); ?>" <?php malmo_elated_inline_style($button_styles); ?> <?php malmo_elated_class_attribute($button_classes); ?> <?php echo malmo_elated_get_inline_attrs($button_data); ?> <?php echo malmo_elated_get_inline_attrs($button_custom_attrs); ?>>
	<span class="eltd-btn-text"><?php echo esc_html($text); ?></span>
	<?php if($show_icon) : ?>
		<span class="eltd-btn-icon-holder">
			<?php echo malmo_elated_icon_collections()->renderIcon($icon, $icon_pack, array(
				'icon_attributes' => array(
					'class' => 'eltd-btn-icon-elem'
				)
			)); ?>
		</span>
	<?php endif; ?>

	<?php if($display_helper) : ?>
		<span class="eltd-btn-helper" <?php malmo_elated_inline_style($helper_styles); ?>></span>
	<?php endif; ?>
</a>