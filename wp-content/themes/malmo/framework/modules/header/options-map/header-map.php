<?php

if(!function_exists('malmo_elated_header_options_map')) {

    function malmo_elated_header_options_map() {

        malmo_elated_add_admin_page(
            array(
                'slug'  => '_header_page',
                'title' => esc_html__('Header', 'malmo'),
                'icon'  => 'fa fa-header'
            )
        );

        $panel_header = malmo_elated_add_admin_panel(
            array(
                'page'  => '_header_page',
                'name'  => 'panel_header',
                'title' => esc_html__('Header', 'malmo')
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $panel_header,
                'type'          => 'radiogroup',
                'name'          => 'header_type',
                'default_value' => 'header-standard',
                'label'         => esc_html__('Choose Header Type', 'malmo'),
                'description'   => esc_html__('Select the type of header you would like to use', 'malmo'),
                'options'       => array(
                    'header-standard' => array(
                        'image' => ELATED_FRAMEWORK_ROOT.'/admin/assets/img/header-standard.png',
                        'label' => esc_html__('Standard', 'malmo')
                    ),
                    'header-minimal'  => array(
                        'image' => ELATED_FRAMEWORK_ROOT.'/admin/assets/img/header-minimal.png',
                        'label' => esc_html__('Minimal', 'malmo')
                    ),
                    'header-centered' => array(
                        'image' => ELATED_FRAMEWORK_ROOT.'/admin/assets/img/header-centered.png',
                        'label' => esc_html__('Centered', 'malmo')
                    ),
                    'header-vertical' => array(
                        'image' => ELATED_FRAMEWORK_ROOT.'/admin/assets/img/header-vertical.png',
                        'label' => esc_html__('Vertical', 'malmo')
                    )
                ),
                'args'          => array(
                    'use_images'  => true,
                    'hide_labels' => true,
                    'dependence'  => true,
                    'show'        => array(
                        'header-standard' => '#eltd_panel_header_standard, #eltd_header_behaviour,#eltd_panel_sticky_header, #eltd_panel_main_menu',
                        'header-minimal'  => '#eltd_panel_header_minimal,#eltd_header_behaviour,#eltd_panel_sticky_header',
                        'header-centered' => '#eltd_panel_header_centered,#eltd_header_behaviour,#eltd_panel_sticky_header,#eltd_panel_main_menu',
                        'header-vertical' => '#eltd_panel_header_vertical, #eltd_panel_vertical_main_menu',
                    ),
                    'hide'        => array(
                        'header-standard' => '#eltd_panel_header_vertical, #eltd_panel_vertical_main_menu, #eltd_panel_header_minimal, #eltd_panel_header_centered, #eltd_panel_fixed_header',
                        'header-minimal'  => '#eltd_panel_header_standard, #eltd_panel_main_menu, #eltd_panel_header_vertical, #eltd_panel_vertical_main_menu, #eltd_panel_fixed_header, #eltd_panel_header_centered',
                        'header-centered' => '#eltd_panel_header_standard, #eltd_panel_header_minimal, #eltd_panel_header_vertical, #eltd_panel_vertical_main_menu, #eltd_panel_fixed_header',
                        'header-vertical' => '#eltd_panel_header_standard, #eltd_panel_header_minimal, #eltd_panel_header_centered, #eltd_panel_fixed_header,#eltd_header_behaviour,#eltd_panel_sticky_header,#eltd_panel_main_menu',
                    )
                )
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'          => $panel_header,
                'type'            => 'select',
                'name'            => 'header_behaviour',
                'default_value'   => 'sticky-header-on-scroll-up',
                'label'           => esc_html__('Choose Header behaviour', 'malmo'),
                'description'     => esc_html__('Select the behaviour of header when you scroll down to page', 'malmo'),
                'options'         => array(
                    'no-behavior'                     => esc_html__('No Behavior', 'malmo'),
                    'sticky-header-on-scroll-up'      => esc_html__('Sticky on scrol up', 'malmo'),
                    'sticky-header-on-scroll-down-up' => esc_html__('Sticky on scrol up/down', 'malmo'),
                    'fixed-on-scroll'                 => esc_html__('Fixed on scroll', 'malmo')
                ),
                'hidden_property' => 'header_type',
                'hidden_value'    => '',
                'hidden_values'   => array('header-vertical'),
                'args'            => array(
                    'dependence' => true,
                    'show'       => array(
                        'sticky-header-on-scroll-up'      => '#eltd_panel_sticky_header',
                        'sticky-header-on-scroll-down-up' => '#eltd_panel_sticky_header',
                        'fixed-on-scroll'                 => '#eltd_panel_fixed_header'
                    ),
                    'hide'       => array(
                        'sticky-header-on-scroll-up'      => '#eltd_panel_fixed_header',
                        'sticky-header-on-scroll-down-up' => '#eltd_panel_fixed_header',
                        'fixed-on-scroll'                 => '#eltd_panel_sticky_header',
                        'no-behavior'                     => '#eltd_panel_fixed_header, #eltd_panel_fixed_header, #eltd_panel_sticky_header'
                    )
                )
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'          => 'top_bar',
                'type'          => 'yesno',
                'default_value' => 'no',
                'label'         => esc_html__('Top Bar', 'malmo'),
                'description'   => esc_html__('Enabling this option will show top bar area', 'malmo'),
                'parent'        => $panel_header,
                'args'          => array(
                    "dependence"             => true,
                    "dependence_hide_on_yes" => "",
                    "dependence_show_on_yes" => "#eltd_top_bar_container"
                )
            )
        );

        $top_bar_container = malmo_elated_add_admin_container(array(
            'name'            => 'top_bar_container',
            'parent'          => $panel_header,
            'hidden_property' => 'top_bar',
            'hidden_value'    => 'no'
        ));

        malmo_elated_add_admin_field(
            array(
                'parent'        => $top_bar_container,
                'type'          => 'select',
                'name'          => 'top_bar_layout',
                'default_value' => 'three-columns',
                'label'         => esc_html__('Choose top bar layout', 'malmo'),
                'description'   => esc_html__('Select the layout for top bar', 'malmo'),
                'options'       => array(
                    'two-columns'   => esc_html__('Two columns', 'malmo'),
                    'three-columns' => esc_html__('Three columns', 'malmo')
                ),
                'args'          => array(
                    'dependence' => true,
                    'hide'       => array(
                        'two-columns'   => '#eltd_top_bar_layout_container',
                        'three-columns' => '#eltd_top_bar_two_columns_layout_container'
                    ),
                    'show'       => array(
                        'two-columns'   => '#eltd_top_bar_two_columns_layout_container',
                        'three-columns' => '#eltd_top_bar_layout_container'
                    )
                )
            )
        );

        $top_bar_layout_container = malmo_elated_add_admin_container(array(
            'name'            => 'top_bar_layout_container',
            'parent'          => $top_bar_container,
            'hidden_property' => 'top_bar_layout',
            'hidden_value'    => '',
            'hidden_values'   => array('two-columns'),
        ));

        malmo_elated_add_admin_field(
            array(
                'parent'        => $top_bar_layout_container,
                'type'          => 'select',
                'name'          => 'top_bar_column_widths',
                'default_value' => '30-30-30',
                'label'         => esc_html__('Choose column widths', 'malmo'),
                'description'   => '',
                'options'       => array(
                    '30-30-30' => '33% - 33% - 33%',
                    '25-50-25' => '25% - 50% - 25%'
                )
            )
        );

        $top_bar_two_columns_layout = malmo_elated_add_admin_container(array(
            'name'            => 'top_bar_two_columns_layout_container',
            'parent'          => $top_bar_container,
            'hidden_property' => 'top_bar_layout',
            'hidden_value'    => '',
            'hidden_values'   => array('three-columns'),
        ));

        malmo_elated_add_admin_field(
            array(
                'parent'        => $top_bar_two_columns_layout,
                'type'          => 'select',
                'name'          => 'top_bar_two_column_widths',
                'default_value' => '50-50',
                'label'         => esc_html__('Choose column widths', 'malmo'),
                'description'   => '',
                'options'       => array(
                    '50-50' => '50% - 50%',
                    '33-66' => '33% - 66%',
                    '66-33' => '66% - 33%'
                )
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'          => 'top_bar_in_grid',
                'type'          => 'yesno',
                'default_value' => 'yes',
                'label'         => esc_html__('Top Bar in grid', 'malmo'),
                'description'   => esc_html__('Set top bar content to be in grid', 'malmo'),
                'parent'        => $top_bar_container,
                'args'          => array(
                    "dependence"             => true,
                    "dependence_hide_on_yes" => "",
                    "dependence_show_on_yes" => "#eltd_top_bar_in_grid_container"
                )
            )
        );

        $top_bar_in_grid_container = malmo_elated_add_admin_container(array(
            'name'            => 'top_bar_in_grid_container',
            'parent'          => $top_bar_container,
            'hidden_property' => 'top_bar_in_grid',
            'hidden_value'    => 'no'
        ));

        malmo_elated_add_admin_field(array(
            'name'        => 'top_bar_grid_background_color',
            'type'        => 'color',
            'label'       => esc_html__('Grid Background Color', 'malmo'),
            'description' => esc_html__('Set grid background color for top bar', 'malmo'),
            'parent'      => $top_bar_in_grid_container
        ));


        malmo_elated_add_admin_field(array(
            'name'        => 'top_bar_grid_background_transparency',
            'type'        => 'text',
            'label'       => esc_html__('Grid Background Transparency', 'malmo'),
            'description' => esc_html__('Set grid background transparency for top bar', 'malmo'),
            'parent'      => $top_bar_in_grid_container,
            'args'        => array('col_width' => 3)
        ));

        malmo_elated_add_admin_field(array(
            'name'        => 'top_bar_background_color',
            'type'        => 'color',
            'label'       => esc_html__('Background Color', 'malmo'),
            'description' => esc_html__('Set background color for top bar', 'malmo'),
            'parent'      => $top_bar_container
        ));

        malmo_elated_add_admin_field(array(
            'name'        => 'top_bar_background_transparency',
            'type'        => 'text',
            'label'       => esc_html__('Background Transparency', 'malmo'),
            'description' => esc_html__('Set background transparency for top bar', 'malmo'),
            'parent'      => $top_bar_container,
            'args'        => array('col_width' => 3)
        ));

        malmo_elated_add_admin_field(array(
            'name'        => 'top_bar_height',
            'type'        => 'text',
            'label'       => esc_html__('Top bar height', 'malmo'),
            'description' => esc_html__('Enter top bar height (Default is 40px)', 'malmo'),
            'parent'      => $top_bar_container,
            'args'        => array(
                'col_width' => 2,
                'suffix'    => 'px'
            )
        ));

        malmo_elated_add_admin_field(
            array(
                'parent'        => $panel_header,
                'type'          => 'select',
                'name'          => 'header_style',
                'default_value' => '',
                'label'         => esc_html__('Header Skin', 'malmo'),
                'description'   => esc_html__('Choose a header style to make header elements (logo, main menu, side menu button) in that predefined style', 'malmo'),
                'options'       => array(
                    ''             => '',
                    'light-header' => esc_html__('Light', 'malmo'),
                    'dark-header'  => esc_html__('Dark', 'malmo')
                )
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $panel_header,
                'type'          => 'yesno',
                'name'          => 'enable_header_style_on_scroll',
                'default_value' => 'no',
                'label'         => esc_html__('Enable Header Style on Scroll', 'malmo'),
                'description'   => esc_html__('Enabling this option, header will change style depending on row settings for dark/light style', 'malmo'),
            )
        );

        $panel_header_standard = malmo_elated_add_admin_panel(
            array(
                'page'            => '_header_page',
                'name'            => 'panel_header_standard',
                'title'           => esc_html__('Header Standard', 'malmo'),
                'hidden_property' => 'header_type',
                'hidden_value'    => '',
                'hidden_values'   => array(
                    'header-minimal',
                    'header-centered',
                    'header-vertical'
                )
            )
        );

        malmo_elated_add_admin_section_title(
            array(
                'parent' => $panel_header_standard,
                'name'   => 'menu_area_title',
                'title'  => esc_html__('Menu Area', 'malmo')
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $panel_header_standard,
                'type'          => 'yesno',
                'name'          => 'menu_area_in_grid_header_standard',
                'default_value' => 'yes',
                'label'         => esc_html__('Header in grid', 'malmo'),
                'description'   => esc_html__('Set header content to be in grid', 'malmo'),
                'args'          => array(
                    'dependence'             => true,
                    'dependence_hide_on_yes' => '',
                    'dependence_show_on_yes' => '#eltd_menu_area_in_grid_header_standard_container'
                )
            )
        );

        $menu_area_in_grid_header_standard_container = malmo_elated_add_admin_container(
            array(
                'parent'          => $panel_header_standard,
                'name'            => 'menu_area_in_grid_header_standard_container',
                'hidden_property' => 'menu_area_in_grid_header_standard',
                'hidden_value'    => 'no'
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $menu_area_in_grid_header_standard_container,
                'type'          => 'color',
                'name'          => 'menu_area_grid_background_color_header_standard',
                'default_value' => '',
                'label'         => esc_html__('Grid Background color', 'malmo'),
                'description'   => esc_html__('Set grid background color for header area', 'malmo'),
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $menu_area_in_grid_header_standard_container,
                'type'          => 'text',
                'name'          => 'menu_area_grid_background_transparency_header_standard',
                'default_value' => '',
                'label'         => esc_html__('Grid background transparency', 'malmo'),
                'description'   => esc_html__('Set grid background transparency for header', 'malmo'),
                'args'          => array(
                    'col_width' => 3
                )
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $panel_header_standard,
                'type'          => 'color',
                'name'          => 'menu_area_background_color_header_standard',
                'default_value' => '',
                'label'         => esc_html__('Background color', 'malmo'),
                'description'   => esc_html__('Set background color for header', 'malmo')
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $panel_header_standard,
                'type'          => 'text',
                'name'          => 'menu_area_background_transparency_header_standard',
                'default_value' => '',
                'label'         => esc_html__('Background transparency', 'malmo'),
                'description'   => esc_html__('Set background transparency for header', 'malmo'),
                'args'          => array(
                    'col_width' => 3
                )
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $panel_header_standard,
                'type'          => 'text',
                'name'          => 'menu_area_height_header_standard',
                'default_value' => '',
                'label'         => esc_html__('Height', 'malmo'),
                'description'   => esc_html__('Enter header height (default is 60px)', 'malmo'),
                'args'          => array(
                    'col_width' => 3,
                    'suffix'    => 'px'
                )
            )
        );

        /***************** Minimal Header Layout start *******************/

        $panel_header_minimal = malmo_elated_add_admin_panel(
            array(
                'page'            => '_header_page',
                'name'            => 'panel_header_minimal',
                'title'           => esc_html__('Header Minimal', 'malmo'),
                'hidden_property' => 'header_type',
                'hidden_value'    => '',
                'hidden_values'   => array(
                    'header-vertical',
                    'header-standard',
                    'header-centered'
                )
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $panel_header_minimal,
                'type'          => 'yesno',
                'name'          => 'menu_area_in_grid_header_minimal',
                'default_value' => 'no',
                'label'         => esc_html__('Header In Grid', 'malmo'),
                'description'   => esc_html__('Set header content to be in grid', 'malmo'),
                'args'          => array(
                    'dependence'             => true,
                    'dependence_hide_on_yes' => '',
                    'dependence_show_on_yes' => '#eltd_menu_area_in_grid_header_minimal_container'
                )
            )
        );

        $menu_area_in_grid_header_minimal_container = malmo_elated_add_admin_container(
            array(
                'parent'          => $panel_header_minimal,
                'name'            => 'menu_area_in_grid_header_minimal_container',
                'hidden_property' => 'menu_area_in_grid_header_minimal',
                'hidden_value'    => 'no'
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $menu_area_in_grid_header_minimal_container,
                'type'          => 'color',
                'name'          => 'menu_area_grid_background_color_header_minimal',
                'default_value' => '',
                'label'         => esc_html__('Grid Background Color', 'malmo'),
                'description'   => esc_html__('Set grid background color for header area', 'malmo'),
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $menu_area_in_grid_header_minimal_container,
                'type'          => 'text',
                'name'          => 'menu_area_grid_background_transparency_header_minimal',
                'default_value' => '',
                'label'         => esc_html__('Grid Background Transparency', 'malmo'),
                'description'   => esc_html__('Set grid background transparency for header', 'malmo'),
                'args'          => array(
                    'col_width' => 3
                )
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $panel_header_minimal,
                'type'          => 'color',
                'name'          => 'menu_area_background_color_header_minimal',
                'default_value' => '',
                'label'         => esc_html__('Background color', 'malmo'),
                'description'   => esc_html__('Set background color for header', 'malmo')
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $panel_header_minimal,
                'type'          => 'text',
                'name'          => 'menu_area_background_transparency_header_minimal',
                'default_value' => '',
                'label'         => esc_html__('Background transparency', 'malmo'),
                'description'   => esc_html__('Set background transparency for header', 'malmo'),
                'args'          => array(
                    'col_width' => 3
                )
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $panel_header_minimal,
                'type'          => 'text',
                'name'          => 'menu_area_height_header_minimal',
                'default_value' => '',
                'label'         => esc_html__('Height', 'malmo'),
                'description'   => esc_html__('Enter header height (default is 100px)', 'malmo'),
                'args'          => array(
                    'col_width' => 3,
                    'suffix'    => 'px'
                )
            )
        );

        /***************** Minimal Header Layout - end ****************/

        /***************** Centered Header Layout - start ****************/

        $panel_header_centered = malmo_elated_add_admin_panel(
            array(
                'page'            => '_header_page',
                'name'            => 'panel_header_centered',
                'title'           => esc_html__('Header Centered', 'malmo'),
                'hidden_property' => 'header_type',
                'hidden_value'    => '',
                'hidden_values'   => array(
                    'header-vertical',
                    'header-standard',
                    'header-minimal'
                )
            )
        );

        malmo_elated_add_admin_section_title(
            array(
                'parent' => $panel_header_centered,
                'name'   => 'logo_menu_area_title',
                'title'  => esc_html__('Logo Area', 'malmo')
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $panel_header_centered,
                'type'          => 'yesno',
                'name'          => 'logo_area_in_grid_header_centered',
                'default_value' => 'no',
                'label'         => esc_html__('Logo Area In Grid', 'malmo'),
                'description'   => esc_html__('Set menu area content to be in grid', 'malmo'),
                'args'          => array(
                    'dependence'             => true,
                    'dependence_hide_on_yes' => '',
                    'dependence_show_on_yes' => '#eltd_logo_area_in_grid_header_centered_container'
                )
            )
        );

        $logo_area_in_grid_header_centered_container = malmo_elated_add_admin_container(
            array(
                'parent'          => $panel_header_centered,
                'name'            => 'logo_area_in_grid_header_centered_container',
                'hidden_property' => 'logo_area_in_grid_header_centered',
                'hidden_value'    => 'no'
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $logo_area_in_grid_header_centered_container,
                'type'          => 'color',
                'name'          => 'logo_area_grid_background_color_header_centered',
                'default_value' => '',
                'label'         => esc_html__('Grid Background Color', 'malmo'),
                'description'   => esc_html__('Set grid background color for logo area', 'malmo'),
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $logo_area_in_grid_header_centered_container,
                'type'          => 'text',
                'name'          => 'logo_area_grid_background_transparency_header_centered',
                'default_value' => '',
                'label'         => esc_html__('Grid Background Transparency', 'malmo'),
                'description'   => esc_html__('Set grid background transparency', 'malmo'),
                'args'          => array(
                    'col_width' => 3
                )
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $logo_area_in_grid_header_centered_container,
                'type'          => 'yesno',
                'name'          => 'logo_area_in_grid_border_header_centered',
                'default_value' => 'no',
                'label'         => esc_html__('Grid Area Border', 'malmo'),
                'description'   => esc_html__('Set border on grid area', 'malmo'),
                'args'          => array(
                    'dependence'             => true,
                    'dependence_hide_on_yes' => '',
                    'dependence_show_on_yes' => '#eltd_logo_area_in_grid_border_header_centered_container'
                )
            )
        );

        $logo_area_in_grid_border_header_centered_container = malmo_elated_add_admin_container(
            array(
                'parent'          => $logo_area_in_grid_header_centered_container,
                'name'            => 'logo_area_in_grid_border_header_centered_container',
                'hidden_property' => 'logo_area_in_grid_border_header_centered',
                'hidden_value'    => 'no'
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $logo_area_in_grid_border_header_centered_container,
                'type'          => 'color',
                'name'          => 'logo_area_in_grid_border_color_header_centered',
                'default_value' => '',
                'label'         => esc_html__('Border Color', 'malmo'),
                'description'   => esc_html__('Set border color for grid area', 'malmo'),
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $panel_header_centered,
                'type'          => 'color',
                'name'          => 'logo_area_background_color_header_centered',
                'default_value' => '',
                'label'         => esc_html__('Background color', 'malmo'),
                'description'   => esc_html__('Set background color for logo area', 'malmo')
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $panel_header_centered,
                'type'          => 'text',
                'name'          => 'logo_area_background_transparency_header_centered',
                'default_value' => '',
                'label'         => esc_html__('Background transparency', 'malmo'),
                'description'   => esc_html__('Set background transparency for logo area', 'malmo'),
                'args'          => array(
                    'col_width' => 3
                )
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $panel_header_centered,
                'type'          => 'text',
                'name'          => 'logo_wrapper_padding_header_centered',
                'default_value' => '',
                'label'         => esc_html__('Logo Padding', 'malmo'),
                'description'   => esc_html__('Insert padding in format: 0px 0px 1px 0px', 'malmo'),
                'args'          => array(
                    'col_width' => 3
                )
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $panel_header_centered,
                'type'          => 'text',
                'name'          => 'logo_area_height_header_centered',
                'default_value' => '',
                'label'         => esc_html__('Height', 'malmo'),
                'description'   => esc_html__('Enter logo area height (default is 155px)', 'malmo'),
                'args'          => array(
                    'col_width' => 3,
                    'suffix'    => 'px'
                )
            )
        );


        malmo_elated_add_admin_section_title(
            array(
                'parent' => $panel_header_centered,
                'name'   => 'main_menu_area_title',
                'title'  => esc_html__('Menu Area', 'malmo'),
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $panel_header_centered,
                'type'          => 'yesno',
                'name'          => 'menu_area_in_grid_header_centered',
                'default_value' => 'no',
                'label'         => esc_html__('Menu Area In Grid', 'malmo'),
                'description'   => esc_html__('Set menu area content to be in grid', 'malmo'),
                'args'          => array(
                    'dependence'             => true,
                    'dependence_hide_on_yes' => '',
                    'dependence_show_on_yes' => '#eltd_menu_area_in_grid_header_centered_container'
                )
            )
        );

        $menu_area_in_grid_header_centered_container = malmo_elated_add_admin_container(
            array(
                'parent'          => $panel_header_centered,
                'name'            => 'menu_area_in_grid_header_centered_container',
                'hidden_property' => 'menu_area_in_grid_header_centered',
                'hidden_value'    => 'no'
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $menu_area_in_grid_header_centered_container,
                'type'          => 'color',
                'name'          => 'menu_area_grid_background_color_header_centered',
                'default_value' => '',
                'label'         => esc_html__('Grid Background Color', 'malmo'),
                'description'   => esc_html__('Set grid background color for menu area', 'malmo'),
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $menu_area_in_grid_header_centered_container,
                'type'          => 'text',
                'name'          => 'menu_area_grid_background_transparency_header_centered',
                'default_value' => '',
                'label'         => esc_html__('Grid Background Transparency', 'malmo'),
                'description'   => esc_html__('Set grid background transparency', 'malmo'),
                'args'          => array(
                    'col_width' => 3
                )
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $menu_area_in_grid_header_centered_container,
                'type'          => 'yesno',
                'name'          => 'menu_area_in_grid_shadow_header_centered',
                'default_value' => 'no',
                'label'         => esc_html__('Grid Area Shadow', 'malmo'),
                'description'   => esc_html__('Set shadow on grid area', 'malmo')
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $panel_header_centered,
                'type'          => 'color',
                'name'          => 'menu_area_background_color_header_centered',
                'default_value' => '',
                'label'         => esc_html__('Background color', 'malmo'),
                'description'   => esc_html__('Set background color for menu area', 'malmo')
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $panel_header_centered,
                'type'          => 'text',
                'name'          => 'menu_area_background_transparency_header_centered',
                'default_value' => '',
                'label'         => esc_html__('Background transparency', 'malmo'),
                'description'   => esc_html__('Set background transparency for menu area', 'malmo'),
                'args'          => array(
                    'col_width' => 3
                )
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $panel_header_centered,
                'type'          => 'text',
                'name'          => 'menu_area_height_header_centered',
                'default_value' => '',
                'label'         => esc_html__('Height', 'malmo'),
                'description'   => esc_html__('Enter menu area height (default is 100px)', 'malmo'),
                'args'          => array(
                    'col_width' => 3,
                    'suffix'    => 'px'
                )
            )
        );

        /***************** Centered Header Layout - end ****************/

        /***************** Vertical Header Layout - start ****************/
        $panel_header_vertical = malmo_elated_add_admin_panel(
            array(
                'page'            => '_header_page',
                'name'            => 'panel_header_vertical',
                'title'           => esc_html__('Header Vertical', 'malmo'),
                'hidden_property' => 'header_type',
                'hidden_value'    => '',
                'hidden_values'   => array(
                    'header-standard',
                    'header-minimal',
                    'header-centered',
                )
            )
        );

        malmo_elated_add_admin_field(array(
            'name'        => 'vertical_header_background_color',
            'type'        => 'color',
            'label'       => esc_html__('Background Color', 'malmo'),
            'description' => esc_html__('Set background color for vertical menu', 'malmo'),
            'parent'      => $panel_header_vertical
        ));

        malmo_elated_add_admin_field(array(
            'name'        => 'vertical_header_transparency',
            'type'        => 'text',
            'label'       => esc_html__('Transparency', 'malmo'),
            'description' => esc_html__('Enter transparency for vertical menu (value from 0 to 1)', 'malmo'),
            'parent'      => $panel_header_vertical,
            'args'        => array(
                'col_width' => 1
            )
        ));

        malmo_elated_add_admin_field(
            array(
                'name'          => 'vertical_header_background_image',
                'type'          => 'image',
                'default_value' => '',
                'label'         => esc_html__('Background Image', 'malmo'),
                'description'   => esc_html__('Set background image for vertical menu', 'malmo'),
                'parent'        => $panel_header_vertical
            )
        );

        $panel_sticky_header = malmo_elated_add_admin_panel(
            array(
                'title'           => esc_html__('Sticky Header', 'malmo'),
                'name'            => 'panel_sticky_header',
                'page'            => '_header_page',
                'hidden_property' => 'header_behaviour',
                'hidden_values'   => array(
                    'fixed-on-scroll',
                    'no-behavior'
                )
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'        => 'scroll_amount_for_sticky',
                'type'        => 'text',
                'label'       => esc_html__('Scroll Amount for Sticky', 'malmo'),
                'description' => esc_html__('Enter scroll amount for Sticky Menu to appear (deafult is header height)', 'malmo'),
                'parent'      => $panel_sticky_header,
                'args'        => array(
                    'col_width' => 2,
                    'suffix'    => 'px'
                )
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'          => 'sticky_header_in_grid',
                'type'          => 'yesno',
                'default_value' => 'yes',
                'label'         => esc_html__('Sticky Header in grid', 'malmo'),
                'description'   => esc_html__('Set sticky header content to be in grid', 'malmo'),
                'parent'        => $panel_sticky_header,
                'args'          => array(
                    "dependence"             => true,
                    "dependence_hide_on_yes" => "",
                    "dependence_show_on_yes" => "#eltd_sticky_header_in_grid_container"
                )
            )
        );

        $sticky_header_in_grid_container = malmo_elated_add_admin_container(array(
            'name'            => 'sticky_header_in_grid_container',
            'parent'          => $panel_sticky_header,
            'hidden_property' => 'sticky_header_in_grid',
            'hidden_value'    => 'no'
        ));

        malmo_elated_add_admin_field(array(
            'name'        => 'sticky_header_grid_background_color',
            'type'        => 'color',
            'label'       => esc_html__('Grid Background Color', 'malmo'),
            'description' => esc_html__('Set grid background color for sticky header', 'malmo'),
            'parent'      => $sticky_header_in_grid_container
        ));

        malmo_elated_add_admin_field(array(
            'name'        => 'sticky_header_grid_transparency',
            'type'        => 'text',
            'label'       => esc_html__('Sticky Header Grid Transparency', 'malmo'),
            'description' => esc_html__('Enter transparency for sticky header grid (value from 0 to 1)', 'malmo'),
            'parent'      => $sticky_header_in_grid_container,
            'args'        => array(
                'col_width' => 1
            )
        ));

        malmo_elated_add_admin_field(array(
            'name'        => 'sticky_header_background_color',
            'type'        => 'color',
            'label'       => esc_html__('Background Color', 'malmo'),
            'description' => esc_html__('Set background color for sticky header', 'malmo'),
            'parent'      => $panel_sticky_header
        ));

        malmo_elated_add_admin_field(array(
            'name'        => 'sticky_header_transparency',
            'type'        => 'text',
            'label'       => esc_html__('Sticky Header Transparency', 'malmo'),
            'description' => esc_html__('Enter transparency for sticky header (value from 0 to 1)', 'malmo'),
            'parent'      => $panel_sticky_header,
            'args'        => array(
                'col_width' => 1
            )
        ));

        malmo_elated_add_admin_field(array(
            'name'        => 'sticky_header_height',
            'type'        => 'text',
            'label'       => esc_html__('Sticky Header Height', 'malmo'),
            'description' => esc_html__('Enter height for sticky header (default is 60px)', 'malmo'),
            'parent'      => $panel_sticky_header,
            'args'        => array(
                'col_width' => 2,
                'suffix'    => 'px'
            )
        ));

        $group_sticky_header_menu = malmo_elated_add_admin_group(array(
            'title'       => esc_html__('Sticky Header Menu', 'malmo'),
            'name'        => 'group_sticky_header_menu',
            'parent'      => $panel_sticky_header,
            'description' => 'Define styles for sticky menu items',
        ));

        $row1_sticky_header_menu = malmo_elated_add_admin_row(array(
            'name'   => 'row1',
            'parent' => $group_sticky_header_menu
        ));

        malmo_elated_add_admin_field(array(
            'name'        => 'sticky_color',
            'type'        => 'colorsimple',
            'label'       => esc_html__('Text Color', 'malmo'),
            'description' => '',
            'parent'      => $row1_sticky_header_menu
        ));

        malmo_elated_add_admin_field(array(
            'name'        => 'sticky_hovercolor',
            'type'        => 'colorsimple',
            'label'       => esc_html__('Hover/Active color', 'malmo'),
            'description' => '',
            'parent'      => $row1_sticky_header_menu
        ));

        $row2_sticky_header_menu = malmo_elated_add_admin_row(array(
            'name'   => 'row2',
            'parent' => $group_sticky_header_menu
        ));

        malmo_elated_add_admin_field(
            array(
                'name'          => 'sticky_google_fonts',
                'type'          => 'fontsimple',
                'label'         => esc_html__('Font Family', 'malmo'),
                'default_value' => '-1',
                'parent'        => $row2_sticky_header_menu,
            )
        );

        malmo_elated_add_admin_field(
            array(
                'type'          => 'textsimple',
                'name'          => 'sticky_fontsize',
                'label'         => esc_html__('Font Size', 'malmo'),
                'default_value' => '',
                'parent'        => $row2_sticky_header_menu,
                'args'          => array(
                    'suffix' => 'px'
                )
            )
        );

        malmo_elated_add_admin_field(
            array(
                'type'          => 'textsimple',
                'name'          => 'sticky_lineheight',
                'label'         => esc_html__('Line height', 'malmo'),
                'default_value' => '',
                'parent'        => $row2_sticky_header_menu,
                'args'          => array(
                    'suffix' => 'px'
                )
            )
        );

        malmo_elated_add_admin_field(
            array(
                'type'          => 'selectblanksimple',
                'name'          => 'sticky_texttransform',
                'label'         => esc_html__('Text transform', 'malmo'),
                'default_value' => '',
                'options'       => malmo_elated_get_text_transform_array(),
                'parent'        => $row2_sticky_header_menu
            )
        );

        $row3_sticky_header_menu = malmo_elated_add_admin_row(array(
            'name'   => 'row3',
            'parent' => $group_sticky_header_menu
        ));

        malmo_elated_add_admin_field(
            array(
                'type'          => 'selectblanksimple',
                'name'          => 'sticky_fontstyle',
                'default_value' => '',
                'label'         => esc_html__('Font Style', 'malmo'),
                'options'       => malmo_elated_get_font_style_array(),
                'parent'        => $row3_sticky_header_menu
            )
        );

        malmo_elated_add_admin_field(
            array(
                'type'          => 'selectblanksimple',
                'name'          => 'sticky_fontweight',
                'default_value' => '',
                'label'         => esc_html__('Font Weight', 'malmo'),
                'options'       => malmo_elated_get_font_weight_array(),
                'parent'        => $row3_sticky_header_menu
            )
        );

        malmo_elated_add_admin_field(
            array(
                'type'          => 'textsimple',
                'name'          => 'sticky_letterspacing',
                'label'         => esc_html__('Letter Spacing', 'malmo'),
                'default_value' => '',
                'parent'        => $row3_sticky_header_menu,
                'args'          => array(
                    'suffix' => 'px'
                )
            )
        );

        $panel_fixed_header = malmo_elated_add_admin_panel(
            array(
                'title'           => esc_html__('Fixed Header', 'malmo'),
                'name'            => 'panel_fixed_header',
                'page'            => '_header_page',
                'hidden_property' => 'header_behaviour',
                'hidden_values'   => array(
                    'sticky-header-on-scroll-up',
                    'sticky-header-on-scroll-down-up',
                    'no-behavior'
                )
            )
        );

        malmo_elated_add_admin_field(array(
            'name'          => 'fixed_header_grid_background_color',
            'type'          => 'color',
            'default_value' => '',
            'label'         => esc_html__('Grid Background Color', 'malmo'),
            'description'   => esc_html__('Set grid background color for fixed header', 'malmo'),
            'parent'        => $panel_fixed_header
        ));

        malmo_elated_add_admin_field(array(
            'name'          => 'fixed_header_grid_transparency',
            'type'          => 'text',
            'default_value' => '',
            'label'         => esc_html__('Header Transparency Grid', 'malmo'),
            'description'   => esc_html__('Enter transparency for fixed header grid (value from 0 to 1)', 'malmo'),
            'parent'        => $panel_fixed_header,
            'args'          => array(
                'col_width' => 1
            )
        ));

        malmo_elated_add_admin_field(array(
            'name'          => 'fixed_header_background_color',
            'type'          => 'color',
            'default_value' => '',
            'label'         => esc_html__('Background Color', 'malmo'),
            'description'   => esc_html__('Set background color for fixed header', 'malmo'),
            'parent'        => $panel_fixed_header
        ));

        malmo_elated_add_admin_field(array(
            'name'        => 'fixed_header_transparency',
            'type'        => 'text',
            'label'       => esc_html__('Header Transparency', 'malmo'),
            'description' => esc_html__('Enter transparency for fixed header (value from 0 to 1)', 'malmo'),
            'parent'      => $panel_fixed_header,
            'args'        => array(
                'col_width' => 1
            )
        ));


        $panel_main_menu = malmo_elated_add_admin_panel(
            array(
                'title'           => esc_html__('Main Menu', 'malmo'),
                'name'            => 'panel_main_menu',
                'page'            => '_header_page',
                'hidden_property' => 'header_type',
                'hidden_values'   => array('header-vertical', 'header-minimal')
            )
        );

        malmo_elated_add_admin_section_title(
            array(
                'parent' => $panel_main_menu,
                'name'   => 'main_menu_area_title',
                'title'  => esc_html__('Main Menu General Settings', 'malmo')
            )
        );

        $drop_down_group = malmo_elated_add_admin_group(
            array(
                'parent'      => $panel_main_menu,
                'name'        => 'drop_down_group',
                'title'       => esc_html__('Main Dropdown Menu', 'malmo'),
                'description' => esc_html__('Choose a color and transparency for the main menu background (0 = fully transparent, 1 = opaque)', 'malmo')
            )
        );

        $drop_down_row1 = malmo_elated_add_admin_row(
            array(
                'parent' => $drop_down_group,
                'name'   => 'drop_down_row1',
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $drop_down_row1,
                'type'          => 'colorsimple',
                'name'          => 'dropdown_background_color',
                'default_value' => '',
                'label'         => esc_html__('Background Color', 'malmo'),
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $drop_down_row1,
                'type'          => 'textsimple',
                'name'          => 'dropdown_background_transparency',
                'default_value' => '',
                'label'         => esc_html__('Transparency', 'malmo'),
            )
        );

        $drop_down_padding_group = malmo_elated_add_admin_group(
            array(
                'parent'      => $panel_main_menu,
                'name'        => 'drop_down_padding_group',
                'title'       => esc_html__('Main Dropdown Menu Padding', 'malmo'),
                'description' => esc_html__('Choose a top/bottom padding for dropdown menu', 'malmo')
            )
        );

        $drop_down_padding_row = malmo_elated_add_admin_row(
            array(
                'parent' => $drop_down_padding_group,
                'name'   => 'drop_down_padding_row',
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $drop_down_padding_row,
                'type'          => 'textsimple',
                'name'          => 'dropdown_top_padding',
                'default_value' => '',
                'label'         => esc_html__('Top Padding', 'malmo'),
                'args'          => array(
                    'suffix' => 'px'
                )
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $drop_down_padding_row,
                'type'          => 'textsimple',
                'name'          => 'dropdown_bottom_padding',
                'default_value' => '',
                'label'         => esc_html__('Bottom Padding', 'malmo'),
                'args'          => array(
                    'suffix' => 'px'
                )
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $panel_main_menu,
                'type'          => 'select',
                'name'          => 'menu_dropdown_appearance',
                'default_value' => 'default',
                'label'         => esc_html__('Main Dropdown Menu Appearance', 'malmo'),
                'description'   => esc_html__('Choose appearance for dropdown menu', 'malmo'),
                'options'       => array(
                    'dropdown-default'           => esc_html__('Default', 'malmo'),
                    'dropdown-slide-from-bottom' => esc_html__('Slide From Bottom', 'malmo'),
                    'dropdown-slide-from-top'    => esc_html__('Slide From Top', 'malmo'),
                    'dropdown-animate-height'    => esc_html__('Animate Height', 'malmo'),
                    'dropdown-slide-from-left'   => esc_html__('Slide From Left', 'malmo')
                )
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $panel_main_menu,
                'type'          => 'text',
                'name'          => 'dropdown_top_position',
                'default_value' => '',
                'label'         => esc_html__('Dropdown position', 'malmo'),
                'description'   => esc_html__('Enter value in percentage of entire header height', 'malmo'),
                'args'          => array(
                    'col_width' => 3,
                    'suffix'    => '%'
                )
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $panel_main_menu,
                'type'          => 'yesno',
                'name'          => 'enable_wide_menu_background',
                'default_value' => 'no',
                'label'         => esc_html__('Enable Full Width Background for Wide Dropdown Type', 'malmo'),
                'description'   => esc_html__('Enabling this option will show full width background  for wide dropdown type', 'malmo'),
            )
        );

        $first_level_group = malmo_elated_add_admin_group(
            array(
                'parent'      => $panel_main_menu,
                'name'        => 'first_level_group',
                'title'       => esc_html__('1st Level Menu', 'malmo'),
                'description' => esc_html__('Define styles for 1st level in Top Navigation Menu', 'malmo')
            )
        );

        $first_level_row1 = malmo_elated_add_admin_row(
            array(
                'parent' => $first_level_group,
                'name'   => 'first_level_row1'
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $first_level_row1,
                'type'          => 'colorsimple',
                'name'          => 'menu_color',
                'default_value' => '',
                'label'         => esc_html__('Text Color', 'malmo'),
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $first_level_row1,
                'type'          => 'colorsimple',
                'name'          => 'menu_hovercolor',
                'default_value' => '',
                'label'         => esc_html__('Hover Text Color', 'malmo'),
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $first_level_row1,
                'type'          => 'colorsimple',
                'name'          => 'menu_activecolor',
                'default_value' => '',
                'label'         => esc_html__('Active Text Color', 'malmo'),
            )
        );

        $first_level_row2 = malmo_elated_add_admin_row(
            array(
                'parent' => $first_level_group,
                'name'   => 'first_level_row2',
                'next'   => true
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $first_level_row2,
                'type'          => 'colorsimple',
                'name'          => 'menu_text_background_color',
                'default_value' => '',
                'label'         => esc_html__('Text Background Color', 'malmo'),
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $first_level_row2,
                'type'          => 'colorsimple',
                'name'          => 'menu_hover_background_color',
                'default_value' => '',
                'label'         => esc_html__('Hover Text Background Color', 'malmo'),
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $first_level_row2,
                'type'          => 'colorsimple',
                'name'          => 'menu_active_background_color',
                'default_value' => '',
                'label'         => esc_html__('Active Text Background Color', 'malmo'),
            )
        );

        $first_level_row3 = malmo_elated_add_admin_row(
            array(
                'parent' => $first_level_group,
                'name'   => 'first_level_row3',
                'next'   => true
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $first_level_row3,
                'type'          => 'colorsimple',
                'name'          => 'menu_light_hovercolor',
                'default_value' => '',
                'label'         => esc_html__('Light Menu Hover Text Color', 'malmo'),
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $first_level_row3,
                'type'          => 'colorsimple',
                'name'          => 'menu_light_activecolor',
                'default_value' => '',
                'label'         => esc_html__('Light Menu Active Text Color', 'malmo'),
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $first_level_row3,
                'type'          => 'colorsimple',
                'name'          => 'menu_light_border_color',
                'default_value' => '',
                'label'         => esc_html__('Light Menu Border Hover/Active Color', 'malmo'),
            )
        );

        $first_level_row4 = malmo_elated_add_admin_row(
            array(
                'parent' => $first_level_group,
                'name'   => 'first_level_row4',
                'next'   => true
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $first_level_row4,
                'type'          => 'colorsimple',
                'name'          => 'menu_dark_hovercolor',
                'default_value' => '',
                'label'         => esc_html__('Dark Menu Hover Text Color', 'malmo'),
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $first_level_row4,
                'type'          => 'colorsimple',
                'name'          => 'menu_dark_activecolor',
                'default_value' => '',
                'label'         => esc_html__('Dark Menu Active Text Color', 'malmo'),
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $first_level_row4,
                'type'          => 'colorsimple',
                'name'          => 'menu_dark_border_color',
                'default_value' => '',
                'label'         => esc_html__('Dark Menu Border Hover/Active Color', 'malmo'),
            )
        );

        $first_level_row5 = malmo_elated_add_admin_row(
            array(
                'parent' => $first_level_group,
                'name'   => 'first_level_row5',
                'next'   => true
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $first_level_row5,
                'type'          => 'fontsimple',
                'name'          => 'menu_google_fonts',
                'default_value' => '-1',
                'label'         => esc_html__('Font Family', 'malmo'),
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $first_level_row5,
                'type'          => 'textsimple',
                'name'          => 'menu_fontsize',
                'default_value' => '',
                'label'         => esc_html__('Font Size', 'malmo'),
                'args'          => array(
                    'suffix' => 'px'
                )
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $first_level_row5,
                'type'          => 'textsimple',
                'name'          => 'menu_hover_background_color_transparency',
                'default_value' => '',
                'label'         => esc_html__('Hover Background Color Transparency', 'malmo'),
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $first_level_row5,
                'type'          => 'textsimple',
                'name'          => 'menu_active_background_color_transparency',
                'default_value' => '',
                'label'         => esc_html__('Active Background Color Transparency', 'malmo'),
            )
        );

        $first_level_row6 = malmo_elated_add_admin_row(
            array(
                'parent' => $first_level_group,
                'name'   => 'first_level_row6',
                'next'   => true
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $first_level_row6,
                'type'          => 'selectblanksimple',
                'name'          => 'menu_fontstyle',
                'default_value' => '',
                'label'         => esc_html__('Font Style', 'malmo'),
                'options'       => malmo_elated_get_font_style_array()
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $first_level_row6,
                'type'          => 'selectblanksimple',
                'name'          => 'menu_fontweight',
                'default_value' => '',
                'label'         => esc_html__('Font Weight', 'malmo'),
                'options'       => malmo_elated_get_font_weight_array()
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $first_level_row6,
                'type'          => 'textsimple',
                'name'          => 'menu_letterspacing',
                'default_value' => '',
                'label'         => esc_html__('Letter Spacing', 'malmo'),
                'args'          => array(
                    'suffix' => 'px'
                )
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $first_level_row6,
                'type'          => 'selectblanksimple',
                'name'          => 'menu_texttransform',
                'default_value' => '',
                'label'         => esc_html__('Text Transform', 'malmo'),
                'options'       => malmo_elated_get_text_transform_array()
            )
        );

        $first_level_row7 = malmo_elated_add_admin_row(
            array(
                'parent' => $first_level_group,
                'name'   => 'first_level_row7',
                'next'   => true
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $first_level_row7,
                'type'          => 'textsimple',
                'name'          => 'menu_lineheight',
                'default_value' => '',
                'label'         => esc_html__('Line Height', 'malmo'),
                'args'          => array(
                    'suffix' => 'px'
                )
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $first_level_row7,
                'type'          => 'textsimple',
                'name'          => 'menu_padding_left_right',
                'default_value' => '',
                'label'         => esc_html__('Padding Left/Right', 'malmo'),
                'args'          => array(
                    'suffix' => 'px'
                )
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $first_level_row7,
                'type'          => 'textsimple',
                'name'          => 'menu_margin_left_right',
                'default_value' => '',
                'label'         => esc_html__('Margin Left/Right', 'malmo'),
                'args'          => array(
                    'suffix' => 'px'
                )
            )
        );

        $second_level_group = malmo_elated_add_admin_group(
            array(
                'parent'      => $panel_main_menu,
                'name'        => 'second_level_group',
                'title'       => esc_html__('2nd Level Menu', 'malmo'),
                'description' => esc_html__('Define styles for 2nd level in Top Navigation Menu', 'malmo')
            )
        );

        $second_level_row1 = malmo_elated_add_admin_row(
            array(
                'parent' => $second_level_group,
                'name'   => 'second_level_row1'
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $second_level_row1,
                'type'          => 'colorsimple',
                'name'          => 'dropdown_color',
                'default_value' => '',
                'label'         => esc_html__('Text Color', 'malmo')
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $second_level_row1,
                'type'          => 'colorsimple',
                'name'          => 'dropdown_hovercolor',
                'default_value' => '',
                'label'         => esc_html__('Hover/Active Color', 'malmo')
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $second_level_row1,
                'type'          => 'colorsimple',
                'name'          => 'dropdown_background_hovercolor',
                'default_value' => '',
                'label'         => esc_html__('Hover/Active Background Color', 'malmo')
            )
        );

        $second_level_row2 = malmo_elated_add_admin_row(
            array(
                'parent' => $second_level_group,
                'name'   => 'second_level_row2',
                'next'   => true
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $second_level_row2,
                'type'          => 'fontsimple',
                'name'          => 'dropdown_google_fonts',
                'default_value' => '-1',
                'label'         => esc_html__('Font Family', 'malmo')
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $second_level_row2,
                'type'          => 'textsimple',
                'name'          => 'dropdown_fontsize',
                'default_value' => '',
                'label'         => esc_html__('Font Size', 'malmo'),
                'args'          => array(
                    'suffix' => 'px'
                )
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $second_level_row2,
                'type'          => 'textsimple',
                'name'          => 'dropdown_lineheight',
                'default_value' => '',
                'label'         => esc_html__('Line Height', 'malmo'),
                'args'          => array(
                    'suffix' => 'px'
                )
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $second_level_row2,
                'type'          => 'textsimple',
                'name'          => 'dropdown_padding_top_bottom',
                'default_value' => '',
                'label'         => esc_html__('Padding Top/Bottom', 'malmo'),
                'args'          => array(
                    'suffix' => 'px'
                )
            )
        );

        $second_level_row3 = malmo_elated_add_admin_row(
            array(
                'parent' => $second_level_group,
                'name'   => 'second_level_row3',
                'next'   => true
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $second_level_row3,
                'type'          => 'selectblanksimple',
                'name'          => 'dropdown_fontstyle',
                'default_value' => '',
                'label'         => 'Font style',
                'options'       => malmo_elated_get_font_style_array()
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $second_level_row3,
                'type'          => 'selectblanksimple',
                'name'          => 'dropdown_fontweight',
                'default_value' => '',
                'label'         => esc_html__('Font weight', 'malmo'),
                'options'       => malmo_elated_get_font_weight_array()
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $second_level_row3,
                'type'          => 'textsimple',
                'name'          => 'dropdown_letterspacing',
                'default_value' => '',
                'label'         => esc_html__('Letter spacing', 'malmo'),
                'args'          => array(
                    'suffix' => 'px'
                )
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $second_level_row3,
                'type'          => 'selectblanksimple',
                'name'          => 'dropdown_texttransform',
                'default_value' => '',
                'label'         => esc_html__('Text Transform', 'malmo'),
                'options'       => malmo_elated_get_text_transform_array()
            )
        );

        $second_level_wide_group = malmo_elated_add_admin_group(
            array(
                'parent'      => $panel_main_menu,
                'name'        => 'second_level_wide_group',
                'title'       => esc_html__('2nd Level Wide Menu', 'malmo'),
                'description' => esc_html__('Define styles for 2nd level in Wide Menu', 'malmo')
            )
        );

        $second_level_wide_row1 = malmo_elated_add_admin_row(
            array(
                'parent' => $second_level_wide_group,
                'name'   => 'second_level_wide_row1'
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $second_level_wide_row1,
                'type'          => 'colorsimple',
                'name'          => 'dropdown_wide_color',
                'default_value' => '',
                'label'         => esc_html__('Text Color', 'malmo')
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $second_level_wide_row1,
                'type'          => 'colorsimple',
                'name'          => 'dropdown_wide_hovercolor',
                'default_value' => '',
                'label'         => esc_html__('Hover/Active Color', 'malmo')
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $second_level_wide_row1,
                'type'          => 'colorsimple',
                'name'          => 'dropdown_wide_background_hovercolor',
                'default_value' => '',
                'label'         => esc_html__('Hover/Active Background Color', 'malmo')
            )
        );

        $second_level_wide_row2 = malmo_elated_add_admin_row(
            array(
                'parent' => $second_level_wide_group,
                'name'   => 'second_level_wide_row2',
                'next'   => true
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $second_level_wide_row2,
                'type'          => 'fontsimple',
                'name'          => 'dropdown_wide_google_fonts',
                'default_value' => '-1',
                'label'         => esc_html__('Font Family', 'malmo')
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $second_level_wide_row2,
                'type'          => 'textsimple',
                'name'          => 'dropdown_wide_fontsize',
                'default_value' => '',
                'label'         => esc_html__('Font Size', 'malmo'),
                'args'          => array(
                    'suffix' => 'px'
                )
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $second_level_wide_row2,
                'type'          => 'textsimple',
                'name'          => 'dropdown_wide_lineheight',
                'default_value' => '',
                'label'         => esc_html__('Line Height', 'malmo'),
                'args'          => array(
                    'suffix' => 'px'
                )
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $second_level_wide_row2,
                'type'          => 'textsimple',
                'name'          => 'dropdown_wide_padding_top_bottom',
                'default_value' => '',
                'label'         => esc_html__('Padding Top/Bottom', 'malmo'),
                'args'          => array(
                    'suffix' => 'px'
                )
            )
        );

        $second_level_wide_row3 = malmo_elated_add_admin_row(
            array(
                'parent' => $second_level_wide_group,
                'name'   => 'second_level_wide_row3',
                'next'   => true
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $second_level_wide_row3,
                'type'          => 'selectblanksimple',
                'name'          => 'dropdown_wide_fontstyle',
                'default_value' => '',
                'label'         => esc_html__('Font style', 'malmo'),
                'options'       => malmo_elated_get_font_style_array()
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $second_level_wide_row3,
                'type'          => 'selectblanksimple',
                'name'          => 'dropdown_wide_fontweight',
                'default_value' => '',
                'label'         => esc_html__('Font weight', 'malmo'),
                'options'       => malmo_elated_get_font_weight_array()
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $second_level_wide_row3,
                'type'          => 'textsimple',
                'name'          => 'dropdown_wide_letterspacing',
                'default_value' => '',
                'label'         => esc_html__('Letter spacing', 'malmo'),
                'args'          => array(
                    'suffix' => 'px'
                )
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $second_level_wide_row3,
                'type'          => 'selectblanksimple',
                'name'          => 'dropdown_wide_texttransform',
                'default_value' => '',
                'label'         => esc_html__('Text Transform', 'malmo'),
                'options'       => malmo_elated_get_text_transform_array()
            )
        );

        $third_level_group = malmo_elated_add_admin_group(
            array(
                'parent'      => $panel_main_menu,
                'name'        => 'third_level_group',
                'title'       => esc_html__('3nd Level Menu', 'malmo'),
                'description' => esc_html__('Define styles for 3nd level in Top Navigation Menu', 'malmo')
            )
        );

        $third_level_row1 = malmo_elated_add_admin_row(
            array(
                'parent' => $third_level_group,
                'name'   => 'third_level_row1'
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $third_level_row1,
                'type'          => 'colorsimple',
                'name'          => 'dropdown_color_thirdlvl',
                'default_value' => '',
                'label'         => esc_html__('Text Color', 'malmo')
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $third_level_row1,
                'type'          => 'colorsimple',
                'name'          => 'dropdown_hovercolor_thirdlvl',
                'default_value' => '',
                'label'         => esc_html__('Hover/Active Color', 'malmo')
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $third_level_row1,
                'type'          => 'colorsimple',
                'name'          => 'dropdown_background_hovercolor_thirdlvl',
                'default_value' => '',
                'label'         => esc_html__('Hover/Active Background Color', 'malmo')
            )
        );

        $third_level_row2 = malmo_elated_add_admin_row(
            array(
                'parent' => $third_level_group,
                'name'   => 'third_level_row2',
                'next'   => true
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $third_level_row2,
                'type'          => 'fontsimple',
                'name'          => 'dropdown_google_fonts_thirdlvl',
                'default_value' => '-1',
                'label'         => esc_html__('Font Family', 'malmo')
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $third_level_row2,
                'type'          => 'textsimple',
                'name'          => 'dropdown_fontsize_thirdlvl',
                'default_value' => '',
                'label'         => esc_html__('Font Size', 'malmo'),
                'args'          => array(
                    'suffix' => 'px'
                )
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $third_level_row2,
                'type'          => 'textsimple',
                'name'          => 'dropdown_lineheight_thirdlvl',
                'default_value' => '',
                'label'         => esc_html__('Line Height', 'malmo'),
                'args'          => array(
                    'suffix' => 'px'
                )
            )
        );

        $third_level_row3 = malmo_elated_add_admin_row(
            array(
                'parent' => $third_level_group,
                'name'   => 'third_level_row3',
                'next'   => true
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $third_level_row3,
                'type'          => 'selectblanksimple',
                'name'          => 'dropdown_fontstyle_thirdlvl',
                'default_value' => '',
                'label'         => esc_html__('Font style', 'malmo'),
                'options'       => malmo_elated_get_font_style_array()
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $third_level_row3,
                'type'          => 'selectblanksimple',
                'name'          => 'dropdown_fontweight_thirdlvl',
                'default_value' => '',
                'label'         => esc_html__('Font weight', 'malmo'),
                'options'       => malmo_elated_get_font_weight_array()
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $third_level_row3,
                'type'          => 'textsimple',
                'name'          => 'dropdown_letterspacing_thirdlvl',
                'default_value' => '',
                'label'         => esc_html__('Letter spacing', 'malmo'),
                'args'          => array(
                    'suffix' => 'px'
                )
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $third_level_row3,
                'type'          => 'selectblanksimple',
                'name'          => 'dropdown_texttransform_thirdlvl',
                'default_value' => '',
                'label'         => esc_html__('Text Transform', 'malmo'),
                'options'       => malmo_elated_get_text_transform_array()
            )
        );


        /***********************************************************/
        $third_level_wide_group = malmo_elated_add_admin_group(
            array(
                'parent'      => $panel_main_menu,
                'name'        => 'third_level_wide_group',
                'title'       => esc_html__('3rd Level Wide Menu', 'malmo'),
                'description' => esc_html__('Define styles for 3rd level in Wide Menu', 'malmo')
            )
        );

        $third_level_wide_row1 = malmo_elated_add_admin_row(
            array(
                'parent' => $third_level_wide_group,
                'name'   => 'third_level_wide_row1'
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $third_level_wide_row1,
                'type'          => 'colorsimple',
                'name'          => 'dropdown_wide_color_thirdlvl',
                'default_value' => '',
                'label'         => esc_html__('Text Color', 'malmo')
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $third_level_wide_row1,
                'type'          => 'colorsimple',
                'name'          => 'dropdown_wide_hovercolor_thirdlvl',
                'default_value' => '',
                'label'         => esc_html__('Hover/Active Color', 'malmo')
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $third_level_wide_row1,
                'type'          => 'colorsimple',
                'name'          => 'dropdown_wide_background_hovercolor_thirdlvl',
                'default_value' => '',
                'label'         => esc_html__('Hover/Active Background Color', 'malmo')
            )
        );

        $third_level_wide_row2 = malmo_elated_add_admin_row(
            array(
                'parent' => $third_level_wide_group,
                'name'   => 'third_level_wide_row2',
                'next'   => true
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $third_level_wide_row2,
                'type'          => 'fontsimple',
                'name'          => 'dropdown_wide_google_fonts_thirdlvl',
                'default_value' => '-1',
                'label'         => esc_html__('Font Family', 'malmo')
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $third_level_wide_row2,
                'type'          => 'textsimple',
                'name'          => 'dropdown_wide_fontsize_thirdlvl',
                'default_value' => '',
                'label'         => esc_html__('Font Size', 'malmo'),
                'args'          => array(
                    'suffix' => 'px'
                )
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $third_level_wide_row2,
                'type'          => 'textsimple',
                'name'          => 'dropdown_wide_lineheight_thirdlvl',
                'default_value' => '',
                'label'         => esc_html__('Line Height', 'malmo'),
                'args'          => array(
                    'suffix' => 'px'
                )
            )
        );

        $third_level_wide_row3 = malmo_elated_add_admin_row(
            array(
                'parent' => $third_level_wide_group,
                'name'   => 'third_level_wide_row3',
                'next'   => true
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $third_level_wide_row3,
                'type'          => 'selectblanksimple',
                'name'          => 'dropdown_wide_fontstyle_thirdlvl',
                'default_value' => '',
                'label'         => esc_html__('Font style', 'malmo'),
                'options'       => malmo_elated_get_font_style_array()
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $third_level_wide_row3,
                'type'          => 'selectblanksimple',
                'name'          => 'dropdown_wide_fontweight_thirdlvl',
                'default_value' => '',
                'label'         => esc_html__('Font weight', 'malmo'),
                'options'       => malmo_elated_get_font_weight_array()
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $third_level_wide_row3,
                'type'          => 'textsimple',
                'name'          => 'dropdown_wide_letterspacing_thirdlvl',
                'default_value' => '',
                'label'         => esc_html__('Letter spacing', 'malmo'),
                'args'          => array(
                    'suffix' => 'px'
                )
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $third_level_wide_row3,
                'type'          => 'selectblanksimple',
                'name'          => 'dropdown_wide_texttransform_thirdlvl',
                'default_value' => '',
                'label'         => esc_html__('Text Transform', 'malmo'),
                'options'       => malmo_elated_get_text_transform_array()
            )
        );

        $panel_vertical_main_menu = malmo_elated_add_admin_panel(
            array(
                'title'           => esc_html__('Vertical Main Menu', 'malmo'),
                'name'            => 'panel_vertical_main_menu',
                'page'            => '_header_page',
                'hidden_property' => 'header_type',
                'hidden_values'   => array(
                    'header-standard',
                    'header-minimal',
                    'header-centered'
                )
            )
        );

        $drop_down_group = malmo_elated_add_admin_group(
            array(
                'parent'      => $panel_vertical_main_menu,
                'name'        => 'vertical_drop_down_group',
                'title'       => esc_html__('Main Dropdown Menu', 'malmo'),
                'description' => esc_html__('Set a style for dropdown menu', 'malmo')
            )
        );

        $vertical_drop_down_row1 = malmo_elated_add_admin_row(
            array(
                'parent' => $drop_down_group,
                'name'   => 'eltd_drop_down_row1',
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $vertical_drop_down_row1,
                'type'          => 'colorsimple',
                'name'          => 'vertical_dropdown_background_color',
                'default_value' => '',
                'label'         => esc_html__('Background Color', 'malmo'),
            )
        );

        $group_vertical_first_level = malmo_elated_add_admin_group(array(
            'name'        => 'group_vertical_first_level',
            'title'       => esc_html__('1st level', 'malmo'),
            'description' => esc_html__('Define styles for 1st level menu', 'malmo'),
            'parent'      => $panel_vertical_main_menu
        ));

        $row_vertical_first_level_1 = malmo_elated_add_admin_row(array(
            'name'   => 'row_vertical_first_level_1',
            'parent' => $group_vertical_first_level
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'colorsimple',
            'name'          => 'vertical_menu_1st_color',
            'default_value' => '',
            'label'         => esc_html__('Text Color', 'malmo'),
            'parent'        => $row_vertical_first_level_1
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'colorsimple',
            'name'          => 'vertical_menu_1st_hover_color',
            'default_value' => '',
            'label'         => esc_html__('Hover/Active Color', 'malmo'),
            'parent'        => $row_vertical_first_level_1
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'textsimple',
            'name'          => 'vertical_menu_1st_fontsize',
            'default_value' => '',
            'label'         => esc_html__('Font Size', 'malmo'),
            'args'          => array(
                'suffix' => 'px'
            ),
            'parent'        => $row_vertical_first_level_1
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'textsimple',
            'name'          => 'vertical_menu_1st_lineheight',
            'default_value' => '',
            'label'         => esc_html__('Line Height', 'malmo'),
            'args'          => array(
                'suffix' => 'px'
            ),
            'parent'        => $row_vertical_first_level_1
        ));

        $row_vertical_first_level_2 = malmo_elated_add_admin_row(array(
            'name'   => 'row_vertical_first_level_2',
            'parent' => $group_vertical_first_level,
            'next'   => true
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'selectblanksimple',
            'name'          => 'vertical_menu_1st_texttransform',
            'default_value' => '',
            'label'         => esc_html__('Text Transform', 'malmo'),
            'options'       => malmo_elated_get_text_transform_array(),
            'parent'        => $row_vertical_first_level_2
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'fontsimple',
            'name'          => 'vertical_menu_1st_google_fonts',
            'default_value' => '-1',
            'label'         => esc_html__('Font Family', 'malmo'),
            'parent'        => $row_vertical_first_level_2
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'selectblanksimple',
            'name'          => 'vertical_menu_1st_fontstyle',
            'default_value' => '',
            'label'         => esc_html__('Font Style', 'malmo'),
            'options'       => malmo_elated_get_font_style_array(),
            'parent'        => $row_vertical_first_level_2
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'selectblanksimple',
            'name'          => 'vertical_menu_1st_fontweight',
            'default_value' => '',
            'label'         => esc_html__('Font Weight', 'malmo'),
            'options'       => malmo_elated_get_font_weight_array(),
            'parent'        => $row_vertical_first_level_2
        ));

        $row_vertical_first_level_3 = malmo_elated_add_admin_row(array(
            'name'   => 'row_vertical_first_level_3',
            'parent' => $group_vertical_first_level,
            'next'   => true
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'textsimple',
            'name'          => 'vertical_menu_1st_letter_spacing',
            'default_value' => '',
            'label'         => esc_html__('Letter Spacing', 'malmo'),
            'args'          => array(
                'suffix' => 'px'
            ),
            'parent'        => $row_vertical_first_level_3
        ));

        $group_vertical_second_level = malmo_elated_add_admin_group(array(
            'name'        => 'group_vertical_second_level',
            'title'       => esc_html__('2nd level', 'malmo'),
            'description' => esc_html__('Define styles for 2nd level menu', 'malmo'),
            'parent'      => $panel_vertical_main_menu
        ));

        $row_vertical_second_level_1 = malmo_elated_add_admin_row(array(
            'name'   => 'row_vertical_second_level_1',
            'parent' => $group_vertical_second_level
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'colorsimple',
            'name'          => 'vertical_menu_2nd_color',
            'default_value' => '',
            'label'         => esc_html__('Text Color', 'malmo'),
            'parent'        => $row_vertical_second_level_1
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'colorsimple',
            'name'          => 'vertical_menu_2nd_hover_color',
            'default_value' => '',
            'label'         => esc_html__('Hover/Active Color', 'malmo'),
            'parent'        => $row_vertical_second_level_1
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'textsimple',
            'name'          => 'vertical_menu_2nd_fontsize',
            'default_value' => '',
            'label'         => esc_html__('Font Size', 'malmo'),
            'args'          => array(
                'suffix' => 'px'
            ),
            'parent'        => $row_vertical_second_level_1
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'textsimple',
            'name'          => 'vertical_menu_2nd_lineheight',
            'default_value' => '',
            'label'         => esc_html__('Line Height', 'malmo'),
            'args'          => array(
                'suffix' => 'px'
            ),
            'parent'        => $row_vertical_second_level_1
        ));

        $row_vertical_second_level_2 = malmo_elated_add_admin_row(array(
            'name'   => 'row_vertical_second_level_2',
            'parent' => $group_vertical_second_level,
            'next'   => true
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'selectblanksimple',
            'name'          => 'vertical_menu_2nd_texttransform',
            'default_value' => '',
            'label'         => esc_html__('Text Transform', 'malmo'),
            'options'       => malmo_elated_get_text_transform_array(),
            'parent'        => $row_vertical_second_level_2
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'fontsimple',
            'name'          => 'vertical_menu_2nd_google_fonts',
            'default_value' => '-1',
            'label'         => esc_html__('Font Family', 'malmo'),
            'parent'        => $row_vertical_second_level_2
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'selectblanksimple',
            'name'          => 'vertical_menu_2nd_fontstyle',
            'default_value' => '',
            'label'         => esc_html__('Font Style', 'malmo'),
            'options'       => malmo_elated_get_font_style_array(),
            'parent'        => $row_vertical_second_level_2
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'selectblanksimple',
            'name'          => 'vertical_menu_2nd_fontweight',
            'default_value' => '',
            'label'         => esc_html__('Font Weight', 'malmo'),
            'options'       => malmo_elated_get_font_weight_array(),
            'parent'        => $row_vertical_second_level_2
        ));

        $row_vertical_second_level_3 = malmo_elated_add_admin_row(array(
            'name'   => 'row_vertical_second_level_3',
            'parent' => $group_vertical_second_level,
            'next'   => true
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'textsimple',
            'name'          => 'vertical_menu_2nd_letter_spacing',
            'default_value' => '',
            'label'         => esc_html__('Letter Spacing', 'malmo'),
            'args'          => array(
                'suffix' => 'px'
            ),
            'parent'        => $row_vertical_second_level_3
        ));

        $group_vertical_third_level = malmo_elated_add_admin_group(array(
            'name'        => 'group_vertical_third_level',
            'title'       => esc_html__('3rd level', 'malmo'),
            'description' => esc_html__('Define styles for 3rd level menu', 'malmo'),
            'parent'      => $panel_vertical_main_menu
        ));

        $row_vertical_third_level_1 = malmo_elated_add_admin_row(array(
            'name'   => 'row_vertical_third_level_1',
            'parent' => $group_vertical_third_level
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'colorsimple',
            'name'          => 'vertical_menu_3rd_color',
            'default_value' => '',
            'label'         => esc_html__('Text Color', 'malmo'),
            'parent'        => $row_vertical_third_level_1
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'colorsimple',
            'name'          => 'vertical_menu_3rd_hover_color',
            'default_value' => '',
            'label'         => esc_html__('Hover/Active Color', 'malmo'),
            'parent'        => $row_vertical_third_level_1
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'textsimple',
            'name'          => 'vertical_menu_3rd_fontsize',
            'default_value' => '',
            'label'         => esc_html__('Font Size', 'malmo'),
            'args'          => array(
                'suffix' => 'px'
            ),
            'parent'        => $row_vertical_third_level_1
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'textsimple',
            'name'          => 'vertical_menu_3rd_lineheight',
            'default_value' => '',
            'label'         => esc_html__('Line Height', 'malmo'),
            'args'          => array(
                'suffix' => 'px'
            ),
            'parent'        => $row_vertical_third_level_1
        ));

        $row_vertical_third_level_2 = malmo_elated_add_admin_row(array(
            'name'   => 'row_vertical_third_level_2',
            'parent' => $group_vertical_third_level,
            'next'   => true
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'selectblanksimple',
            'name'          => 'vertical_menu_3rd_texttransform',
            'default_value' => '',
            'label'         => esc_html__('Text Transform', 'malmo'),
            'options'       => malmo_elated_get_text_transform_array(),
            'parent'        => $row_vertical_third_level_2
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'fontsimple',
            'name'          => 'vertical_menu_3rd_google_fonts',
            'default_value' => '-1',
            'label'         => esc_html__('Font Family', 'malmo'),
            'parent'        => $row_vertical_third_level_2
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'selectblanksimple',
            'name'          => 'vertical_menu_3rd_fontstyle',
            'default_value' => '',
            'label'         => esc_html__('Font Style', 'malmo'),
            'options'       => malmo_elated_get_font_style_array(),
            'parent'        => $row_vertical_third_level_2
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'selectblanksimple',
            'name'          => 'vertical_menu_3rd_fontweight',
            'default_value' => '',
            'label'         => 'Font Weight',
            'options'       => malmo_elated_get_font_weight_array(),
            'parent'        => $row_vertical_third_level_2
        ));

        $row_vertical_third_level_3 = malmo_elated_add_admin_row(array(
            'name'   => 'row_vertical_third_level_3',
            'parent' => $group_vertical_third_level,
            'next'   => true
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'textsimple',
            'name'          => 'vertical_menu_3rd_letter_spacing',
            'default_value' => '',
            'label'         => esc_html__('Letter Spacing', 'malmo'),
            'args'          => array(
                'suffix' => 'px'
            ),
            'parent'        => $row_vertical_third_level_3
        ));

        $panel_mobile_header = malmo_elated_add_admin_panel(array(
            'title' => esc_html__('Mobile Header', 'malmo'),
            'name'  => 'panel_mobile_header',
            'page'  => '_header_page'
        ));

        malmo_elated_add_admin_field(array(
            'name'        => 'mobile_header_height',
            'type'        => 'text',
            'label'       => esc_html__('Mobile Header Height', 'malmo'),
            'description' => esc_html__('Enter height for mobile header in pixels', 'malmo'),
            'parent'      => $panel_mobile_header,
            'args'        => array(
                'col_width' => 3,
                'suffix'    => 'px'
            )
        ));

        malmo_elated_add_admin_field(array(
            'name'        => 'mobile_header_background_color',
            'type'        => 'color',
            'label'       => esc_html__('Mobile Header Background Color', 'malmo'),
            'description' => esc_html__('Choose color for mobile header', 'malmo'),
            'parent'      => $panel_mobile_header
        ));

        malmo_elated_add_admin_field(array(
            'name'        => 'mobile_menu_background_color',
            'type'        => 'color',
            'label'       => esc_html__('Mobile Menu Background Color', 'malmo'),
            'description' => esc_html__('Choose color for mobile menu', 'malmo'),
            'parent'      => $panel_mobile_header
        ));

        malmo_elated_add_admin_field(array(
            'name'        => 'mobile_menu_separator_color',
            'type'        => 'color',
            'label'       => esc_html__('Mobile Menu Item Separator Color', 'malmo'),
            'description' => esc_html__('Choose color for mobile menu horizontal separators', 'malmo'),
            'parent'      => $panel_mobile_header
        ));

        malmo_elated_add_admin_field(array(
            'name'        => 'mobile_logo_height',
            'type'        => 'text',
            'label'       => esc_html__('Logo Height For Mobile Header', 'malmo'),
            'description' => esc_html__('Define logo height for screen size smaller than 1000px', 'malmo'),
            'parent'      => $panel_mobile_header,
            'args'        => array(
                'col_width' => 3,
                'suffix'    => 'px'
            )
        ));

        malmo_elated_add_admin_field(array(
            'name'        => 'mobile_logo_height_phones',
            'type'        => 'text',
            'label'       => esc_html__('Logo Height For Mobile Devices', 'malmo'),
            'description' => esc_html__('Define logo height for screen size smaller than 480px', 'malmo'),
            'parent'      => $panel_mobile_header,
            'args'        => array(
                'col_width' => 3,
                'suffix'    => 'px'
            )
        ));

        malmo_elated_add_admin_section_title(array(
            'parent' => $panel_mobile_header,
            'name'   => 'mobile_header_fonts_title',
            'title'  => esc_html__('Typography', 'malmo')
        ));

        malmo_elated_add_admin_field(array(
            'name'        => 'mobile_text_color',
            'type'        => 'color',
            'label'       => esc_html__('Navigation Text Color', 'malmo'),
            'description' => esc_html__('Define color for mobile navigation text', 'malmo'),
            'parent'      => $panel_mobile_header
        ));

        malmo_elated_add_admin_field(array(
            'name'        => 'mobile_text_hover_color',
            'type'        => 'color',
            'label'       => esc_html__('Navigation Hover/Active Color', 'malmo'),
            'description' => esc_html__('Define hover/active color for mobile navigation text', 'malmo'),
            'parent'      => $panel_mobile_header
        ));

        malmo_elated_add_admin_field(array(
            'name'        => 'mobile_font_family',
            'type'        => 'font',
            'label'       => esc_html__('Navigation Font Family', 'malmo'),
            'description' => esc_html__('Define font family for mobile navigation text', 'malmo'),
            'parent'      => $panel_mobile_header
        ));

        malmo_elated_add_admin_field(array(
            'name'        => 'mobile_font_size',
            'type'        => 'text',
            'label'       => esc_html__('Navigation Font Size', 'malmo'),
            'description' => esc_html__('Define font size for mobile navigation text', 'malmo'),
            'parent'      => $panel_mobile_header,
            'args'        => array(
                'col_width' => 3,
                'suffix'    => 'px'
            )
        ));

        malmo_elated_add_admin_field(array(
            'name'        => 'mobile_line_height',
            'type'        => 'text',
            'label'       => esc_html__('Navigation Line Height', 'malmo'),
            'description' => esc_html__('Define line height for mobile navigation text', 'malmo'),
            'parent'      => $panel_mobile_header,
            'args'        => array(
                'col_width' => 3,
                'suffix'    => 'px'
            )
        ));

        malmo_elated_add_admin_field(array(
            'name'        => 'mobile_text_transform',
            'type'        => 'select',
            'label'       => esc_html__('Navigation Text Transform', 'malmo'),
            'description' => esc_html__('Define text transform for mobile navigation text', 'malmo'),
            'parent'      => $panel_mobile_header,
            'options'     => malmo_elated_get_text_transform_array(true)
        ));

        malmo_elated_add_admin_field(array(
            'name'        => 'mobile_font_style',
            'type'        => 'select',
            'label'       => esc_html__('Navigation Font Style', 'malmo'),
            'description' => esc_html__('Define font style for mobile navigation text', 'malmo'),
            'parent'      => $panel_mobile_header,
            'options'     => malmo_elated_get_font_style_array(true)
        ));

        malmo_elated_add_admin_field(array(
            'name'        => 'mobile_font_weight',
            'type'        => 'select',
            'label'       => esc_html__('Navigation Font Weight', 'malmo'),
            'description' => esc_html__('Define font weight for mobile navigation text', 'malmo'),
            'parent'      => $panel_mobile_header,
            'options'     => malmo_elated_get_font_weight_array(true)
        ));

        malmo_elated_add_admin_section_title(array(
            'name'   => 'mobile_opener_panel',
            'parent' => $panel_mobile_header,
            'title'  => esc_html__('Mobile Menu Opener', 'malmo')
        ));

        malmo_elated_add_admin_field(array(
            'name'          => 'mobile_icon_pack',
            'type'          => 'select',
            'label'         => esc_html__('Mobile Navigation Icon Pack', 'malmo'),
            'default_value' => 'font_awesome',
            'description'   => esc_html__('Choose icon pack for mobile navigation icon', 'malmo'),
            'parent'        => $panel_mobile_header,
            'options'       => malmo_elated_icon_collections()->getIconCollectionsExclude(array(
                'linea_icons',
                'simple_line_icons'
            ))
        ));

        malmo_elated_add_admin_field(array(
            'name'        => 'mobile_icon_color',
            'type'        => 'color',
            'label'       => esc_html__('Mobile Navigation Icon Color', 'malmo'),
            'description' => esc_html__('Choose color for icon header', 'malmo'),
            'parent'      => $panel_mobile_header
        ));

        malmo_elated_add_admin_field(array(
            'name'        => 'mobile_icon_hover_color',
            'type'        => 'color',
            'label'       => esc_html__('Mobile Navigation Icon Hover Color', 'malmo'),
            'description' => esc_html__('Choose hover color for mobile navigation icon ', 'malmo'),
            'parent'      => $panel_mobile_header
        ));

        malmo_elated_add_admin_field(array(
            'name'        => 'mobile_icon_size',
            'type'        => 'text',
            'label'       => esc_html__('Mobile Navigation Icon size', 'malmo'),
            'description' => esc_html__('Choose size for mobile navigation icon ', 'malmo'),
            'parent'      => $panel_mobile_header,
            'args'        => array(
                'col_width' => 3,
                'suffix'    => 'px'
            )
        ));
    }

    add_action('malmo_elated_options_map', 'malmo_elated_header_options_map', 3);

}