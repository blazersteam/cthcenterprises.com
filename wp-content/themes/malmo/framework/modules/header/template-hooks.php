<?php

//top header bar
add_action('malmo_elated_before_page_header', 'malmo_elated_get_header_top');

//mobile header
add_action('malmo_elated_after_page_header', 'malmo_elated_get_mobile_header');