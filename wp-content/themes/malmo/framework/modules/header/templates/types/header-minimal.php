<?php do_action('malmo_elated_before_page_header'); ?>

<header class="eltd-page-header">
    <?php if($show_fixed_wrapper) : ?>
    <div class="eltd-fixed-wrapper">
        <?php endif; ?>
        <div class="eltd-menu-area">
            <?php if($menu_area_in_grid) : ?>
            <div class="eltd-grid">
                <?php endif; ?>
                <?php do_action('malmo_elated_after_header_menu_area_html_open') ?>
                <div class="eltd-vertical-align-containers">
                    <div class="eltd-position-left">
                        <div class="eltd-position-left-inner">
                            <?php if(!$hide_logo) {
                                malmo_elated_get_logo();
                            } ?>
                        </div>
                    </div>
                    <div class="eltd-position-right">
                        <div class="eltd-position-right-inner">
                            <!--
                            <a href="javascript:void(0)" class="eltd-fullscreen-menu-opener">
                                <span class="eltd-fullscreen-menu-opener-icon">
                                    <?php // echo malmo_elated_icon_collections()->renderIcon('lnr-menu', 'linear_icons'); ?>
                                </span>
                            </a>
                            -->

                            <a href="javascript:void(0)" class="eltd-fullscreen-menu-opener">
                                <span class="eltd-fullscreen-menu-opener-holder">
                                    <span class="eltd-fullscreen-menu-opener-inner">
                                        <span class="eltd-fullscreen-icon">
                                            <span class="eltd-line"></span>
                                            <span class="eltd-line"></span>
                                            <span class="eltd-line"></span>
                                        </span>
                                    </span>
                                </span>
                            </a>

                        </div>
                    </div>
                </div>
                <?php if($menu_area_in_grid) : ?>
            </div>
        <?php endif; ?>
        </div>
        <?php if($show_fixed_wrapper) : ?>
    </div>
<?php endif; ?>
    <?php if($show_sticky) {
        malmo_elated_get_sticky_header('minimal');
    } ?>
</header>

<?php do_action('malmo_elated_after_page_header'); ?>

