<?php do_action('malmo_elated_before_site_logo'); ?>

	<div class="eltd-logo-wrapper">
		<a href="<?php echo esc_url(home_url('/')); ?>" <?php malmo_elated_inline_style($logo_styles); ?>>
			<?php if(!empty($logo_image)) { ?>
			<img <?php echo malmo_elated_get_inline_attrs($logo_dimensions_attr); ?> class="eltd-normal-logo" src="<?php echo esc_url($logo_image); ?>" alt="<?php esc_attr_e( 'logo', 'malmo' ); ?>"/>
			<?php } else{ echo '<h1 class="site-title">'.get_bloginfo( "name" ).'</h1>'; } ?>
			<?php if(!empty($logo_image_dark)) { ?>
				<img <?php echo malmo_elated_get_inline_attrs($logo_dimensions_attr); ?> class="eltd-dark-logo" src="<?php echo esc_url($logo_image_dark); ?>" alt="<?php esc_attr_e( 'dark logo', 'malmo' ); ?>"/><?php } ?>
			<?php if(!empty($logo_image_light)) { ?>
				<img <?php echo malmo_elated_get_inline_attrs($logo_dimensions_attr); ?> class="eltd-light-logo" src="<?php echo esc_url($logo_image_light); ?>" alt="<?php esc_attr_e( 'light logo', 'malmo' ); ?>"/><?php } ?>
		</a>
	</div>

<?php do_action('malmo_elated_after_site_logo'); ?>