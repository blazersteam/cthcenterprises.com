<?php

if(!function_exists('malmo_elated_mobile_header_general_styles')) {
	/**
	 * Generates general custom styles for mobile header
	 */
	function malmo_elated_mobile_header_general_styles() {
		$mobile_header_styles = array();
		if(malmo_elated_options()->getOptionValue('mobile_header_height') !== '') {
			$mobile_header_styles['height'] = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('mobile_header_height')).'px';
		}

		if(malmo_elated_options()->getOptionValue('mobile_header_background_color')) {
			$mobile_header_styles['background-color'] = malmo_elated_options()->getOptionValue('mobile_header_background_color');
		}

		echo malmo_elated_dynamic_css('.eltd-mobile-header .eltd-mobile-header-inner', $mobile_header_styles);
	}

	add_action('malmo_elated_style_dynamic', 'malmo_elated_mobile_header_general_styles');
}

if(!function_exists('malmo_elated_mobile_navigation_styles')) {
	/**
	 * Generates styles for mobile navigation
	 */
	function malmo_elated_mobile_navigation_styles() {
		$mobile_nav_styles = array();
		if(malmo_elated_options()->getOptionValue('mobile_menu_background_color')) {
			$mobile_nav_styles['background-color'] = malmo_elated_options()->getOptionValue('mobile_menu_background_color');
		}

		echo malmo_elated_dynamic_css('.eltd-mobile-header .eltd-mobile-nav', $mobile_nav_styles);

		$mobile_nav_item_styles = array();
		if(malmo_elated_options()->getOptionValue('mobile_menu_separator_color') !== '') {
			$mobile_nav_item_styles['border-bottom-color'] = malmo_elated_options()->getOptionValue('mobile_menu_separator_color');
		}

		if(malmo_elated_options()->getOptionValue('mobile_text_color') !== '') {
			$mobile_nav_item_styles['color'] = malmo_elated_options()->getOptionValue('mobile_text_color');
		}

		if(malmo_elated_is_font_option_valid(malmo_elated_options()->getOptionValue('mobile_font_family'))) {
			$mobile_nav_item_styles['font-family'] = malmo_elated_get_formatted_font_family(malmo_elated_options()->getOptionValue('mobile_font_family'));
		}

		if(malmo_elated_options()->getOptionValue('mobile_font_size') !== '') {
			$mobile_nav_item_styles['font-size'] = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('mobile_font_size')).'px';
		}

		if(malmo_elated_options()->getOptionValue('mobile_line_height') !== '') {
			$mobile_nav_item_styles['line-height'] = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('mobile_line_height')).'px';
		}

		if(malmo_elated_options()->getOptionValue('mobile_text_transform') !== '') {
			$mobile_nav_item_styles['text-transform'] = malmo_elated_options()->getOptionValue('mobile_text_transform');
		}

		if(malmo_elated_options()->getOptionValue('mobile_font_style') !== '') {
			$mobile_nav_item_styles['font-style'] = malmo_elated_options()->getOptionValue('mobile_font_style');
		}

		if(malmo_elated_options()->getOptionValue('mobile_font_weight') !== '') {
			$mobile_nav_item_styles['font-weight'] = malmo_elated_options()->getOptionValue('mobile_font_weight');
		}

		$mobile_nav_item_selector = array(
			'.eltd-mobile-header .eltd-mobile-nav a',
			'.eltd-mobile-header .eltd-mobile-nav h4'
		);

		echo malmo_elated_dynamic_css($mobile_nav_item_selector, $mobile_nav_item_styles);

		$mobile_nav_item_hover_styles = array();
		if(malmo_elated_options()->getOptionValue('mobile_text_hover_color') !== '') {
			$mobile_nav_item_hover_styles['color'] = malmo_elated_options()->getOptionValue('mobile_text_hover_color');
		}

		$mobile_nav_item_selector_hover = array(
			'.eltd-mobile-header .eltd-mobile-nav a:hover',
			'.eltd-mobile-header .eltd-mobile-nav h4:hover'
		);

		echo malmo_elated_dynamic_css($mobile_nav_item_selector_hover, $mobile_nav_item_hover_styles);
	}

	add_action('malmo_elated_style_dynamic', 'malmo_elated_mobile_navigation_styles');
}

if(!function_exists('malmo_elated_mobile_logo_styles')) {
	/**
	 * Generates styles for mobile logo
	 */
	function malmo_elated_mobile_logo_styles() {
		if(malmo_elated_options()->getOptionValue('mobile_logo_height') !== '') { ?>
			@media only screen and (max-width: 1000px) {
			<?php echo malmo_elated_dynamic_css(
				'.eltd-mobile-header .eltd-mobile-logo-wrapper a',
				array('height' => malmo_elated_filter_px(malmo_elated_options()->getOptionValue('mobile_logo_height')).'px !important')
			); ?>
			}
		<?php }

		if(malmo_elated_options()->getOptionValue('mobile_logo_height_phones') !== '') { ?>
			@media only screen and (max-width: 480px) {
			<?php echo malmo_elated_dynamic_css(
				'.eltd-mobile-header .eltd-mobile-logo-wrapper a',
				array('height' => malmo_elated_filter_px(malmo_elated_options()->getOptionValue('mobile_logo_height_phones')).'px !important')
			); ?>
			}
		<?php }

		if(malmo_elated_options()->getOptionValue('mobile_header_height') !== '') {
			$max_height = intval(malmo_elated_filter_px(malmo_elated_options()->getOptionValue('mobile_header_height')) * 0.9).'px';
			echo malmo_elated_dynamic_css('.eltd-mobile-header .eltd-mobile-logo-wrapper a', array('max-height' => $max_height));
		}
	}

	add_action('malmo_elated_style_dynamic', 'malmo_elated_mobile_logo_styles');
}

if(!function_exists('malmo_elated_mobile_icon_styles')) {
	/**
	 * Generates styles for mobile icon opener
	 */
	function malmo_elated_mobile_icon_styles() {
		$mobile_icon_styles = array();
		if(malmo_elated_options()->getOptionValue('mobile_icon_color') !== '') {
			$mobile_icon_styles['color'] = malmo_elated_options()->getOptionValue('mobile_icon_color');
		}

		if(malmo_elated_options()->getOptionValue('mobile_icon_size') !== '') {
			$mobile_icon_styles['font-size'] = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('mobile_icon_size')).'px';
		}

		echo malmo_elated_dynamic_css('.eltd-mobile-header .eltd-mobile-menu-opener a', $mobile_icon_styles);

		if(malmo_elated_options()->getOptionValue('mobile_icon_hover_color') !== '') {
			echo malmo_elated_dynamic_css(
				'.eltd-mobile-header .eltd-mobile-menu-opener a:hover',
				array('color' => malmo_elated_options()->getOptionValue('mobile_icon_hover_color')));
		}
	}

	add_action('malmo_elated_style_dynamic', 'malmo_elated_mobile_icon_styles');
}