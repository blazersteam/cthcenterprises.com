<?php

$portfolio_client = get_post_meta(get_the_ID(), 'portfolio_client_info', true);
if($portfolio_client != '') :
    ?>
    <div class="eltd-portfolio-client-info eltd-portfolio-info-item">
        <h3 class="eltd-portfolio-info-label"><?php echo esc_html__('Client:', 'malmo'); ?></h3>
        <h5 class="eltd-portfolio-info-value"><?php echo esc_html($portfolio_client); ?></h5>
    </div>
<?php endif; ?>