<div class="eltd-big-image-holder">
	<?php
	$media = malmo_elated_get_portfolio_single_media();

	if(is_array($media) && count($media)) : ?>
		<div class="eltd-portfolio-media">
			<?php foreach($media as $single_media) : ?>
				<div class="eltd-portfolio-single-media">
					<?php malmo_elated_portfolio_get_media_html($single_media); ?>
				</div>
			<?php endforeach; ?>
		</div>
	<?php endif; ?>
</div>

<div class="eltd-portfolio-info-content-holder <?php echo malmo_elated_get_portfolio_columns_class($portfolio_template, $info_box_position); ?> clearfix">
	<div class="eltd-column1">
		<div class="eltd-column-inner">
			<?php if ($info_box_position == 'right') : ?>
				<?php malmo_elated_portfolio_get_info_part('content'); ?>
			<?php else: ?>
				<div class="eltd-portfolio-info-holder">
					<?php	
					//get portfolio custom fields section
					malmo_elated_portfolio_get_info_part('custom-fields');

					//get portfolio categories section
					malmo_elated_portfolio_get_info_part('categories');

					//get portfolio client section
					malmo_elated_portfolio_get_info_part('client');

					//get portfolio date section
					malmo_elated_portfolio_get_info_part('date');

					//get portfolio tags section
					malmo_elated_portfolio_get_info_part('tags');

					//get portfolio share section
					malmo_elated_portfolio_get_info_part('social');
					?>
				</div>
			<?php endif; ?>

		</div>
	</div>
	<div class="eltd-column2">
		<div class="eltd-column-inner">
			<?php if ($info_box_position == 'right') : ?>
				<div class="eltd-portfolio-info-holder">
					<?php
					//get portfolio custom fields section
					malmo_elated_portfolio_get_info_part('custom-fields');

					//get portfolio categories section
					malmo_elated_portfolio_get_info_part('categories');

					//get portfolio client section
					malmo_elated_portfolio_get_info_part('client');

					//get portfolio date section
					malmo_elated_portfolio_get_info_part('date');

					//get portfolio tags section
					malmo_elated_portfolio_get_info_part('tags');

					//get portfolio share section
					malmo_elated_portfolio_get_info_part('social');
					?>
				</div>
			<?php else: ?>
				<?php malmo_elated_portfolio_get_info_part('content'); ?>
			<?php endif; ?>
		</div>
	</div>
</div>