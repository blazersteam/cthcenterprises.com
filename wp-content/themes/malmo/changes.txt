1.9
- Added WooCommerce 3.6.5 compatibility
- Updated WPBakery Page Builder to 6.0.5
- Updated Revolution Slider to 6.0.8
- Updated Elated Core to 1.4.1
- Fixed Portfolio filter issues
- Fixed warning on Portfolio image upload

1.8.2
- Fixed issue with split screen slider and contact form 7

1.8.1
- Improved framework files

1.8
- Added WooCommerce 3.6.1 compatibility
- Updated Elated Core to 1.4
- Improved import functionality
- Improved theme security

1.7.1
- Updated Elated Core to 1.3.1
- Improved framework files

1.7
- Added WooCommerce 3.5.7 compatibility
- Updated WPBakery Page Builder to 5.7
- Updated Revolution Slider to 5.4.8.3
- Updated Elated Core to 1.3
- Updated Child theme to 1.0.1
- Improved framework files

1.6.1
-Fixed date format on Standard blog post - date on side

1.6
- Added WooCommerce 3.4.4 compatibility
- Added compatibility with Gutenberg plugin
- Updated WPBakery Page Builder to 5.5.2
- Updated Revolution Slider to 5.4.8
- Updated Envato Market to 2.0.1
- Fixed invalid content in pot file

1.5
- Added compatibility with PHP 7.2 
- Added Woocommerce 3.3.3 compatibility
- Updated Visual Composer to 5.4.7
- Updated Revolution Slider to 5.4.7
- Updated Envato Market to 2.0.0

1.4
- Added Envato Market plugin as required
- Added recommended plugins (WooCommerce and Contact Form 7)
- Updated Visual Composer to 5.4.5
- Updated Revolution Slider to 5.4.6.4
- Updated Elated Core to 1.2.1
- Fixed issue with google maps
- Fixed potential security issue when saving theme options
- Fixed the issue wp-mediaelement script on single portfolio
- Improved import for Revolution slider slides
- Improved menu import functionality

1.3
- Added multisite support
- Added WooCommerce 3+ compatibility 
- Updated Visual Composer to 5.1.1
- Updated Revolution Slider to version 5.4.3.1
- Fixed https problem with links

1.2
- Added Woocommerce 2.6.9 compatibility 
- Updated Visual Composer to 5.0.1
- Updated Revolution Slider to 5.3.1.5
- Updated Elated Core plugin to 1.1.1

1.1
- Added Woocommerce 2.6.8 compatibility 
- Updated Visual Composer to 5.0
- Updated Revolution Slider to 5.3.0.2
- Updated Elated Core plugin to 1.1
- Fixed the issue with general custom styles not being applied
- Fixed Blog slider button functionality
- Fixed bug with Elated Option saving